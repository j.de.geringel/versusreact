import React, { Component } from 'react'
import { View, Text, Image, TouchableHighlight, Share, Alert } from 'react-native'
import { List, ListItem, Button, Icon } from 'react-native-elements'
import { NavigationActions } from 'react-navigation'
import colors from './../colors'


import { XhrGameService } from './services/games/XhrGameService'
import { UserService } from './services/UserService'
import SoundService from './services/SoundService'
import State from './services/State';

import RegularHeader from './components/RegularHeader'



export default class NewGameScreen extends Component {

  async startRandomGame() {
    await XhrGameService().startOrJoinRandomGame();
    this.props.navigation.dispatch(
      NavigationActions.reset({
        index: 0, actions: [NavigationActions.navigate({ routeName: 'Initial' })]
      }));
  }

  inviteOpponent() {
    SoundService.playClickSound();
    Share.share({
      message: UserService().getUser().name + ' challenges you to play a Versus Minigame Battle! Download this game from your App Store, see links at http://www.polyprog.io#versus',
      title: ''
    })
      .then()
      .catch((error) => this.setState({ result: 'error: ' + error.message }));
  }

  ifAllowedToPlayOnline() {
    SoundService.playClickSound();
    if (State.onlineBlocked) {
      Alert.alert(
        'Please update',
        'Your app is outdated. Online play not possible, please update the app.'
      )
      return false;
    }
    else {
      //perform action
      return true;
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <RegularHeader message={UserService().signedIn() ? "New Game" : ''} navigation={this.props.navigation} />
        <View style={{ flex: 1, backgroundColor: colors.lightbg }}>
          <View style={{ flex: 7, justifyContent: 'flex-start' }}>

            {UserService().signedIn() == false ?

              <TouchableHighlight key="signin" onPress={() => this.props.navigation.navigate('SignUpScreen')}>
                <View style={{ backgroundColor: colors.primdark, height: 70,borderTopWidth: 1, borderBottomWidth: 1, borderColor: colors.primmed, flexDirection: 'row' }}>
                  <View style={{ width: 60, alignItems: 'flex-end', justifyContent: 'center' }}>
                    <Image source={require('./../images/icons/logo.png')} style={{ width: 45, height: 40 }} />
                  </View>
                  <View style={{ flex: 3, padding: 5, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ paddingLeft: 10, paddingBottom: 3, fontSize: 18, fontWeight: 'bold', color: colors.white }}>Sign In</Text>
                    <Text style={{ color: colors.white, paddingLeft: 10, paddingTop: 3, fontSize: 12, textAlign: 'center' }}>Sign In to play online!</Text>
                  </View>
                  <View style={{ width: 40, justifyContent: 'center', alignItems: 'center', paddingRight: 10 }}>
                    <Icon name='chevron-right' color={colors.white} size={40} />
                  </View>
                </View>
              </TouchableHighlight>

              : null}

            {UserService().signedIn() ?

              <TouchableHighlight key="search" onPress={() => this.ifAllowedToPlayOnline() ? this.props.navigation.navigate('NewGameSearchScreen') : null}>
                <View style={{ backgroundColor: 'white', height: 70, borderBottomWidth: 1, borderBottomColor: colors.divider, flexDirection: 'row' }}>
                  <View style={{ width: 60, alignItems: 'flex-end', justifyContent: 'center' }}>
                    <Image source={require('./../images/icons/mode-search.png')} style={{ width: 40, height: 40 }} />
                  </View>
                  <View style={{ flex: 3, padding: 5, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ paddingLeft: 10, fontSize: 18, fontWeight: 'bold', paddingBottom: 3 }}>Search VS user</Text>
                    <Text style={{ color: 'gray', paddingLeft: 10, fontSize: 12, paddingTop: 3 }}>Play online by searching VS users</Text>
                  </View>
                  <View style={{ width: 40, justifyContent: 'center', alignItems: 'center', paddingRight: 10 }}>
                    <Icon name='chevron-right' color={colors.primalpha} size={40} />
                  </View>
                </View>
              </TouchableHighlight>

              : null}


            {UserService().signedIn() ?
              <TouchableHighlight key="random" onPress={() => this.ifAllowedToPlayOnline() ? this.startRandomGame() : null}>
                <View style={{ backgroundColor: 'white', height: 70, borderBottomWidth: 1, borderBottomColor: colors.divider, flexDirection: 'row' }}>
                  <View style={{ width: 60, alignItems: 'flex-end', justifyContent: 'center' }}>
                    <Image source={require('./../images/icons/mode-random.png')} style={{ width: 40, height: 40 }} />
                  </View>
                  <View style={{ flex: 3, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ paddingLeft: 10, paddingBottom: 3, fontSize: 18, fontWeight: 'bold' }}>Random Opponent</Text>
                    <Text style={{ color: 'gray', paddingLeft: 10, paddingTop: 3, fontSize: 12, textAlign: 'center' }}>Play online against a random person</Text>
                  </View>
                  <View style={{ width: 40, justifyContent: 'center', alignItems: 'center', paddingRight: 10 }}>
                    <Icon name='chevron-right' color={colors.primalpha} size={40} />
                  </View>
                </View>
              </TouchableHighlight>

              : null}

            <TouchableHighlight key="local" onPress={() => {
              SoundService.playClickSound();
              this.props.navigation.navigate('PartyGameLobby', { gameMode: 'local', straightToTotals: true });
            }}>
              <View style={{ backgroundColor: 'white', height: 70, borderBottomWidth: 1, borderBottomColor: colors.divider, flexDirection: 'row' }}>
                <View style={{ width: 60, alignItems: 'flex-end', justifyContent: 'center' }}>
                  <Image source={require('./../images/icons/localgame.png')} style={{ width: 45, height: 45 }} />
                </View>
                <View style={{ flex: 3, padding: 5, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ paddingLeft: 10, paddingBottom: 5, fontSize: 18, fontWeight: 'bold' }}>Start Party Game</Text>
                  <Text style={{ color: 'gray', paddingLeft: 10, paddingTop: 5, fontSize: 12 }}>Play offline on one phone</Text>
                </View>
                <View style={{ width: 40, justifyContent: 'center', alignItems: 'center', paddingRight: 10 }}>
                  <Icon name='chevron-right' color={colors.primalpha} size={40} />
                </View>
              </View>
            </TouchableHighlight>

            <TouchableHighlight key="practice" onPress={() => {
              SoundService.playClickSound();
              this.props.navigation.navigate('GameOverview');
            }}>
              <View style={{ backgroundColor: 'white', height: 70, borderBottomWidth: 1, borderBottomColor: colors.divider, flexDirection: 'row' }}>
                <View style={{ width: 60, alignItems: 'flex-end', justifyContent: 'center' }}>
                  <Image source={require('./../images/icons/cat_game.png')} style={{ width: 40, height: 40 }} />
                </View>
                <View style={{ flex: 3, padding: 5, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ paddingLeft: 10, paddingBottom: 3, fontSize: 18, fontWeight: 'bold' }}>Practice</Text>
                  <Text style={{ color: 'gray', paddingLeft: 10, paddingTop: 3, fontSize: 12 }}>Practice any game by yourself</Text>
                </View>
                <View style={{ width: 40, justifyContent: 'center', alignItems: 'center', paddingRight: 10 }}>
                  <Icon name='chevron-right' color={colors.primalpha} size={40} />
                </View>
              </View>
            </TouchableHighlight>

            {UserService().signedIn() ?
              <TouchableHighlight key="share" onPress={() => this.inviteOpponent()}>
                <View style={{ backgroundColor: 'white', height: 70, borderBottomWidth: 1, borderBottomColor: colors.divider, flexDirection: 'row' }}>
                  <View style={{ width: 60, alignItems: 'flex-end', justifyContent: 'center' }}>
                    <Image source={require('./../images/icons/mode-share.png')} style={{ width: 40, height: 40 }} />
                  </View>
                  <View style={{ flex: 3, padding: 5, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ paddingLeft: 10, paddingBottom: 3, fontSize: 18, fontWeight: 'bold' }}>Invite friends</Text>
                    <Text style={{ color: 'gray', paddingLeft: 10, paddingTop: 3, fontSize: 12, textAlign: 'center' }}>Challenge friends who are not yet VS users to play against you</Text>
                  </View>
                  <View style={{ width: 40, justifyContent: 'center', alignItems: 'center', paddingRight: 10 }}>
                    <Icon name='chevron-right' color={colors.primalpha} size={40} />
                  </View>
                </View>
              </TouchableHighlight>
              : null}



          </View>
        </View>
      </View>
    )
  }
}

/*
 <ListItem leftIcon={{ name: 'dice-3', color: '#D50F25', type: 'material-community', style: { marginRight: 15, fontSize: 35 } }} key="3" 
               titleStyle={{ fontSize: 20 }} title='Search random user' subtitle="Search "  hideChevron={true} />
*/