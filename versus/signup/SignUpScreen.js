import React, { Component } from 'react'
import { View, Text, Image, Platform, TouchableHighlight, Animated, Easing, Dimensions, Alert, ActivityIndicator, Linking } from 'react-native'
import { List, ListItem, Button, Icon, SocialIcon } from 'react-native-elements'

import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from "react-native-fcm";


import { Router } from './../../Router'
import SoundService from './../services/SoundService'
import { StorageService } from './../services/StorageService'
import colors from './../../colors';
import constants from './../../constants';
import State from './../services/State';

var SafariView;
if (Platform.OS === 'ios') { require('react-native-safari-view');}


export default class SignUpScreen extends Component {

  fcmToken;
  baseFontSize = Dimensions.get('window').width > 340 ? 18 : 16;

  constructor(props) {
    super(props);
    this.state = { scrollTop: new Animated.Value(500.0), showLoadingIndictor: true, fcmToken: null };
  }

  async componentDidMount() {

    try {
      FCM.requestPermissions();

      FCM.getFCMToken().then(token => {
        if (token) {
          this.setState({ fcmToken: token });
        }

      });

      this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, (token) => {
        if (token) {
          this.setState({ fcmToken: token });
        }
      });
    }
    catch (e) {
      console.warn("firbase token error:" + e);
    }



    await this.sleep(1500);
    SoundService.playCorrectSound();
    this.setState({ showLoadingIndictor: false });
    Animated.timing(this.state.scrollTop,
      { toValue: 0, duration: 1000 }
    ).start();
  }

  // ES7, async/await
  sleep(ms = 0) {
    return new Promise(r => setTimeout(r, ms));
  }

  openUrl(url) {
    SoundService.playClickSound();
    if (SafariView) {
      SafariView.isAvailable()
        .then(SafariView.show({
          url: url
        }))
        .catch(error => {
          console.warn("Safari Webview Not available");
          Linking.openURL(url).catch(err => console.error('An error occurred', err));
        });
    }
    else { // android
      Linking.openURL(url).catch(err => console.error('An error occurred', err));
    }
  }
  //<Image style={{ flex: 1, height: undefined, width: undefined, resizeMode: 'stretch' }} source={require('./../../images/screenshots/background.png')}>

  render() {



    return (
      <View style={{ flex: 1, backgroundColor: colors.primdark, }}>
        <View style={{ flex: 1, justifyContent: 'space-around' }}>

          <View style={{ flex: 1 }} />
          <View style={{ flex: 5 }}>

            <Image style={{ flex: 1, height: undefined, width: undefined, resizeMode: 'contain' }} source={require('./../../images/logo-new.png')} />

          </View>
          <View style={{ flex: 1 }} />


          {this.state.showLoadingIndictor ? <ActivityIndicator animating={true} size="large" style={{ position: 'absolute', bottom: 40, left: 0, right: 0 }} /> : null}


          <Animated.View style={{
            position: 'relative', top: this.state.scrollTop,
            backgroundColor: 'rgba(9, 36, 53, 0.8)', borderColor: colors.black, borderTopWidth: 0
          }}>

            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1, paddingLeft: 15, paddingRight: 15, paddingBottom: 20, paddingTop: 0 }}>
                <Text style={{ textAlign: 'center', fontSize: this.baseFontSize + 12, color: 'white', fontWeight: 'bold', }}>WELCOME!</Text>
                <Text style={{ textAlign: 'center', fontSize: this.baseFontSize + 2, color: '#CED8DF', marginTop: 5 }}>Sign in to play online minigames</Text>
              </View>
            </View>

            <View style={{ height: 70 }}>
              <View style={{ flexDirection: 'row', flex: 1 }} >
                <TouchableHighlight onPress={() => {
                  this.openUrl(constants.socialAddress + '/api/user/login/facebook?fcmtoken=' + this.state.fcmToken);

                }}
                  style={{ flex: 1, height: 70, backgroundColor: "#48629B", marginLeft: 15, marginRight: 7, borderRadius: 5, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Icon name="facebook" type="font-awesome" color="white" size={30} iconStyle={{ paddingLeft: 15 }} />
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: this.baseFontSize - 16, color: '#48629B' }}>s</Text>
                      <Text style={{ fontSize: this.baseFontSize, paddingRight: 0, color: 'white', textAlign: 'center' }}>FACEBOOK</Text>
                      <Text style={{ fontSize: this.baseFontSize - 4, paddingRight: 0, color: 'white', textAlign: 'center', opacity: 0.5 }}>SIGN IN</Text>
                    </View>
                  </View>
                </TouchableHighlight>

                <TouchableHighlight onPress={() => {
                  SoundService.playClickSound();
                  Linking.openURL(constants.socialAddress + '/api/user/login/google?fcmtoken=' + this.state.fcmToken).catch(err => console.error('An error occurred', err));
                }}
                  style={{ flex: 1, height: 70, backgroundColor: "#dd4b39", marginRight: 15, marginLeft: 7, borderRadius: 5, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Icon name="google" type="font-awesome" color="white" size={30} iconStyle={{ paddingLeft: 15 }} />
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: this.baseFontSize - 16, color: "#dd4b39" }}>s</Text>
                      <Text style={{ fontSize: this.baseFontSize, paddingRight: 5, color: 'white', textAlign: 'center' }}>GOOGLE</Text>
                      <Text style={{ fontSize: this.baseFontSize - 4, paddingRight: 5, color: 'white', textAlign: 'center', opacity: 0.5 }}>SIGN IN</Text>
                    </View>
                  </View>
                </TouchableHighlight>
              </View>

            </View>



            <View style={{ marginTop: 15, marginBottom: 20, height: 70 }}>
              <View style={{ flexDirection: 'row', flex: 1 }} >
                <TouchableHighlight onPress={() => {
                  SoundService.playClickSound();
                  this.props.navigation.navigate('SignUpModal', { fcmToken: this.state.fcmToken });
                }}
                  style={{ flex: 1, height: 70, backgroundColor: colors.secbutton, marginLeft: 15, marginRight: 7, borderRadius: 5, alignItems: 'center', flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Icon name="envelope" type="font-awesome" color="white" size={27} iconStyle={{ paddingLeft: 15 }} />
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: this.baseFontSize - 16, color: colors.secbutton }}>s</Text>
                      <Text style={{ fontSize: this.baseFontSize, paddingRight: 5, color: 'white', textAlign: 'center' }}>EMAIL</Text>
                      <Text style={{ fontSize: this.baseFontSize - 4, paddingRight: 5, color: 'white', textAlign: 'center', opacity: 0.3 }}>SIGN UP</Text>
                    </View>
                  </View>
                </TouchableHighlight>

                <TouchableHighlight onPress={async () => {
                  SoundService.playClickSound();
                  let localGame = await StorageService().getLocalGame();
                  if (localGame) {
                    this.props.navigation.navigate('Home');
                  }
                  else {
                    this.props.navigation.navigate('NewGameScreen');
                  }

                }}
                  style={{ flex: 1, height: 70, backgroundColor: "#4c4c4c", marginRight: 15, marginLeft: 7, borderRadius: 5, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Icon name="gamepad" type="font-awesome" color="white" size={27} iconStyle={{ paddingLeft: 15 }} />
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: this.baseFontSize - 16, color: '#DD5044' }}>s</Text>
                      <Text style={{ fontSize: this.baseFontSize, paddingRight: 5, color: 'white', textAlign: 'center' }}>OFFLINE</Text>
                      <Text style={{ fontSize: this.baseFontSize - 4, paddingRight: 5, color: 'white', textAlign: 'center', opacity: 0.5 }}>PLAY</Text>
                    </View>
                  </View>
                </TouchableHighlight>
              </View>

            </View>



          </Animated.View>
        </View>

      </View>
    )
  }
}
//   </Image>


/*  <View style={{ borderColor: colors.black, borderTopWidth: 2, borderBottomWidth: 2 }}>
              <TouchableHighlight onPress={() => {
                SoundService.playClickSound();
                this.props.navigation.navigate('SocialSignInWebview', { url: constants.socialAddress + '/api/user/login/facebook' });
              }} style={{ flexDirection: 'row' }}>
                <View style={{ flexDirection: 'row', flex: 1 }} >
                  <View style={{ flex: 2, backgroundColor: '#48629B', alignItems: 'center', justifyContent: 'center', borderRightWidth: 2 }}>
                    <Icon size={this.baseFontSize + 15} 
                      name='facebook' type="font-awesome" color='white' />
                  </View>
                  <View style={{ flex: 5, padding: 15, }}>
                    <Text style={{ fontSize: this.baseFontSize, color: colors.white }}>Facebook Sign In</Text>
                  </View>
                  <View style={{ width: 40, justifyContent: 'center', alignItems: 'center', paddingRight: 10 }}>
                    <Icon name='chevron-right' color={colors.primlight} size={40} />
                  </View>

                </View>
              </TouchableHighlight>

            </View>
             <View style={{ borderColor: colors.black, borderBottomWidth: 2 }}>
              <TouchableHighlight onPress={() => {
                SoundService.playClickSound();
                this.props.navigation.navigate('SocialSignInWebview', { url: constants.socialAddress + '/api/user/login/facebook' });
              }} style={{ flexDirection: 'row', borderRightWidth: 2 }}>
                <View style={{ flexDirection: 'row', flex: 1 }} >
                  <View style={{ flex: 2, backgroundColor: '#DD5044', alignItems: 'center', justifyContent: 'center', borderRightWidth: 2  }}>
                    <Icon size={this.baseFontSize + 15} 
                      name='google' type="font-awesome" color='white' />
                  </View>
                  <View style={{ flex: 5, padding: 15, }}>
                    <Text style={{ fontSize: this.baseFontSize, color: colors.white }}>Google Sign In</Text>
                  </View>
                  <View style={{ width: 40, justifyContent: 'center', alignItems: 'center', paddingRight: 10 }}>
                    <Icon name='chevron-right' color={colors.primlight} size={40} />
                  </View>

                </View>
              </TouchableHighlight>

            </View>*/
