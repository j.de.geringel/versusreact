

import React, { Component } from 'react'
import { View, Text, Image, TextInput, AsyncStorage, TouchableHighlight } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { FormLabel, FormInput, Button, Icon } from 'react-native-elements'
import { Router } from './../../Router'
import { UserService } from './../services/UserService'

import colors from './../../colors';
import constants from './../../constants';

import RegularHeader from './../components/RegularHeader'

export default class SignUpModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      postInvalid: false,
      emailInvalid: false, email: '',
      userNameInvalid: false, userName: '',
      passwordInvalid: false, password: ''
    };
  }

  validForm = function () {
    // set and reset
    this.state.postInvalid = false;
    this.state.errorMessage = '';
    let allValid = true;

    //email
    var reEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (reEmail.test(this.state.email) || this.state.loginMode) {
      this.setState({ emailInvalid: false })
    }
    else {
      this.setState({ emailInvalid: true })
      allValid = false;
    }
    //username
    var reUser = /^[A-Za-z0-9_-]{4,12}$/;
    if (reUser.test(this.state.userName)) {
      this.setState({ userNameInvalid: false })
    }
    else {
      this.setState({ userNameInvalid: true })
      allValid = false;
    }
    //password
    var rePwd = /^[A-Za-z0-9_-]{5,12}$/;
    if (rePwd.test(this.state.password)) {
      this.setState({ passwordInvalid: false })
    }
    else {
      this.setState({ passwordInvalid: true })
      allValid = false;
    }

    if (allValid) {

      // let's sign him up
      fetch(constants.apiAddress + '/user', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user: {
            email: this.state.email,
            password: this.state.password,
            userName: this.state.userName,
            loginMode: this.state.loginMode,
            fcmToken: this.props.navigation.state.params.fcmToken
          }
        })
      }).then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status == 'success') {
            AsyncStorage.setItem('@versusSuperStore:User', JSON.stringify({ userName: this.state.userName, id: responseJson.data.id })).then(() => {
              UserService().setUser({ userName: this.state.userName, id: responseJson.data.id });
              this.props.navigation.dispatch(NavigationActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({ routeName: 'Home' })
                ]
              }));
            }).done()
          }
          else {
            this.setState({ postInvalid: true, errorMessage: responseJson.message })
          }
        })
        .catch((error) => {
          console.warn(error);
          this.setState({ postInvalid: true })
        });
    }
    return allValid;
  };


  render() {

    let emailWarning = this.state.emailInvalid ? <Text style={{ color: 'red', marginLeft: 63 }}>Not a valid email</Text> : null;
    let userNameWarning = this.state.userNameInvalid ? <Text style={{ color: 'red', marginLeft: 63 }}>Must be 4-12 long, alphanumeric</Text> : null;
    let passwordWarning = this.state.passwordInvalid ? <Text style={{ color: 'red', marginLeft: 63 }}>Must be 5-12 long</Text> : null;
    let errorMessage = this.state.postInvalid ? <Text style={{ color: 'red', marginLeft: 20, marginTop: 10 }}>{this.state.errorMessage ? this.state.errorMessage : 'Something unexpected went wrong!'}</Text> : null;

    let emailComponent;
    if (this.state.loginMode) {
      emailComponent = null;
    }
    else {
      emailComponent = (<View style={{ flexDirection: 'row', marginTop: 20, paddingLeft: 20, alignItems: 'flex-end' }}>
        <Icon size={25} containerStyle={{ paddingBottom: 5 }}
          name='email' color={colors.primlight} />
        <FormInput placeholder="Email" placeholderTextColor={colors.primlight} containerStyle={{ flex: 8 }}
          inputStyle={{ paddingBottom: 7, fontSize: 20, color:colors.white }} keyboardType="email-address"
          onChangeText={(email) => this.setState({ email: email })} maxLength={25} />
      </View>
      );
    }


    return (
      <View style={{ flex: 1, backgroundColor: colors.primdark }}>
        <RegularHeader message={this.state.loginMode ? 'Login' : 'Sign up'} navigation={this.props.navigation} />

        <View style={{ flex: 7, padding: 20, marginTop: 20, }}>
          <View style={{ flexDirection: 'row', paddingLeft: 20 }}>
            <Icon size={25}
              name='account-box' color={colors.primlight} />
            <FormInput placeholder="Username" placeholderTextColor={colors.primlight} containerStyle={{ flex: 8 }}
              inputStyle={{ paddingBottom: 7, fontSize: 20,color:colors.white }} maxLength={12} 
              onChangeText={(userName) => this.setState({ userName: userName })} />

          </View>
          {userNameWarning}

          {emailComponent}
          {emailWarning}

          <View style={{ flexDirection: 'row', paddingLeft: 20, marginTop: 20, }}>
            <Icon size={25} containerStyle={{ paddingBottom: 5 }}
              name='lock-outline' color={colors.primlight} />
            <FormInput placeholder="Password" placeholderTextColor={colors.primlight} inputStyle={{ paddingBottom: 7, fontSize: 20, color:colors.white }} secureTextEntry={true}
              containerStyle={{ flex: 8, padding: 0 }} maxLength={12}
              onChangeText={(password) => this.setState({ password: password })} />
          </View>
          {passwordWarning}

          <Button onPress={() => this.validForm()} buttonStyle={{ marginTop: 35 }} raised title={this.state.loginMode ? 'Login' : 'Sign up'}
            backgroundColor={colors.buttonok} fontSize={22} borderRadius={3} />
          {errorMessage}

          <View>
            <TouchableHighlight onPress={() => this.setState({ loginMode: !this.state.loginMode })}>
              <Text style={{ fontSize: 20, padding: 20, color: colors.divider, textAlign: 'center' }}>
                {this.state.loginMode ? 'No account?' : 'Have an account?'} <Text style={{ fontStyle: 'italic', textDecorationLine: 'underline' }}>{this.state.loginMode ? 'Register!' : 'Log in!'}</Text></Text>
            </TouchableHighlight>
          </View>




        </View>
      </View>
    );
  }
}

