import React, { Component } from 'react'
import { View, Text, Image, TouchableHighlight, ActivityIndicator, Alert, ScrollView } from 'react-native'
import { Icon} from 'react-native-elements'
import ActionButton from 'react-native-action-button';

import { UserService } from './services/UserService'
import { StorageService } from './services/StorageService'
import { XhrGameService } from './services/games/XhrGameService'
import SoundService from './services/SoundService'
import State from './services/State'
import Avatar from './components/Avatar';

import HomeMenu from './components/HomeMenu';
import VersusListItem from './components/VersusListItem';
import colors from './../colors';
import constants from './../constants';


export default class Home extends Component {

  timeout;
  loadingIndicatorTimout;
  showShouldUpdateWarning = false;

  constructor(props) {
    super(props);
    this.state = { games: [], localGame: null, initialLoading: true, xhrLoadSuccess: false, showMenu: false, shouldUpdate: false };
  }

  async componentDidMount() {

    let localGame = await StorageService().getLocalGame();
    this.setState({ localGame: localGame });


    if (UserService().signedIn()) {
      this.setupLoadingIndicator();
      this.getGames();
    }
  }

  /** Activity Indicator:
      - XHr laad succesvol binnen 5 sec -> spinner tot die tijd
      - Xhr komt nooit terug -> 5 seconden spinner, xhr mislukt txt daarna
      - Xhr komt met error terug -> geen spinner en xhr mislukt txt */
  setupLoadingIndicator() {
    loadingIndicatorTimout = setTimeout(() => {
      let loadSucces = this.state.xhrLoadSuccess ? true : false; // if after five seconds no good load, consider xhr problems
      this.setState({ initialLoading: false, xhrLoadSuccess: loadSucces });
    }, 5000); // loading indicator removed forcefully after 5 seconds
  }

  componentWillUnmount() {
    this.timeout ? clearTimeout(this.timeout) : null;
    loadingIndicatorTimout ? clearTimeout(loadingIndicatorTimout) : null;
  }

  /** Poll strategy: 
   *    - when on Home fetch and on answer/error poll after 10 secs 
   *    - when on background do not fetch and poll after 10 secs */
  getGames = function () {

    if (this.props.navigation.state.routeName == 'Home' && UserService().getUser().id) {
      this.showShouldUpdateWarning = false;
      try {
        fetch(constants.apiAddress + '/games?userid=' + UserService().getUser().id + '&version=' + constants.gameVersion)
          .then((response) => response.json())
          .then((responseJson) => {

            if (responseJson.data && responseJson.data.shouldUpdate) { // api app compatible check

              State.onlineBlocked = true;
              this.setState({
                initialLoading: false, xhrLoadSuccess: true, shouldUpdate: true
              });
            }
            else if (responseJson.data) {
              State.onlineBlocked = false;
              this.setState({
                games: responseJson.data, initialLoading: false, xhrLoadSuccess: true,
                shouldUpdate: false
              });
            }
            console.info("GETTING GAMES FROM SUCCESS");
            this.timeout ? clearTimeout(this.timeout) : null;
            this.timeout = setTimeout(() => this.getGames(), 10000);
          })
          .catch((error) => {
            console.warn(error);
            this.setState({ games: responseJson.data, initialLoading: false, xhrLoadSuccess: false });
            console.info("GETTING GAMES FROM ERROR");
            this.timeout ? clearTimeout(this.timeout) : null;
            this.timeout = setTimeout(() => this.getGames(), 10000);
            throw error;
          });
      }
      catch (e) {
        console.warn("CONNECTION PROBLEMS");
      }
    }
    else {
      console.info("GETTING GAMES FROM ELSE");
      this.timeout ? clearTimeout(this.timeout) : null;
      this.timeout = setTimeout(() => this.getGames(), 10000);
    }
  }

  render() {
    let gameRender;
    let localGame;

    let activityIndicator = this.state.initialLoading && UserService().signedIn() ? <ActivityIndicator animating={this.state.initialLoading}
      style={{ alignItems: 'center', justifyContent: 'center', padding: 8, height: 80 }} size="large" /> : null;

    /* LOCAL GAME */
    localGame = this.state.localGame ?
      <TouchableHighlight key="local" onPress={() => {
        SoundService.playClickSound();
        State.currentMatch = { gameMode: 'local', noRounds: 7, dateStarted: new Date(this.state.localGame.date) };
        this.props.navigation.navigate('LobbyScreen', { gameMode: 'local', straightToTotals: true })
      }}
        onLongPress={() => Alert.alert('Delete Game?',
          'You long pressed this game, do you want to delete it?',
          [
            { text: 'Cancel', onPress: () => true },
            {
              text: 'Delete', onPress: async () => {
                await StorageService().removeLocalGame();
                this.setState({ localGame: null });
              }
            }
          ]
        )}
      >
      <VersusListItem image={require('./../images/icons/localgame.png')} subTitle={'Started: ' + new Date(this.state.localGame.date).toISOString().substring(0, 10)} 
         catImage={require('./../images/icons/cat_game.png')} title="Local Game"  /> 
      </TouchableHighlight>

      : null;

    let notifactionText;

    if (!this.state.initialLoading && !this.state.xhrLoadSuccess) {
      notifactionText = <Text style={{ padding: 20, color: colors.dividerDarker, fontStyle: 'italic' }}>Connection problems, online games are not fetched</Text>;
    }
    else if (!this.state.initialLoading && this.state.shouldUpdate) {
      notifactionText =
        <View style={{ backgroundColor: colors.white }}>
          <Text style={{ padding: 20, color: colors.dividerDarker, fontSize: 25, textAlign: 'center' }}>UPDATE AVAILABLE</Text>
          <Text style={{ paddingLeft: 20, paddingRight: 20, paddingBottom: 20, color: colors.primdark, textAlign: 'center' }}>You will not be able to play online untill you update the app. (android: download from http://www.polyprog.io, ios: you will get a notification from appflight to update)!</Text>
        </View>;
    }
    else if (!this.state.initialLoading && (this.state.games.length == 0 || this.state.games.filter((r) => r.gamestatus != 9).length == 0) // this mean no games OR no active games
      && !this.state.localGame) {
      notifactionText = (
        <TouchableHighlight key="newgame" onPress={() => this.props.navigation.navigate('NewGameScreen')}>
          <View style={{ backgroundColor: colors.white, height: 70, padding: 10, borderBottomWidth: 1, borderTopWidth: 1, borderColor: colors.divider, flexDirection: 'row' }}>
            <View style={{ flex: 3, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ color: 'gray' }}>No active games, use the + icon to start a game!</Text>
            </View>
          </View>
        </TouchableHighlight>

      );
    }


    /* ACTIVE ONLINE GAMES */
    let activegames = this.state.games.filter((r) => (r.gamestatus != 7 && r.gamestatus != 8 && r.gamestatus != 9)).map((g, i) => {
      let title;
      let subTitle;
      let image;

      if (g.usergamestatus == 0) { // Open game started
        title = 'Against ' + g.opponentname;
        subTitle = g.lastround ? 'Ready to play round ' + (g.lastround + 1) : 'Ready to play round 1';
        image = require('./../images/icons/cat_game.png');
      }
      else if (g.usergamestatus == 1) { // User finished with games
        title = 'Against ' + g.opponentname;
        subTitle = 'Finished, waiting for opponent';
        image = require('./../images/icons/standings.png');
      }
      else if (g.usergamestatus == 5) { // challenged opponent
        title = 'Against ' + g.opponentname;
        subTitle = 'Waiting for opponent to accept';
        image = require('./../images/icons/hourglass.png');
      }
      else if (g.usergamestatus == 6) { // user challenged
        title = g.opponentname + ' challenged you';
        // two buttons accept or reject
      }
      else if (g.usergamestatus == 7) { // random game waiting
        title = 'Random game';
        subTitle = "Waiting for opponent to join...";
        image = require('./../images/icons/hourglass.png');
      }

      let border = i == 0 ? 0 : 1;

      return <TouchableHighlight key={i} onPress={() => {
        SoundService.playClickSound();
        if (g.usergamestatus != 7 && g.usergamestatus != 6 && g.usergamestatus != 5) {
          State.currentMatch = { gameMode: 'online', gameid: g.gameid, noRounds: 7 };
          this.props.navigation.navigate('LobbyScreen', { straightToTotals: true });
        }
      }} onLongPress={() => {
        if (g.usergamestatus != 6) {
          Alert.alert('Remove Game?',
            'You long pressed this game, do you want to remove it? You will lose.',
            [
              { text: 'Cancel', onPress: () => true },
              {
                text: 'Delete', onPress: async () => {
                  await XhrGameService().removeGameMe(g.gameid, g.opponentid);
                  this.getGames();
                }
              },

            ]);
        }
      }}>
        <View style={{ backgroundColor: 'white', padding: 10, borderTopWidth: border, borderTopColor: colors.divider, flexDirection: 'row' }}>
          <View style={{ width: 54, alignItems: 'center', paddingLeft: 10, justifyContent: 'center' }}>
            {g.usergamestatus != 7 ? <Avatar userid={g.opponentid} username={g.opponentname} image={g.opponentimage} size={50} /> :
              null}
          </View>
          <View style={{ flex: 3, padding: 5, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold', paddingBottom: 2, color:colors.black }}>{title}</Text>
            {g.usergamestatus == 6 ?
              <View style={{ flexDirection: 'row', paddingLeft: 35, paddingRight: 35, height: 45, paddingTop: 5 }}>
                <TouchableHighlight onPress={async () => {
                  SoundService.playClickSound();
                  await XhrGameService().declineChallengeMe(g.gameid);
                  this.getGames();
                }}
                  style={{ flex: 1, borderRadius: 5, backgroundColor: colors.primbutton, justifyContent: 'center', marginRight: 5 }}>
                  <Text style={{ color: colors.white, textAlign: 'center', fontWeight: 'bold' }}>DECLINE</Text>
                </TouchableHighlight>
                <TouchableHighlight onPress={async () => {
                  SoundService.playClickSound();
                  await XhrGameService().acceptChallengeMe(g.gameid);
                  this.getGames();
                }}
                  style={{ flex: 1, borderRadius: 5, backgroundColor: colors.buttonok, justifyContent: 'center', marginLeft: 5 }}>
                  <Text style={{ color: colors.white, textAlign: 'center', fontWeight: 'bold' }}>ACCEPT</Text>
                </TouchableHighlight>
              </View>
              :
              <Text style={{ color: 'gray', marginTop: 2, fontSize: 12 }}>{subTitle}</Text>}
          </View>
          {g.usergamestatus == 6 ?
            null
            :
            <View style={{ width: 50, justifyContent: 'center', alignItems: 'center' }}>
              <Image source={image} style={{ width: 35, height: 35 }} />
            </View>}
        </View>
      </TouchableHighlight>
    });
    let finishedGames = this.state.games.filter((r) => (r.gamestatus == 7 || r.gamestatus == 8 || r.gamestatus == 9)).map((g, i) => {

      let image;
      let subTitle;

      if (g.gamestatus == 9) { // Game ended normally
        image = g.winnername == UserService().getUser().name ? require('./../images/icons/trophy.png') :
          require('./../images/icons/trophy_2nd.png');
        subTitle = g.winnername == UserService().getUser().name ? 'You won! See results' : g.winnername + ' won! See results';
      }
      else if (g.gamestatus == 7) { // opponent quit
        image = require('./../images/icons/trophy.png');
        subTitle = "Opponent quit the game";
      }
      else if (g.gamestatus == 8) {
        // challenge rejected
        image = require('./../images/icons/forbidden.png');
        subTitle = "Challenge rejected";
      }

      return <TouchableHighlight key={i} onPress={() => {
        if (g.gamestatus != 8) {
          SoundService.playClickSound();
          State.currentMatch = { gameid: g.gameid, noRounds: 7 };
          this.props.navigation.navigate('LobbyScreen', { gameEnded: true, straightToTotals: true });
        }
      }}
        onLongPress={() => Alert.alert('Rematch or Delete?',
          'Do you want to challenge your opponent for a rematch or delete the game from view?',
          [{ text: 'Cancel', onPress: () => true },
            {
              text: 'Delete', onPress: async () => {
                await XhrGameService().removeGameMe(g.gameid, null);
                this.getGames();
              }
            },
            {
              text: 'Rematch', onPress: async () => {
                await XhrGameService().startGame(g.opponentid, g.opponentname);
                this.getGames();
              }
            }]
        )}
      >
      
        <View style={{ backgroundColor: 'white', padding: 10, height: 70, borderBottomWidth: 1, borderBottomColor: colors.divider, flexDirection: 'row' }}>
          <View style={{ width: 54, alignItems: 'center', paddingLeft: 10, justifyContent: 'center' }}>
            <Avatar userid={g.opponentid} username={g.opponentname} image={g.opponentimage} size={50} />
          </View>
          <View style={{ flex: 3, padding: 5, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold', paddingBottom: 2, color:colors.black }}>{g.usergamestatus != 7 ? 'Against ' : ''}{g.opponentname}</Text>
            <Text style={{ color: 'gray', marginTop: 2, fontSize: 12 }}>{subTitle}</Text>
          </View>
          <View style={{ width: 50, justifyContent: 'center', alignItems: 'center' }}>
            <Image source={image} style={{ width: 35, height: 35 }} />
          </View>
        </View>

      </TouchableHighlight>
    });

    gameRender = (
      <View>
        {activityIndicator}
        {notifactionText}
        {activegames}
        {this.state.games.filter((r) => r.gamestatus == 9 || r.gamestatus == 8 || r.gamestatus == 7).length > 0 ? //HISTORY
          (<View>
            <View style={{ backgroundColor: colors.primdark, paddingLeft: 20, }}><Text style={{ fontSize: 20, color: colors.white, paddingTop: 8, paddingBottom: 8 }}>HISTORY</Text></View>
            {finishedGames}</View>
          )
          : null}

      </View>
    )

    return (
      <View style={{ flex: 1, backgroundColor: colors.lightbg }}>
        {/* MENU START */}
        <View style={{ height: 50, padding: 5, paddingRight: 20, paddingLeft: 20, backgroundColor: colors.primdark, flexDirection: 'row' }}>
          <Text style={{ fontSize: 20, color: colors.white, paddingTop: 8, paddingBottom: 8 }}>GAMES</Text>
          <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end', flexDirection: 'row' }}>
            <Icon name='menu' color={colors.white} size={38} onPress={() => { SoundService.playClickSound(); this.setState({ showMenu: true }); }} />
          </View>
        </View>
        <HomeMenu show={this.state.showMenu} closeCallback={() => this.setState({showMenu:false})} navigation={this.props.navigation}/>
       
        <ScrollView>
          {UserService().signedIn() == false ?
            <TouchableHighlight key="signin" onPress={() => this.props.navigation.navigate('SignUpScreen')}>
                <VersusListItem image={require('./../images/icons/logo.png')} subTitle="Sign In to play online!" 
                  title="Sign In" backgroundColor={colors.primdark} textColor={colors.white}  /> 
            </TouchableHighlight>
            : null}
          {localGame}
          {gameRender}
        </ScrollView>
        <ActionButton buttonTextStyle={{ color: colors.white }} buttonColor={colors.primdark} onPress={() => { SoundService.playClickSound(); this.props.navigation.navigate('NewGameScreen'); }} />
      </View>
    )
  }
}