/*const _XHR = GLOBAL.originalXMLHttpRequest ?  
    GLOBAL.originalXMLHttpRequest :           
    GLOBAL.XMLHttpRequest                     

XMLHttpRequest = _XHR*/

import React from 'react'

import { View, AsyncStorage, Linking } from 'react-native'
import { UserService } from './services/UserService'
import { gameType } from './services/GameStore'

import State from './services/State'
import { Router } from './../Router';
import Constants from './../constants';

import { NavigationActions } from 'react-navigation'




export default class Initial extends React.Component {

  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    await this.checkOrsetLocalVersion();
    // add event listener for oauth callbacks
    Linking.addEventListener('url', this._handleOpenURL.bind(this));
    this.determineNextScreen();
  }

  componentWillUnmount() {
    Linking.removeEventListener('url', this._handleOpenURL);
  }

  _handleOpenURL(event) { //redirect from oauth provider
    if (event && event.url) {
      let username = event.url.substring(event.url.indexOf('#username=') + 10, event.url.indexOf('&'));
      let userid = event.url.substring(event.url.indexOf('&userid=') + 8);
      console.warn("Storing oauth user:" + username + ", id:" + userid);

      AsyncStorage.setItem('@versusSuperStore:User', JSON.stringify({ userName: username, id: userid })).then(() => {
        UserService().setUser({ userName: username, id: userid });
        this.determineNextScreen();
      }).done()
    }
  }


  async checkOrsetLocalVersion() {

    let savedVersion = await AsyncStorage.getItem('@versusSuperStore:version');

    if (!savedVersion || Constants.localCacheMinVersion > savedVersion) { //force local cache wipe
      await AsyncStorage.clear();
      await AsyncStorage.setItem('@versusSuperStore:version', JSON.stringify(Constants.gameVersion));
    }
  }


  async determineNextScreen() {
    let user = await AsyncStorage.getItem('@versusSuperStore:User');
  
    if (user) {
      UserService().setUser(JSON.parse(user));
      this.props.navigation.dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'Home' })
        ]
      }));
      //State.currentMatch = {gameid: 4};
      //State.currentRound = {roundNumber: 4};
      //this.props.navigation.navigate('GamePicker', { round: 1 });

      //this.props.navigation.navigate('GameResultPage', { resultInfo: {penalties: [{label:'Lives Left x50', value: 1120, icon:'lives'}], score: [{label:'Time left', value: 1174, icon: 'time'}, {label:'Correct x20', value: 1160, icon: 'correct'}], stars: 3.3,  totalScore:1104, playerDied: false }});
    }
    else {
      this.props.navigation.navigate('SignUpScreen');

      //this.props.navigation.navigate('GameSplashScreen', { forcedGameId: 1 });
      //State.currentRound = gameType[6];
      //this.props.navigation.navigate('TwinsGame');
    }
  }

  render() {
    return (<View />
    )
  }
}


