

import React, { Component } from 'react'
import { View, Text, Image, BackAndroid, Alert, TouchableHighlight, Modal, Dimensions } from 'react-native'
import { List, ListItem, Icon } from 'react-native-elements'
import { Router } from './../Router'

import colors from './../colors';

import { ResultService } from './services/results/ResultService';
import { StorageService } from './services/StorageService';
import SoundService from './services/SoundService';
import { getIconForGameCatogery } from './services/IconService';
import State from './services/State';

import { NavigationActions } from 'react-navigation'


export default class GameSplashScreen extends Component {

  baseFontSize = Dimensions.get('window').width > 340 ? 18 : 16;


  constructor(props) {
    super(props);
    this.state = { showScores: false, scores: [], title: '', explain: '' };
    BackAndroid.addEventListener('hardwareBackPress', this._androidback.bind(this));

  }

  /* handle back button  for android */
  _androidback = function () {
    return true;
  }

  /* mount and unmount handle statusbar, backbutton and interval things */
  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', this._androidback);
  }


  async componentDidMount() {

    this.setRoundInformation();

    this.getTop10Scores();
  }

  async getTop10Scores() {
    this.setState({ scores: await StorageService().getTopTen(State.currentRound.id) });
  }

  setRoundInformation() {
    this.setState({ title: State.currentRound.title, explain: State.currentRound.explain, roundType: State.currentRound.type });
  }


  render() {
    let mainContent;

    /** Main content can be either game EXPLAIN or display SCORE mode */
    if (!this.state.showScores) {
      mainContent = <View><Text style={{ color: 'white', fontSize: 40, fontWeight: 'bold' }}>{this.state.title}</Text>
        <Text style={{ color: 'white', fontSize: 19, marginTop: 20 }}>{this.state.explain}</Text></View>
    }
    else {
      mainContent = <View style={{ flex: 1, padding: 10 }}>
        {[1, 2, 3, 4, 5].map((uselessVar, i) => {
          return <View key={i} style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
            <Text style={{ flex: 1, fontSize: 22, paddingRight: 40, color: colors.primlight }}>{i + 1}</Text>
            <Text style={{ flex: 5, color: 'white', fontSize: 22, paddingRight: 40, textAlign: 'center' }}>{this.state.scores.length >= i + 1 ? this.state.scores[i].score : '-'}</Text>
            <Text style={{ flex: 10, color: colors.primlight, fontSize: 20, textAlign: 'center' }}>
              {this.state.scores.length >= i + 1 ? new Date(this.state.scores[i].date).toISOString().substring(0, 10) : '-'}
            </Text>
          </View>
        })}
      </View>

    }

    /** Set the image background, a skewed image of the current game */
    let imageBackground = State.currentRound ?
      <Image source={State.currentRound.image} style={{
        opacity: 0.08, flex: 1, position: 'absolute', left: 200, top: 0,
        width: 248, height: 380, transform: [{ rotate: '-15 deg' }]
      }} /> : null;

    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>

        <View style={{ height: 48, flexDirection: 'row', backgroundColor: colors.primdark }}>
          <TouchableHighlight onPress={() => this.setState({ showScores: false })} style={{ flex: 1 }}>
            <View style={{ height: 48, flex: 1, justifyContent: 'center', borderBottomWidth: this.state.showScores ? 0 : 4, borderColor: colors.primlight }}>
              <Text style={{
                color: this.state.showScores ? colors.primlight : colors.white, textAlign: 'center', fontSize: 18,
                fontWeight: this.state.showScores ? 'normal' : 'bold', paddingTop: this.state.showScores ? 0 : 4
              }}>INFO</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight onPress={() => this.setState({ showScores: true })} style={{ flex: 1 }}>
            <View style={{ height: 48, justifyContent: 'center', flex: 1, borderBottomWidth: this.state.showScores ? 4 : 0, borderColor: colors.primlight }}>
              <Text style={{
                color: this.state.showScores ? colors.white : colors.primlight, textAlign: 'center', fontSize: 18,
                fontWeight: this.state.showScores ? 'bold' : 'normal', paddingTop: this.state.showScores ? 4 : 0
              }}>SCORES</Text>
            </View></TouchableHighlight>
        </View>
        <View style={{ flex: 4, backgroundColor: colors.primdark, justifyContent: 'center', padding: 20 }}>
          {mainContent}
          {imageBackground}
        </View>

        <View style={{ flex: 4, padding: 20, backgroundColor: 'white' }}>
          <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'space-around' }}>
            <View style={{ flexDirection: 'row' }}>
              <Image source={getIconForGameCatogery(this.state.roundType).value} style={{ width: 55, height: 55, marginRight: 10 }} />
              <View style={{ flexDirection: 'column', paddingTop: 10, height: 45, alignItems: 'center' }}>
                <Text style={{ flex: 3, color: colors.dividerDarker }}>TYPE</Text>
                <Text style={{ flex: 3, color: colors.primdark }}>{this.state.roundType}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Image source={require('./../images/icons/trophy.png')} style={{ width: 55, height: 55, marginRight: 10 }} />
              <View style={{ flexDirection: 'column', paddingTop: 10, height: 45, alignItems: 'center' }}>
                <Text style={{ flex: 3, color: colors.dividerDarker }}>BEST SCORE</Text>
                <Text style={{ flex: 3, color: colors.primdark }}>{this.state.scores.length > 0 ? this.state.scores[0].score : '-'}</Text>
              </View>
            </View>
          </View>

          <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>



            <TouchableHighlight style={{ height: 65, flex: 1, marginRight: 7 }}
              onPress={() => {
                SoundService.playClickSound();

                this.props.navigation.dispatch(NavigationActions.reset({
                  index: 0,
                  actions: [
                    NavigationActions.navigate({ routeName: 'Initial' })
                  ]
                }))
              }}>
              <View style={{ flex: 1, backgroundColor: colors.primbutton, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontSize: 22, color: colors.white }}>QUIT</Text>
              </View>
            </TouchableHighlight>

            <TouchableHighlight style={{ height: 65, flex: 1, marginLeft: 7 }}
              onPress={() => {
                SoundService.playClickSound();
                this.props.navigation.navigate(State.currentRound.routeName);
              }}>
              <View style={{ flex: 1, backgroundColor: colors.buttonok, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontSize: 22, color: colors.white }}>START</Text>
              </View>
            </TouchableHighlight>



          </View>
        </View>

        <View style={{ flex: 1, justifyContent: 'flex-end', backgroundColor: 'black' }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
            <View>
              <Icon color='white' name='rowing' size={this.baseFontSize * 2} />
              <Text style={{
                marginTop: -10, position: 'absolute', top: 10, left: 30, color: 'white', backgroundColor: colors.primmed,
                borderRadius: 5
              }}> 0 </Text>
            </View>
            <View>
              <Icon style={{}} color='white' name='filter-2' size={this.baseFontSize * 2} />
              <Text style={{
                marginTop: -10, position: 'absolute', top: 10, left: 30, color: 'white', backgroundColor: colors.primmed,
                borderRadius: 5
              }}> 0 </Text>
            </View>
            <View>
              <Icon style={{}} color='white' name='alarm' size={this.baseFontSize * 2} />
              <Text style={{
                marginTop: -10, position: 'absolute', top: 10, left: 30, color: 'white', backgroundColor: colors.primmed,
                borderRadius: 5
              }}> 0 </Text>
            </View>
          </View>
        </View>
      </View >
    )
  }
}