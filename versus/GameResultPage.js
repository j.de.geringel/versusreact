import React, { Component } from 'react'
import { View, Text, Animated, Easing, Image, Dimensions } from 'react-native'
import { Icon, Button } from 'react-native-elements'
import { Router } from './../Router'

import colors from './../colors';
import { ResultService } from './services/results/ResultService';
import { StorageService } from './services/StorageService';
import SoundService from './services/SoundService';
import State from './services/State';
import { NavigationActions } from 'react-navigation'


export default class GameResultPage extends Component {

    pbResult = 0;

    statusImages = {
        lives: require('./../images/icons/shapes.png'), // favorite
        correct: require('./../images/icons/gamescore_check.png'), // check-box
        time: require('./../images/icons/cat_speed.png'), // watch-later
        levelPerfect: require('./../images/icons/gamescore_perfect.png'), // done-all
        levelCleared: require('./../images/icons/gamescore_cleared.png'), // videogame-asset
    }

    starSize;


    resultInfoParam = this.props.navigation.state.params.resultInfo;

    constructor(props) {
        super(props);
        // here we decide which game to play
        this.springValue1 = new Animated.Value(1);
        this.springValue2 = new Animated.Value(1);
        this.springValue3 = new Animated.Value(1);
        this.totalValue = new Animated.Value(1);

        this.state = {
            star1: new Animated.Value(0.0),
            star2: new Animated.Value(0.0),
            star3: new Animated.Value(0.0),
            star4: new Animated.Value(0.0),
            star5: new Animated.Value(0.0),
            inactiveStarColor: colors.black, // all 4 used to hide star elements on start
            starResultColor: colors.primdark,
            starResultText: 'Calculating score..',
            statInfoColor: colors.primdark,
            displayScore: 0,
            penaltyColor: colors.white,
            scoreColor: [colors.white, colors.white, colors.white],
            submitButtonDisabled: true,
            pbResult: 0
        };

        var { width } = Dimensions.get('window');
        this.starSize = Math.floor(width / 6);


    }

    componentDidMount() {
        this.postResults();
        this.scoreAnimation();
    }

    async postResults() {

        ResultService().postResult(State.currentMatch.gameid, this.resultInfoParam.stars, this.resultInfoParam.totalScore,
            State.currentRound.roundNumber, State.currentMatch.noRounds);
        if (State.currentMatch.gameMode == 'online') {
            // save score locally when within top 10...
            this.pbResult = await StorageService().postResultsInTopTen(State.currentRound.id, this.resultInfoParam.totalScore);
        }
    }

    async scoreAnimation() {
        await this.sleep(800);
        this.setState({ scoreColor: [colors.primdark, colors.white, colors.white] });
        this.showSpringAnimation(this.springValue1);
        SoundService.playPopSound();
        this.setState({ displayScore: this.resultInfoParam.score[0].value });
        if (this.resultInfoParam.score.length > 1) {
            await this.sleep(800);
            SoundService.playPopSound();
            this.showSpringAnimation(this.springValue2);
            this.setState({ scoreColor: [colors.primdark, colors.primdark, colors.white] });
            this.setState({ displayScore: this.resultInfoParam.score[0].value + this.resultInfoParam.score[1].value });
        }
        await this.sleep(800);
        this.showSpringAnimation(this.springValue3);
        SoundService.playPopSound();
        this.setState({ penaltyColor: colors.primbutton, scoreColor: [colors.primdark, colors.primdark, colors.primdark] });
        this.setState({ displayScore: this.resultInfoParam.totalScore, pbResult: this.pbResult }); // pb result now set
        await this.sleep(800);
        this.setState({ inactiveStarColor: colors.black });
        this.starAnimations(this.resultInfoParam.stars);
    }

    async starAnimations(value) {

        let starSizeCalc = (this.starSize - 4);
        value > 1 ? SoundService.playAscending() : null;
        await this.sleep(250);

        if (value > 1) {
            this.fillStar(this.state.star1, starSizeCalc);
            await this.sleep(250);
        }
        else { this.fillStar(this.state.star1, starSizeCalc * value) }


        if (value > 2) {
            this.fillStar(this.state.star2, starSizeCalc);
            await this.sleep(250);
        }
        else if (value > 1) { this.fillStar(this.state.star2, starSizeCalc * (value - 1)); }

        if (value > 3) {
            this.fillStar(this.state.star3, starSizeCalc);
            await this.sleep(250);
        }
        else if (value > 2) { this.fillStar(this.state.star3, starSizeCalc * (value - 2)); }

        if (value > 4) {
            this.fillStar(this.state.star4, starSizeCalc);
            await this.sleep(250);
        }
        else if (value > 3) { this.fillStar(this.state.star4, starSizeCalc * (value - 3)); }

        if (value == 5) {
            this.fillStar(this.state.star5, starSizeCalc);
            await this.sleep(250);
        }
        else if (value > 4) { this.fillStar(this.state.star5, starSizeCalc * (value - 4)); }

        value > 1 ? SoundService.stopAscending() : null;

        this.activateStarLabelsAndButton();
    }

    activateStarLabelsAndButton() {
        this.setState({
            starResultColor: colors.white, statInfoColor: colors.primlight,
            starResultText: 'You scored ' + this.resultInfoParam.stars + ' stars!',
            submitButtonDisabled: false
        })
    }

    async fillStar(starAniVar, toValue) {

        Animated.timing(starAniVar, {
            toValue: toValue,
            duration: 250,
            easing: Easing.linear
        }).start();
    }

    // ES7, async/await
    sleep(ms = 0) {
        return new Promise(r => setTimeout(r, ms));
    }

    showSpringAnimation(animationValue) {
        this.totalValue.setValue(0.7)
        Animated.spring(
            this.totalValue,
            {
                toValue: 1,
                friction: 2
            }
        ).start();

        animationValue.setValue(0.7)
        Animated.spring(
            animationValue,
            {
                toValue: 1,
                friction: 2
            }
        ).start();
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: 1, backgroundColor: colors.primdark }}>

                    <View style={{ height: 50, padding: 5, paddingRight: 20, paddingLeft: 20, backgroundColor: colors.primdark, flexDirection: 'row' }}>
                        <Text style={{ flex: 1, color: colors.white, textAlign: 'center', paddingTop: 5, fontSize: 22 }}>RESULTS</Text>
                    </View>

                    <View style={{ paddingLeft: 10, paddingRight: 15, paddingTop: 10, paddingBottom: 5, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
                        {this.resultInfoParam.score.map((score, i) => {
                            return (
                                <View key={i} style={{ height: 45, flexDirection: 'row' }}>
                                    <View style={{ alignItems: 'center', justifyContent: 'center', flex: 2 }}>
                                        <Image source={this.statusImages[score.icon]}
                                            style={{ height: 32, width: 32 }} />
                                    </View>
                                    <View style={{ flex: 11, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 20, color: colors.primdark }}>{score.label}</Text>
                                    </View>
                                    <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
                                        <Animated.Text style={{ fontSize: 20, fontWeight: 'bold', color: this.state.scoreColor[i], transform: [{ scale: i == 0 ? this.springValue1 : i == 1 ? this.springValue2 : this.springValue3 }] }}>{score.value ? score.value : 0}</Animated.Text>
                                    </View>
                                </View>
                            )
                        })}
                        {this.resultInfoParam.penalties.map((penalty, i) => {
                            return (
                                <View key={i} style={{ backgroundColor: 'white', height: 45, flexDirection: 'row' }}>
                                    <View style={{ alignItems: 'center', justifyContent: 'center', flex: 2 }}>
                                        <Image source={this.statusImages[penalty.icon]}
                                            style={{ height: 32, width: 32, paddingRight: 20 }} />
                                    </View>

                                    <View style={{ flex: 11, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 20, color: colors.primdark }}>{penalty.label}</Text>
                                    </View>
                                    <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
                                        <Animated.Text style={{ fontSize: 20, fontWeight: 'bold', color: this.state.penaltyColor, transform: [{ scale: this.springValue3 }] }}>{'-' + penalty.value}</Animated.Text>
                                    </View>
                                </View>
                            )
                        })}
                        <View style={{ backgroundColor: 'white', height: 45, borderTopWidth: 0, borderTopColor: colors.divider, flexDirection: 'row' }}>
                            <View style={{ flex: 2 }} />
                            <View style={{ flex: 11, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 21, fontWeight: 'bold', color: colors.black }}>Total points</Text>
                            </View>
                            <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center', }}>

                                <Animated.Text style={{ fontSize: 22, fontWeight: 'bold', color: colors.black, transform: [{ scale: this.totalValue }] }}>{this.state.displayScore}</Animated.Text>
                            </View>
                        </View>

                    </View>
                    {/* NOTIFIACTION VBAR  */}
                    {this.state.pbResult ?
                        (<View style={{ flexDirection: 'row', backgroundColor: colors.primbutton, paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15, alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={require('./../images/icons/trophy.png')} style={{ height: 20, width: 20, marginRight: 10 }} />
                            <Text style={{ color: colors.white }}>{this.state.pbResult == 1 ? 'New personal best!' : this.state.pbResult == 2 ? "That's your second best ever!" : "That's your " + this.state.pbResult + "th score ever"}</Text>
                        </View>)
                        : State.currentMatch.gameMode != 'online' ? 
                          (<View style={{ flexDirection: 'row', backgroundColor: colors.secbutton, paddingTop: 5, paddingBottom: 5, alignItems: 'center', justifyContent: 'center' }}>
                            <Icon name="info" color={colors.white} size={20} iconStyle={{marginRight: 10 }}/>
                            <Text style={{ color: colors.white }}>Offline game: high scores not saved</Text>
                        </View>)                       
                        :null}

                    <View style={{ backgroundColor: colors.primdark, paddingTop: 20, paddingLeft: 20, paddingRight: 20, flex: 3, justifyContent: 'center' }}>

                        <View style={{ paddingLeft: 20, paddingRight: 20, justifyContent: 'space-around' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', height: 60 }}>
                                <View style={{ flex: 1 }}>
                                    <View style={{ position: 'absolute', left: 0, top: 0 }}>
                                        <Icon name="star" type="font-awesome" color={this.state.inactiveStarColor} size={this.starSize} />
                                    </View>
                                    <Animated.View style={{ position: 'absolute', left: 0, top: 0, width: this.state.star1, overflow: 'hidden' }}>
                                        <Icon name="star" type="font-awesome" color='gold' size={this.starSize} />
                                    </Animated.View>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <View style={{ position: 'absolute', left: 0, top: 0 }}>
                                        <Icon name="star" type="font-awesome" color={this.state.inactiveStarColor} size={this.starSize} />
                                    </View>
                                    <Animated.View style={{ position: 'absolute', left: 0, top: 0, width: this.state.star2, overflow: 'hidden' }}>
                                        <Icon name="star" type="font-awesome" color='gold' size={this.starSize} />
                                    </Animated.View>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <View style={{ position: 'absolute', left: 0, top: 0 }}>
                                        <Icon name="star" type="font-awesome" color={this.state.inactiveStarColor} size={this.starSize} />
                                    </View>
                                    <Animated.View style={{ position: 'absolute', left: 0, top: 0, width: this.state.star3, overflow: 'hidden' }}>
                                        <Icon name="star" type="font-awesome" color='gold' size={this.starSize} />
                                    </Animated.View>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <View style={{ position: 'absolute', left: 0, top: 0 }}>
                                        <Icon name="star" type="font-awesome" color={this.state.inactiveStarColor} size={this.starSize} />
                                    </View>
                                    <Animated.View style={{ position: 'absolute', left: 0, top: 0, width: this.state.star4, overflow: 'hidden' }}>
                                        <Icon name="star" type="font-awesome" color='gold' size={this.starSize} />
                                    </Animated.View>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <View style={{ position: 'absolute', left: 0, top: 0 }}>
                                        <Icon name="star" type="font-awesome" color={this.state.inactiveStarColor} size={this.starSize} />
                                    </View>
                                    <Animated.View style={{ position: 'absolute', left: 0, top: 0, width: this.state.star5, overflow: 'hidden' }}>
                                        <Icon name="star" type="font-awesome" color='gold' size={this.starSize} />
                                    </Animated.View>
                                </View>

                            </View>
                            <View style={{ marginTop: 15 }}>
                                <Text style={{ textAlign: 'center', fontSize: Math.floor(this.starSize / 2.5), color: this.state.starResultColor }}>{this.state.starResultText}</Text>
                                <Text style={{ textAlign: 'center', fontSize: 16, opacity: 0.8, paddingTop: 3, color: this.state.statInfoColor }}>{'Better then ' + Math.round((this.resultInfoParam.stars * 20) * 100) / 100 + '% of the users!'}</Text>
                            </View>
                        </View>

                    </View>
                    <View style={{ justifyContent: 'center', backgroundColor: colors.primdark, marginBottom: 20 }}>
                        <Button onPress={() => {
                            SoundService.playClickSound();
                            if (!this.state.submitButtonDisabled) {
                                var route = State.currentMatch.gameMode == 'practice' ? 'GameOverview' : 'LobbyScreen';
                                this.props.navigation.dispatch(NavigationActions.reset({
                                    index: 0,
                                    actions: [
                                        NavigationActions.navigate({ routeName: route })
                                    ]
                                }));
                            }
                        }
                        }

                            large raised title="TO LOBBY" backgroundColor={this.state.submitButtonDisabled ? colors.primdark : colors.buttonok}
                            color={this.state.submitButtonDisabled ? colors.primdark : colors.white}
                            fontSize={22} borderRadius={3} buttonStyle={{ paddingLeft: 40, paddingRight: 40 }} />
                    </View>




                </View>

            </View>

        )
    }
}
