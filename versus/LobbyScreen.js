
import React, { Component } from 'react'
import { View, Text, Image, ScrollView, TouchableHighlight, ActivityIndicator, Modal } from 'react-native'
import { List, ListItem, Icon, Button } from 'react-native-elements'
import { Router } from './../Router'

import colors from './../colors';
import { ResultService } from './services/results/ResultService';
import { UserService } from './services/UserService';
import SoundService  from './services/SoundService';
import { GameStore } from './services/GameStore';
import State from './services/State';
import Avatar from './components/Avatar';
import RegularHeader from './components/RegularHeader'
import { NavigationActions } from 'react-navigation'


export default class LobbyScreen extends Component {

  // used for determining how many tabs to display and more
  userMaxRound;
  nextRound;
  passPhoneNextScreen;


  constructor(props) {
    super(props);
    this.state = { results: [], round: '', totalMode: false, passPhoneModalVisible: false, localPlayerName: '' };
  }

  async componentDidMount() {
    //await this.getRounds(null, this.props.navigation.state.params.straightToTotals);
    let straightToTotals = this.props.navigation.state.params ? this.props.navigation.state.params.straightToTotals : null;
    rounds = await ResultService().getLatestResults(State.currentMatch.gameid, UserService().getUser().id);

    this.userMaxRound = await ResultService().getMaxRoundMe(rounds);
    this.nextRound = rounds.nextRound ? rounds.nextRound : this.userMaxRound + 1; // FOR LOCAL GAMESERVICE GIVES BACK THE NEXT ROUND
    this.setRoundInformation(rounds, straightToTotals, this.userMaxRound);
  }


  async getSpecificRound(roundToGet) {
    rounds = await ResultService().getResultForRound(State.currentMatch.gameid, roundToGet);
    this.setRoundInformation(rounds, null, roundToGet);
  }


  async setRoundInformation(rounds, straightToTotals, roundToView) {

    this.setState({ results: [] }); // also a xhractivity flag

    /* logic to determine which round we are in */
   
    if (rounds && rounds.length > 0) {
      if (straightToTotals && this.userMaxRound > 1) { // no need to set game info, we go straight to totals
        this.setState({ totalMode: true, results: rounds, round: this.userMaxRound + 1 });
      }
      else { // set round information
        
        let roundGameTitle = roundToView ? await GameStore().getRandomGame(State.currentMatch.gameid, roundToView) : null;
        this.setState({
          totalMode: false, results: rounds, round: roundToView, gameTitle: roundGameTitle ? roundGameTitle.title : ''
        });
      }
    }
  }

  async getTotals(round) {
    this.setState({ totalMode: true, round: this.userMaxRound + 1 });
  }

  scroll = function () {
    if (this.userMaxRound > 3) {
      this.refs.scrollView.scrollTo({ x: (120 * this.state.round) - 220, animated: true });
    }
  }

  async setNextRoudInState() {
    State.currentRound = await GameStore().getRandomGame(State.currentMatch.gameid, this.nextRound); //pick pseudo random game
    State.currentRound.roundNumber = this.nextRound;
  }

  async determineNextRoundAction() {
    if (State.currentMatch.gameMode == 'local') {
      this.passPhoneNextScreen = State.currentRound.sameRound ? 'GameSplashScreen' : 'GamePicker';
      this.setState({ passPhoneModalVisible: true, localPlayerName: State.currentRound.localPlayerName });
    }
    else {
      this.props.navigation.dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'GamePicker' })
        ]
      }));
    }
  }


  render() {

    /** ROUND TABS GENERATION START */
    let tabwidth;
    if (this.userMaxRound > 0) {
      tabwidth = this.userMaxRound < 3 ? { flex: 1 } : { width: 120 }; // round 1:1 column, round 2: 3 (1,2 AND total)
    }

    let roundRenders = [];


    for (let r = 0; r < this.userMaxRound; r++) {
      if (this.state.round == r + 1) { //currently selected round
        roundRenders.push(<View key={r} style={[{
          height: 48, justifyContent: 'center', borderBottomWidth: 4, borderColor: colors.primlight
        }, tabwidth]}>
          <Text style={{ color: 'white', textAlign: 'center', fontSize: 18, fontWeight: 'bold', paddingTop: 4 }}>{'ROUND ' + (r + 1)}</Text>
        </View>);
      }
      else {
        roundRenders.push(<TouchableHighlight key={r} onPress={() => this.getSpecificRound(r + 1)} style={tabwidth}>
          <View style={[{ height: 48, justifyContent: 'center' }, tabwidth]}>
            <Text style={{ color: colors.primlight, textAlign: 'center', fontSize: 18, }}>{'ROUND ' + (r + 1)}</Text>
          </View></TouchableHighlight>);
      }
    }

    if (roundRenders.length > 1) { // add total tab
      if (this.state.totalMode) { //currently selected
        roundRenders.push(
          <View key={999} style={[{ height: 48, justifyContent: 'center', borderBottomWidth: 4, borderColor: colors.primlight }, tabwidth]}>
            <Text style={{ color: colors.divider, textAlign: 'center', fontSize: 18, fontWeight: 'bold', paddingTop: 4 }}>TOTALS</Text>
          </View>);
      }
      else {
        roundRenders.push(<TouchableHighlight key={999} onPress={() => this.getTotals()} style={tabwidth}>
          <View style={[{ height: 48, justifyContent: 'center' }, tabwidth]}>
            <Text style={{ color: colors.primlight, textAlign: 'center', fontSize: 18, }}>TOTALS</Text>
          </View></TouchableHighlight>);
      }
    }

    let header; // the three header modes, lobby, fixed or scrollable
    if (this.userMaxRound > 2) {
      header = (<View style={{ height: 48 }}><ScrollView ref="scrollView" style={{ flexDirection: 'row', backgroundColor: colors.primdark, }}
        horizontal={true} scrollToEnd={true} showsHorizontalScrollIndicator={false} onContentSizeChange={() => this.scroll()}>
        {roundRenders}
      </ScrollView></View>);
    }
    else if (this.userMaxRound > 0) {
      header = (<View style={{ height: 48, flexDirection: 'row', backgroundColor: colors.primdark, }}>{roundRenders}</View>);
    }

    /** ROUND TAB GENERATION END */

    /** RESULT RENDER START */
    if (this.state.totalMode) {
      this.state.results.sort((a, b) => b.totalscore - a.totalscore);
    }
    else {
      this.state.results.sort((a, b) => b.score - a.score);
    }
    let activityIndicator = this.state.results.length == 0 ? <ActivityIndicator animating={this.state.initialLoading}
      style={{ alignItems: 'center', justifyContent: 'center', padding: 8, height: 80 }} size="large" /> : null;

    let resultRender = (

      this.state.results.map((r, i) => {
        let border = i == 0 ? 0 : 1;
        let iconColor = i == 0 ? '#FFD700' : 'gray';

        if (this.state.totalMode) {
          return (
            <View key={i} style={{ height: 80, paddingTop: 10, borderTopWidth: border, borderTopColor: colors.divider, flexDirection: 'row' }}>
              <View style={{ width: 60, alignItems: 'center', }}>
                <Avatar userid={r.userid} username={r.username} image={r.image} />
              </View>
              <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 20 }}>
                <Text style={{ color: 'gray', fontSize: 25, height: 33 }}>{r.username}</Text>
                <Text style={{ flex: 2, fontSize: 16, paddingTop: 3 }}>{r.usergamestatus == 1 ? 'Finished all games' : r.lastround ? '@round ' + r.lastround : 'not started'}</Text>
              </View>
              <View style={{ width: 85, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 32, color: colors.primlight }}>{r.totalscore ? r.totalscore : '-'}</Text>
              </View>
            </View>)

        }
        else {
          return (
            <View key={i} style={{ height: 80, paddingTop: 10, borderTopWidth: border, borderTopColor: colors.divider, flexDirection: 'row' }}>
              <View style={{ width: 60, alignItems: 'center', }}>
                <Avatar userid={r.userid} username={r.username} image={r.image} />
              </View>
              <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 20 }}>
                <Text style={{ color: 'gray', fontSize: 25, height: 33 }}>{r.username}</Text>
                <Text style={{ flex: 2, fontSize: 16, paddingTop: 3 }}>{r.points === -1 ? 'started, not finished' : r.points !== null ? r.points + ' points' : r.lastround ? '@round ' + r.lastround : 'not started'}</Text>
              </View>
              <View style={{ width: 85, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 32, color: colors.primlight }}>{r.stars !== null ? r.stars : '-'}</Text>
              </View>
            </View>)
        }
      }));

    /** RESULT RENDER END */
    let nextGameButton = null;

    if (this.state.results.length != 0) {
      if (this.nextRound > State.currentMatch.noRounds || 
      (this.props.navigation.state.params && this.props.navigation.state.params.gameEnded)) {
        nextGameButton = <Text style={{ fontSize: 30, color: colors.dividerDarker }}>Game finished!</Text>
      }
      else {
        nextGameButton = <Button onPress={async () => {
          SoundService.playClickSound();
          await this.setNextRoudInState();
          this.determineNextRoundAction();
        }}
          raised large icon={{ name: 'videogame-asset', size: 35 }} title={'Round ' + (this.nextRound) + ' of ' + State.currentMatch.noRounds} backgroundColor={colors.buttonok}
          fontSize={23} borderRadius={3} />
      }
    }

    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>

        <Modal animationType={"none"} transparent={false} visible={this.state.passPhoneModalVisible} onRequestClose={() => this.state.passPhoneModalVisible = false} >
          <View style={{ flex: 1 }}>
            <View style={[{ opacity: 1.0, backgroundColor: colors.primdark, flex: 1 }]}>
              <View style={{ flex: 4, justifyContent: 'center', alignItems: 'center', padding: 20 }}>
                <Text style={{ color: 'white', fontSize: 20, textAlign: 'center' }}>Pass the phone to</Text>
                <Text style={{ color: 'white', fontSize: 45, fontWeight: 'bold', textAlign: 'center', paddingTop: 30, marginBottom: 30 }}>
                  {this.state.localPlayerName}
                </Text>
                <Button onPress={() => {
                  //this.setState({ passPhoneModalVisible: false });
                  this.props.navigation.dispatch(NavigationActions.reset({
                    index: 0,
                    actions: [
                      NavigationActions.navigate({ routeName: this.passPhoneNextScreen })
                    ]
                  }));
                   
                  
                }}  raised large title="READY" backgroundColor={colors.buttonok} fontSize={22} borderRadius={3} buttonStyle={{ paddingLeft: 50, paddingRight: 50 }} />
              </View>
              <View style={{ flex: 1 }} />
            </View>
          </View>
        </Modal>
        <RegularHeader message={this.state.totalMode ? 'Totals' : this.state.round > 0 ? 'R' + this.state.round + ': ' + this.state.gameTitle : 'Lobby'} navigation={this.props.navigation} />
        {header}

        <View style={{ flex: 6, paddingLeft: 20, paddingRight: 20, paddingTop: 20, paddingBottom: 0 }}>
          <View style={{ paddingTop: 0, flexDirection: 'row', }}>
            <View style={{ width: 60 }}></View>
            <View style={{ flex: 3, }}></View>
            <View style={{ width: 85, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: colors.primlight, fontSize: 22 }}>{this.userMaxRound > 0 ? 'result' : ''}</Text>
            </View>

          </View>
          {resultRender}
          {activityIndicator}
        </View>

        <View style={{ flex: 2, alignItems: 'center', }}>
          {nextGameButton}
        </View>
      </View>
    )
  }
}