
import React, { Component } from 'react'
import { View, Text, TouchableHighlight, Animated } from 'react-native'

import SoundService from './../services/SoundService';

import colors from './../../colors';
import GameScoreStatusBar from './common/GameScoreStatusBar';
import TimeLeftBar from './common/TimeLeftBar';
import BaseGame from './common/BaseGame';
import FinishedModal from './common/FinishedModal';


export default class CounterGame extends BaseGame {

  correctClicks = 0;
  sortedValues = [];

  constructor(props) {
    // init the app
    super(props);
    this.state = { lives: 3, score: 0, bounceValue: new Animated.Value(1), elements: [] };

    // make tiles object
    for (i = 0; i < 12; i++) {
      number = Math.floor(Math.random() * 99) + 1;
      // check if number is not already present
      var duplicate = false;
      for (s = 0; s < this.state.elements.length; s++) {
        if (number == this.state.elements[s].number) {
          duplicate = true;
        }
      }
      let numColor = [colors.primdark, colors.primbutton, colors.secbutton, colors.buttonok, colors.primlight][Math.floor(Math.random() * 5)];

      // substract index if duplicat, push otherwise
      duplicate ? i = i - 1 : this.state.elements.push({ number: number, display: true, color: numColor });

    };

    // sortedValues
    for (x = 0; x < this.state.elements.length; x++) {
      this.sortedValues.push(this.state.elements[x].number);
    }
    this.sortedValues = this.sortedValues.sort((a, b) => a - b);
  }


  checkNumber = function (number, arIndex) {

    if (this.sortedValues[this.correctClicks] == number) { //correct
      SoundService.playPopSound();
      let newElements = this.state.elements.slice();
      newElements[arIndex].display = false;
      this.correctClicks = this.correctClicks + 1;
      this.setState({ elements: newElements, score: this.state.score + 5 });
      this.refs.gameScoreStatusBar.setSuccesNotification('+5');

      // GAME FINISHED
      if (this.correctClicks == 12) {
        this.gameFinishedHandler();
      }
    }
    else {
      SoundService.playWrongSound();
      this.shakeScreen();
      if (this.state.lives <= 1) { //gameover
        this.gameFinishedHandler(true);
      }
      this.setState({ lives: this.state.lives - 1 });

    }
  }

  timeUpCallBack() {
    this.gameFinishedHandler(true);
  }

  gameFinishedHandler(playerDied) {
    let resultInfo = { penalties: [], score: [], stars: 0, totalScore: 0, playerDied: playerDied };
    this.refs.timeLeftBar.stopAnimation();

    let timeLeft = 0;
    if (!playerDied) { // only counts when survived the game
      timeLeft = this.refs.timeLeftBar.getTimeLeft() ? this.refs.timeLeftBar.getTimeLeft() : 0;
    }
    resultInfo.score.push({ label: 'Time Left', value: timeLeft, icon: 'time' });

    let correctClicks = this.correctClicks * 5;
    resultInfo.score.push({ label: 'Correct x5', value: correctClicks, icon: 'correct' });

    let misClicks = playerDied ? 60 : this.state.lives < 3 ? (3 - this.state.lives) * 20 : 0;
    resultInfo.penalties.push({ label: 'Lives Lost x20', value: misClicks, icon: 'lives' });

    //max score 120: 60 timeleft, 60 correct = 120
    let score = timeLeft + correctClicks - misClicks;

    /** Stars CALCULATION */
    let stars = 0;
    if (score > 110) {
      stars = 5;
    }
    else { // gaan we van 5 naar 0 sterren in 50 seconden 0.1 ster per secodnden
      stars = 1 + (score * (4 / 110));
    }
    resultInfo.stars = stars;
    resultInfo.totalScore = score;

    /** EINDE SCORE CALCULATION */
    this.postResult(resultInfo);
  }

  render() {
    let numbers = [];
    for (var i = 0; i < this.state.elements.length; i++) {
      let el = this.state.elements[i];
      let index = i;

      if (el.display) {
        numbers.push(
          <TouchableHighlight style={{ flex: 1 }} onPress={() => this.checkNumber(el.number, index)}>
            <View style={{ flex: 1, backgroundColor: el.color, justifyContent: 'center', borderWidth: 1, borderColor: 'black' }} >
              <Text style={{ textAlign: 'center', fontSize: 55, color: 'white', fontWeight: 'bold' }}>{el.number}</Text>
            </View>
          </TouchableHighlight>
        );
      }
      else {
        numbers.push(<View style={{ flex: 1, backgroundColor: 'black' }}></View>);
      }
    }

    // view
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <FinishedModal show={this.state.finishedOverlay} />
        <GameScoreStatusBar ref="gameScoreStatusBar"  navigation={this.props.navigation} lives={this.state.lives} score={this.state.score} />
        <TimeLeftBar ref="timeLeftBar" timeUpCallback={this.timeUpCallBack.bind(this)} style={{ height: 20 }} timeLimit={60000} />
        <Animated.View style={{ flex: 1, transform: [{ scale: this.state.bounceValue }], backgroundColor: 'black' }} >
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              {numbers[0]}{numbers[1]}{numbers[2]}
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              {numbers[3]}{numbers[4]}{numbers[5]}
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              {numbers[6]}{numbers[7]}{numbers[8]}
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              {numbers[9]}{numbers[10]}{numbers[11]}
            </View>
          </View>
        </Animated.View>
      </View>
    );
  }
}

