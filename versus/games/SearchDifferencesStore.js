import State from './../services/State'

var games = [
      {
        controlImage: require('./../../images/3versch_12.jpg'), // graffiti var 1
        image: require('./../../images/3versch_12_1.jpg'),
        targets: [
            { xs: 26.38, xe: 61.62, ys: 33.54, ye: 47.56 }, // mond
            { xs: 82.33, xe: 100.00, ys: 79.88, ye: 93.54 }, // oog r.o.
            { xs: 12.48, xe: 27.05, ys: 50.85, ye: 67.68 }] // oor links
    },
    {
        controlImage: require('./../../images/3versch_12.jpg'), // graffiti var 2
        image: require('./../../images/3versch_12_2.jpg'),
        targets: [
            { xs: 64.24, xe: 77.14, ys: 2.66, ye: 16.98 }, // ood r.b.
            { xs: 42.86, xe: 54.00, ys: 58.17, ye: 72.93 }, // tand
            { xs: 0.00, xe: 13.38, ys: 67.07, ye: 92.07 }] // oog l.o.
    },
    {
        controlImage: require('./../../images/3versch_11.jpg'), // stadje var 1
        image: require('./../../images/3versch_11_1.jpg'),
        targets: [
            { xs: 16.57, xe: 56.00, ys: 0.00, ye: 13.17 }, // wolk
            { xs: 57.90, xe: 68.00, ys: 27.32, ye: 69.02 }, // pilaar
            { xs: 76.86, xe: 95.43, ys: 86.59, ye: 100.00 }] // dak ro
    },
    {
        controlImage: require('./../../images/3versch_11.jpg'), // stadje var 2
        image: require('./../../images/3versch_11_2.jpg'),
        targets: [
            { xs: 30.38, xe: 51.52, ys: 13.41, ye: 27.07 }, // schotel
            { xs: 0.00, xe: 18.76, ys: 49.27, ye: 100.00 }, // geb lo
            { xs: 81.24, xe: 92.00, ys: 22.07, ye: 39.63 }] // toren spts
    },
    {
        controlImage: require('./../../images/3versch_10.jpg'), // trein var 1
        image: require('./../../images/3versch_10_1.jpg'),
        targets: [
            { xs: 3.62, xe: 18.10, ys: 20.00, ye: 33.41 }, // wag links
            { xs: 0.00, xe: 41.71, ys: 81.22, ye: 100.00 }, // spoor
            { xs: 84.57, xe: 100.00, ys: 39.76, ye: 59.51 }] // wag rechts
    },
    {
        controlImage: require('./../../images/3versch_10.jpg'), // trein var 2
        image: require('./../../images/3versch_10_2.jpg'),
        targets: [
            { xs: 73.33, xe: 88.57, ys: 0.00, ye: 19.27 }, // paal
            { xs: 56.76, xe: 70.86, ys: 30.49, ye: 42.68 }, // raampje
            { xs: 0.00, xe: 16.00, ys: 41.46, ye: 54.39 }] // graf links
    },
    {
        controlImage: require('./../../images/3versch_9.jpg'), // vw var 2
        image: require('./../../images/3versch_9_2.jpg'),
        targets: [
            { xs: 86.29, xe: 100.00, ys: 6.83, ye: 31.22 }, // achter
            { xs: 19.05, xe: 28.95, ys: 49.76, ye: 61.46 }, // klep
            { xs: 54.29, xe: 66.29, ys: 34.15, ye: 47.07 }] // spiegel
    },
    {
        controlImage: require('./../../images/3versch_9.jpg'), // vw var 1
        image: require('./../../images/3versch_9_1.jpg'),
        targets: [
            { xs: 9.90, xe: 23.62, ys: 0.00, ye: 34.15 }, // toren
            { xs: 33.90, xe: 44.76, ys: 47.80, ye: 62.68 }, // koplam
            { xs: 47.62, xe: 59.05, ys: 76.59, ye: 100.00 }] // oliespoor
    },
    {
        controlImage: require('./../../images/3versch_8.jpg'), // ironmn var 1
        image: require('./../../images/3versch_8_1.jpg'),
        targets: [
            { xs: 25.71, xe: 40.19, ys: 38.05, ye: 64.15 }, // hoofd
            { xs: 75.24, xe: 88.76, ys: 57.56, ye: 72.93 }, // pakje
            { xs: 85.71, xe: 98.29, ys: 32.20, ye: 53.98 }] // hoofd achter
    },
    {
        controlImage: require('./../../images/3versch_8.jpg'), // ironmn var 1
        image: require('./../../images/3versch_8_2.jpg'),
        targets: [
            { xs: 56.38, xe: 70.48, ys: 37.07, ye: 63.17 }, // hoofd
            { xs: 0.57, xe: 16.00, ys: 57.80, ye: 75.37 }, // pakje
            { xs: 22.67, xe: 43.24, ys: 76.34, ye: 99.27 }] // benen
    },
    {
        controlImage: require('./../../images/3versch_7.jpg'), // skyline var 2
        image: require('./../../images/3versch_7_2.jpg'),
        targets: [
            { xs: 25.62, xe: 38.48, ys: 2.68, ye: 18.17 }, // antennes
            { xs: 82.95, xe: 100.00, ys: 75.73, ye: 95 }, // mensen rechts
            { xs: 88.1, xe: 100, ys: 37.80, ye: 55.12 }] // toren
    },
    {
        controlImage: require('./../../images/3versch_7.jpg'), // skyline var 1
        image: require('./../../images/3versch_7_1.jpg'),
        targets: [
            { xs: 62.00, xe: 76.19, ys: 22.68, ye: 41.59 }, // toren
            { xs: 83.90, xe: 100.00, ys: 0.00, ye: 24.15 }, // boom
            { xs: 0, xe: 17.62, ys: 73.54, ye: 94.27 }] // mensen links]
    },
    {
        controlImage: require('./../../images/3versch_6.jpg'), // flat var 2
        image: require('./../../images/3versch_6_2.jpg'),
        targets: [
            { xs: 2.9, xe: 16.3, ys: 76.5, ye: 93.4 }, // lond
            { xs: 21.4, xe: 37.5, ys: 1.2, ye: 21.5 }, // wndw
            { xs: 59.2, xe: 77.9, ys: 73.4, ye: 97.7 }] // deur}
    },
    {
        controlImage: require('./../../images/3versch_6.jpg'), // flat var 1
        image: require('./../../images/3versch_6_1.jpg'),
        targets: [
            { xs: 77.6, xe: 99.6, ys: 0, ye: 26.1 }, // rbov
            { xs: 60.5, xe: 76.5, ys: 26, ye: 46.2 }, // wndw
            { xs: 39.9, xe: 58.9, ys: 74.6, ye: 100 }] // mdondr
    },
    {
        controlImage: require('./../../images/3versch_5.jpg'), // smily op bank var 1
        image: require('./../../images/3versch_5_1.jpg'),
        targets: [
            { xs: 0, xe: 15, ys: 48.2, ye: 80.5 }, // bank
            { xs: 64, xe: 81, ys: 65, ye: 76 }, // text
            { xs: 24.5, xe: 37.5, ys: 38, ye: 50 }] // tong
    },
    {
        controlImage: require('./../../images/3versch_5.jpg'), // smily op bank var 2
        image: require('./../../images/3versch_5_2.jpg'),
        targets: [
            { xs: 58, xe: 75, ys: 13.8, ye: 24.1 }, // wenkbrouw
            { xs: 23, xe: 33.5, ys: 63.4, ye: 75.4 }, // txt
            { xs: 18, xe: 44, ys: 31, ye: 44 }] // mond
    },
    {
        controlImage: require('./../../images/3versch_4.jpg'), // bureau var 2
        image: require('./../../images/3versch_4_2.jpg'),
        targets: [
            { xs: 78, xe: 90, ys: 41, ye: 55 }, // boek
            { xs: 70, xe: 84, ys: 68, ye: 83 }, // propjes
            { xs: 41, xe: 53, ys: 70, ye: 86 }] // been
    },
    {
        controlImage: require('./../../images/3versch_4.jpg'), // bureau var 1
        image: require('./../../images/3versch_4_1.jpg'),
        targets: [
            { xs: 74, xe: 92, ys: 23, ye: 42 }, // lamp
            { xs: 64, xe: 75, ys: 45, ye: 57 }, // muis
            { xs: 20, xe: 34, ys: 30, ye: 46 }] // blaadje
    },
    {
        controlImage: require('./../../images/3versch_2.jpg'), // octopus
        image: require('./../../images/3versch_2_1.jpg'),
        targets: [
            { xs: 0.0, xe: 12.6, ys: 73.1, ye: 83.2 }, // tentakel
            { xs: 78.1, xe: 97.6, ys: 10.9, ye: 30 }, // damp
            { xs: 50.6, xe: 60.6, ys: 26.5, ye: 36.5 }] // oog
    },
    {
        controlImage: require('./../../images/3versch_3.jpg'), // maan var 1
        image: require('./../../images/3versch_3_1.jpg'),
        targets: [
            { xs: 8, xe: 22, ys: 43, ye: 63 }, // maan
            { xs: 46, xe: 56, ys: 41, ye: 53 }, // raampje
            { xs: 70.6, xe: 84.6, ys: 20.5, ye: 35.5 }] // planeet

    },
    {
        controlImage: require('./../../images/3versch_3.jpg'), // maan var 2
        image: require('./../../images/3versch_3_2.jpg'),
        targets: [
            { xs: 46.1, xe: 56.1, ys: 5, ye: 20 }, // raket punt
            { xs: 36.1, xe: 46.6, ys: 67.9, ye: 84 }, // raket vlam
            { xs: 74.6, xe: 89.6, ys: 49.5, ye: 65 }] // ster verdwenen
    },
    {
        controlImage: require('./../../images/3versch_1.jpg'), // kat
        image: require('./../../images/3versch_1_1.jpg'),
        targets: [
            { xs: 8.9, xe: 25.6, ys: 41.1, ye: 56.2 }, // letters op tas
            { xs: 0, xe: 20.6, ys: 0, ye: 15 }, // achtergrond
            { xs: 70.6, xe: 99.6, ys: 83.5, ye: 96 }] // vloer spleet
    }
];

export function SearchDifferencesStore() {
    return {
        games: games,
        getSemiRandomGame: function (gameid, round) {
            let seed;
            round = round ? round : 1;

            /* LOCAL GAME: Base on gameStarted (origins from asyncStorage)
               ONLINE GANE: Base on round and game id  
            */
            if (State.currentMatch.gameMode == 'online') {
                seed = State.currentMatch.gameid;
            }
            else {
                seed = new Date().getTime();
            }
            var x = Math.sin(seed) * 1000;
            let rand = x - Math.floor(x);
            return games[Math.floor(rand * games.length)];
        }
    }
}





