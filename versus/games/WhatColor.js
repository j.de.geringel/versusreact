
import React, { Component } from 'react'
import { View, Text, TouchableHighlight, Animated, Image, Modal, Dimensions } from 'react-native'
import { Icon, Button } from 'react-native-elements'

import colors from './../../colors';
import BaseGame from './common/BaseGame';
import GameScoreStatusBar from './common/GameScoreStatusBar';
import TimeLeftBar from './common/TimeLeftBar';
import FinishedModal from './common/FinishedModal';

import SoundService from './../services/SoundService'; 

export default class WhatColor extends BaseGame {

    colors = [
        { label: 'blue', value: colors.secbutton },
        { label: 'red', value: '#E71818' },
        { label: 'green', value: colors.buttonok },
        { label: 'gray', value: '#869199' },
        { label: 'orange', value: '#CA7517' },
        { label: 'purple', value: '#A434AD' },
        { label: 'yellow', value: 'gold' }

    ]


    images = [
        { label: 'panda', value: require('./../../images/avatars/panda_no_back.png') },
        { label: 'dog', value: require('./../../images/avatars/hond_no_back.png') },
        { label: 'car', value: require('./../../images/icons/car.png') },
        { label: 'earth', value: require('./../../images/icons/internet.png') },
        { label: 'fox', value: require('./../../images/avatars/vos_no_back.png') },
        { label: 'rabbit', value: require('./../../images/avatars/haas_no_back.png') },
        { label: 'shoe', value: require('./../../images/icons/shoe.png') },
        { label: 'carrot', value: require('./../../images/icons/carrot.png') },
        { label: 'cheese', value: require('./../../images/icons/cheese.png') },
        { label: 'wine', value: require('./../../images/icons/glass.png') },
        { label: 'rocket', value: require('./../../images/icons/rocket.png') },

    ]


    levelLabels = [];
    round = 0;
    roundMode = '';
    safeMode = false;
    totalClickPoints = 0;


    constructor(props) {
        // init the app
        super(props);

        this.state = {
            lives: 3,
            score: 0,
            label: '',
            level: 1,
            levelTiles: [],
            round: 1,
            timeLimit: 4000,
            bounceValue: new Animated.Value(1),
            notPresentMode: false
        }
        this.newTiles();
    }

    componentDidMount() {
        this.forceUpdate();
    }


    newTiles = function () {
        this.state.notPresentMode = false;
        this.state.levelTiles = [];

        let shuffeledColors = this.shuffleArray(this.colors);
        let shuffeledImages = this.shuffleArray(this.images);

        for (let i = 0; i < 6; i++) {
            this.state.levelTiles.push({ color: shuffeledColors[i], image: shuffeledImages[i] });
        }

        // here we determine what label (animal, color) to show and set the important numbersRound var (used in render)
        let randomPick = this.state.levelTiles[Math.floor(Math.random() * this.state.levelTiles.length)];

        // not present mode, overwrite random pick with something not in shuffled mwahaha
        if (Math.random() > 0.66 && this.state.round > 8) {
            this.state.notPresentMode = true;
            randomPick = { color: shuffeledColors[6], image: shuffeledImages[6] };
        }

        let label;

        if ((this.state.round > 5 && Math.random() < 0.66)) { // label can also be an animal after l1
            this.roundMode = 'image';
            label = randomPick.image.label;
        }
        else {

            Math.random() > 0.5 ? this.showImages = true : this.showNumbers = true;
            this.roundMode = 'color';
            label = randomPick.color.label;
        }

        this.state.label == label ? this.newTiles() : this.state.label = label; // to prevent same twice
    }


    checkCorrect = function (tileClicked) {
        if (!this.safeMode) {
            if (tileClicked == "notPresent") {
                if (this.state.notPresentMode) { // correct
                    this.handleCorrect();
                }
                else {
                    this.handleIncorrect();
                }
            }
            else if (tileClicked.color.label == this.state.label || tileClicked.image.label == this.state.label) { // correct 
                this.handleCorrect();
            }
            else { // incorrect
                this.handleIncorrect();
            }
        }
    }

    handleCorrect = async function () {
        SoundService.playPopSound();
        this.state.round = this.state.round + 1;

        this.setState({ score: this.state.score + 20 });
        this.refs.gameScoreStatusBar.setSuccesNotification('+20');
        this.totalClickPoints = this.totalClickPoints + 20;
        this.state.timeLimit = this.state.timeLimit - (this.state.round < 15 ? 130 : 60);
        this.newTiles();
        this.refs.timeLeftBar.startAnimation(this.state.timeLimit);

    }

    handleIncorrect = async function () {
        SoundService.playWrongSound();
        this.safeMode = true;
        this.refs.timeLeftBar.stopAnimation();
        this.shakeScreen();
        await this.sleep(1000);

        // todo shakescreen
        if (this.state.lives == 1) {
            this.gameFinishedHandler();
        }
        else {
            // shakescreen and pauze for a moment.
            this.safeMode = false;
            this.refs.timeLeftBar.startAnimation(this.state.timeLimit);
            this.newTiles();
        }
        this.setState({ lives: this.state.lives - 1 });
    }

 

    timeUpCallBack = function () {
        if (!this.safeMode) {
            this.handleIncorrect();
        }
    }



    gameFinishedHandler = function () {
        this.refs.timeLeftBar.stopAnimation();

        let resultInfo = { penalties: [], score: [], stars: 0, totalScore: 0, playerDied: true };

        resultInfo.score.push({ label: 'Correct x20', value: this.totalClickPoints, icon: 'correct' });
     
        resultInfo.totalScore = this.state.score;

        let stars = 0;
        if (this.state.score >= 1500) {
            stars = 5;
        }
        else { //between 1400 and 0 there are 4 points
            stars = 1 + (this.state.score * (4 / 1500));
        }
        resultInfo.stars = stars;

        /** EINDE SCORE CALCULATION */
        this.postResult(resultInfo);

    }

    createTile = function (tileProps) {

        return (
            <TouchableHighlight onPress={() => this.checkCorrect(tileProps)} style={{ flex: 1 }} >
                <View style={[{ backgroundColor: tileProps.color.value, flex: 1, borderColor: 'black', borderRadius: 8, borderWidth: 5, alignItems: 'center', justifyContent: 'center' }]}>
                    {this.state.round > 4 ? <Image source={tileProps.image.value} style={{ height: 70, width: 70 }} /> : null}
                </View>
            </TouchableHighlight >
        )
    }


    render() {

        return (
            <View style={{ flex: 1 }}>
                <FinishedModal show={this.state.finishedOverlay} />
                <GameScoreStatusBar navigation={this.props.navigation} ref="gameScoreStatusBar" lives={this.state.lives} score={this.state.score} />

                <Animated.View style={{ flex: 1, transform: [{ scale: this.state.bounceValue }] }} >

                    <TimeLeftBar ref="timeLeftBar" timeUpCallback={this.timeUpCallBack.bind(this)} style={{ height: 20, marginTop: 10 }} />

                    <View style={{ flex: 2, padding: 20, backgroundColor: 'black', justifyContent: 'center', borderColor: 'black', borderTopWidth: 2 }}>
                        <Text style={{ textAlign: 'center', color: 'white', fontSize: 50, opacity: 0.9 }}>{this.state.label}</Text>
                    </View>
                    <View style={{ flex: 10, backgroundColor: 'black', borderColor: 'black', borderWidth: 5 }}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            {this.createTile(this.state.levelTiles[0])}
                            {this.createTile(this.state.levelTiles[1])}
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            {this.createTile(this.state.levelTiles[2])}
                            {this.createTile(this.state.levelTiles[3])}
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            {this.createTile(this.state.levelTiles[4])}
                            {this.createTile(this.state.levelTiles[5])}
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <TouchableHighlight onPress={() => this.checkCorrect("notPresent")} style={{ flex: 1 }}>
                                <View style={[{ borderColor: 'black', borderRadius: 8, borderWidth: 5, backgroundColor: colors.primdark, flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection:'row' }]}>
                                    <Image source={require('./../../images/icons/forbidden.png')} style={{height:40, width: 40, marginRight: 20}}/>
                                    <Text style={{ color: 'white', opacity: 0.9, fontSize: 25, fontWeight: 'bold', textAlign: 'center' }}>
                                        NOT PRESENT
                                </Text>
                                </View>
                            </TouchableHighlight >

                        </View>
                    </View>

                </Animated.View>
            </View>
        );
    }
}

