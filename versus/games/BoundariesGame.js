
import React, { Component } from 'react'
import { View, Animated, Text, Dimensions, Easing, Image, TouchableHighlight } from 'react-native'
import { Button, Icon } from 'react-native-elements'

import colors from './../../colors';
import GameScoreStatusBar from './common/GameScoreStatusBar';
import TimeLeftBar from './common/TimeLeftBar';
import BaseGame from './common/BaseGame';
import FinishedModal from './common/FinishedModal';
import RoundModal from './common/RoundModal';

import SoundService from './../services/SoundService'; 

export default class BoundariesGame extends BaseGame {

  currentX = 150; // start coordinates
  currentY = 0;
  targetValue;
  ballDissapeared = false;

  levels = [
    { moveX: false, orient: 'row', xStPrc: 0.5, yStPrc: 0.1, tarPrc: 0.4, xSlow: 1, ySlow: 1, fadeBall: false },
    { moveX: false, orient: 'row', xStPrc: 0.5, yStPrc: 0.2, tarPrc: 0.35, xSlow: 1, ySlow: 1, fadeBall: 1.3 },
    { moveX: true, orient: 'row', xStPrc: 0.1, yStPrc: 0.2, tarPrc: 0.3, xSlow: 0.7, ySlow: 0.7, fadeBall: 1 },
    { moveX: true, orient: 'column', xStPrc: 0.2, yStPrc: 0.2, tarPrc: 0.35, xSlow: 0.7, ySlow: 0.7, fadeBall: 0.7 },
    { moveX: true, orient: 'column', xStPrc: 0.5, yStPrc: 0.2, tarPrc: 0.35, xSlow: 2.5, ySlow: 0.5, fadeBall: 2 },
    { moveX: true, orient: 'row', xStPrc: 0.1, yStPrc: 0.1, tarPrc: 0.25, xSlow: 0.5, ySlow: 0.4, fadeBall: 1, tarDel: 2 },
    { moveX: true, orient: 'row', xStPrc: 0.5, yStPrc: 0.9, tarPrc: 0.20, xSlow: 0.3, ySlow: 0.5, fadeBall: 1, tarDel: 2 },
    { moveX: true, orient: 'column', xStPrc: 0.5, yStPrc: 0.9, tarPrc: 0.25, xSlow: 0.6, ySlow: 0.2, fadeBall: 1.5, tarDel: 2 },
    { moveX: true, orient: 'row', xStPrc: 0.3, yStPrc: 0.2, tarPrc: 0.15, xSlow: 0.4, ySlow: 0.3, fadeBall: 1, tarDel: 3 },
    { moveX: true, orient: 'row', xStPrc: 0.3, yStPrc: 0.2, tarPrc: 0.15, xSlow: 0.2, ySlow: 2, fadeBall: 1, tarDel: 3 },
  ]

  constructor(props) {
    // init the app
    super(props);

    this.state = {
      bounceValue: new Animated.Value(1),
      xValue: new Animated.Value(150),
      yValue: new Animated.Value(0),
      opacityBall: new Animated.Value(0.9),
      lives: 3,
      score: 0,
      level: 0,
      peaks: 0,
      targetOrientation: 'row',
      targetPx: new Animated.Value(0),
      modalVisible: true,
      modalHeading: 'Practice round',
      modalExplain: 'Click button when ball is in the light blue area.',
    }
  };

  componentDidMount() {
    this.state.xValue.addListener(({value}) => this.currentX = value);
    this.state.yValue.addListener(({value}) => this.currentY = value);
  }

  componentWillUnmount() {
    //remove listeners
    this.state.xValue.removeAllListeners();
    this.state.yValue.removeAllListeners();
  }

  startAnimations = async function () {
    this.refs.container.measure((fx, fy, width, height, px, py) => {
      let curLevel = this.levels[this.state.level];
      this.maxHeight = height - 35;
      this.maxWidth = width - 35;

      // set target
      this.setState({
        targetOrientation: curLevel.orient
      });

      // start positon
      this.state.yValue.setValue(curLevel.yStPrc * this.maxHeight);
      this.state.xValue.setValue(curLevel.xStPrc * this.maxWidth);

      if (curLevel.moveX) {
        this.startXanimation();
      }
      this.startYanimation();
      this.fadeBall();

      this.targetValue = curLevel.orient == 'row' ? this.maxHeight * curLevel.tarPrc : this.maxWidth * curLevel.tarPrc;
      this.startTargetAnimation();
    });
  }

  peak(powerUpUsed) {
    if (this.state.level > 0 && (this.state.peaks > 0 || !powerUpUsed)) {
      powerUpUsed ? this.setState({ peaks: this.state.peaks - 1 }) : null;
      Animated.sequence([
        Animated.timing(this.state.opacityBall,
          { toValue: 0.9, duration: 100 }
        ),
        Animated.timing(this.state.opacityBall,
          { toValue: 0.0, duration: 500 }
        )
      ]).start();
    }
  }

  fadeBall() {
    if (this.levels[this.state.level].fadeBall) {
      Animated.timing(this.state.opacityBall,
        {
          toValue: 0,
          duration: 2000 * this.levels[this.state.level].fadeBall
        }
      ).start(()=> this.ballDissapeared = true);
    }
  }

  startTargetAnimation() {

    Animated.timing(this.state.targetPx,
      {
        toValue: this.targetValue,
        duration: (1000),
        delay: 1000 * (this.levels[this.state.level].tarDel ? this.levels[this.state.level].tarDel : 1)
      }
    ).start();
  }

  startYanimation(bounced) {
    if (bounced && !this.ballDissapeared){ SoundService.playPopSound(); };

    let goTo = this.currentY == this.maxHeight ? 0 : this.maxHeight;
    let pace = goTo == 0 ? (this.currentY - goTo) / this.maxHeight : (goTo - this.currentY) / this.maxHeight; // direction

    this.currentYAnimantion = Animated.timing(this.state.yValue,
      {
        toValue: goTo,
        easing: Easing.linear,
        duration: (2500 * pace) * this.levels[this.state.level].ySlow
      }
    )
    this.currentYAnimantion.start((completed) => completed.finished ?  this.startYanimation(true) : null);
  }

  startXanimation(bounced) {
    if (bounced && !this.ballDissapeared){ SoundService.playPopSound(); };

    let goTo = this.currentX == this.maxWidth ? 0 : this.maxWidth;
    let pace = goTo == 0 ? pace = (this.currentX - goTo) / this.maxWidth : pace = (goTo - this.currentX) / this.maxWidth;

    this.currentXAnimantion = Animated.timing(this.state.xValue,
      {
        toValue: goTo,
        easing: Easing.linear,
        duration: (2500 * pace) * this.levels[this.state.level].xSlow
      }
    );
    this.currentXAnimantion.start((completed) => completed.finished ? this.startXanimation(true) : null);

  }

  async checkIfInTarget() {
    if ((this.levels[this.state.level].orient == "row" && this.currentY <= this.targetValue) ||
      (this.levels[this.state.level].orient == "column" && this.currentX <= this.targetValue)) { // score!
      SoundService.playCorrectSound();
      this.currentYAnimantion.stop();
      this.currentXAnimantion ? this.currentXAnimantion.stop() : null;
      this.state.level = this.state.level + 1;
      this.setState({ score: this.state.score + 100 });
      this.refs.gameScoreStatusBar.setSuccesNotification('+100');
      this.state.opacityBall.setValue(0.9);
      await this.sleep(1000);
      this.state.targetPx.setValue(0);
      this.state.level == this.levels.length ? this.gameFinishedHandler(false) : this.setState({ modalVisible: true });
    }
    else { // miss
      SoundService.playWrongSound();
      this.shakeScreen();
      this.peak();
      if (this.state.lives == 1){
        await this.sleep(1000);
        this.gameFinishedHandler(true);
      } 
      this.state.level > 0 ? this.setState({ lives: this.state.lives - 1 }) : null;
    }
  }

  startLevel() { // will be called every time after practice round
    this.ballDissapeared = false;
    let peakPowerUp = (this.state.level) % 3 == 0;
    let modalTxt = this.state.level + 1 == 1 ?
      'The ball will disappear now, you have to time and geuss!' :
      peakPowerUp ? "You earn a peak powerup every third level!" :
        "It's smart to use all your Peak powerups before you run out of lives!";

    this.setState({
      peaks: peakPowerUp ? this.state.peaks + 1 : this.state.peaks,
      modalBonusses: (this.state.level + 1) % 3 == 0 ? [{ txt: "Peak power up added!", score: 1 }] : [],
      modalVisible: false,
      modalHeading: 'Level ' + (this.state.level + 1), // already set modeal props for next level
      modalExplain: modalTxt,
    });
    this.startAnimations();
  }

  gameFinishedHandler(playerDied) {
    let resultInfo = { penalties: [], score: [], stars: 0, totalScore: 0, playerDied: playerDied };

    let livesBonus = playerDied ? 0 : this.state.lives * 50;
    let levelBonus = this.state.level * 100;
    resultInfo.score.push({ label: 'Lives Left x50', value: livesBonus, icon: 'lives' });
    resultInfo.score.push({ label: 'Levels x100', value: levelBonus, icon: 'correct' });

    //max score 1150: 1000 livesleft, 150
    let score = + levelBonus + livesBonus;
    resultInfo.totalScore = score;

    /** Stars CALCULATION */
    let stars = 0;
    if (score >= 1150) {
      stars = 5;
    }
    else { // gaan we van 5 naar 1 sterren in 1150 punten
      stars = 1 + (score * (4 / 1150));
    }
    resultInfo.stars = Math.round(stars * 100) / 100;
    this.postResult(resultInfo);
  }

  render() {
    // BACKGROUND IMAGE AND TARGET VIEW BASED ON ORIENTATION
    let borderStyle, dimensionStyle, backGroundImage;

    if (this.state.targetOrientation == "row") {
      borderStyle = { height: 6 };
      radiusStyle = { borderTopLeftRadius: 5, borderTopRightRadius: 5 };
      dimensionStyle = { height: this.state.targetPx };
      backGroundImage = (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image style={{ height: 180, width: 200, opacity: 0.05, }} source={require('./../../images/logo-new.png')} />
        </View>)
    }
    else {
      borderStyle = { width: 6 };
      radiusStyle = { borderTopLeftRadius: 5, borderBottomLeftRadius: 5 };
      dimensionStyle = { width: this.state.targetPx };
      backGroundImage = (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', overflow: 'visible' }}>
          <Image style={{ height: 180, width: 200, opacity: 0.05, transform: [{ rotate: '270 deg' }, { scale: 0.75 }] }} source={require('./../../images/logo-new.png')} />
        </View>)
    }

    let targetView = (
      <View style={{ flexDirection: this.state.targetOrientation == 'row' ? 'column' : 'row' }}>
        <Animated.View style={[{ backgroundColor: colors.primmed }, dimensionStyle, radiusStyle]} ref="target" />
        <View style={[{ backgroundColor: colors.primlight }, borderStyle]} />
      </View>)


    return (
      <View style={{ backgroundColor: colors.black, flex: 1 }}>
        <GameScoreStatusBar navigation={this.props.navigation} ref="gameScoreStatusBar" lives={this.state.lives} score={this.state.score} />
        <RoundModal show={this.state.modalVisible} header={this.state.modalHeading} explain={this.state.modalExplain}
          closeCallback={this.startLevel.bind(this)} bonusses={this.state.modalBonusses} />
        <FinishedModal show={this.state.finishedOverlay} />

        <Animated.View style={{ flex: 1, paddingBottom: 20, justifyContent: 'space-around', transform: [{ scale: this.state.bounceValue }] }} >
          <View ref="container" style={{
            flex: 9, marginTop: 20, marginLeft: 20, marginRight: 20, backgroundColor: colors.primdark, borderRadius: 5,
            flexDirection: this.state.targetOrientation == 'row' ? 'column' : 'row',
          }} >
            {targetView}
            {backGroundImage}
            <Animated.View style={{
              width: 35, height: 35, borderWidth: 2, borderColor: colors.black, backgroundColor: colors.primbutton, position: "absolute", borderRadius: 25,
              left: this.state.xValue, top: this.state.yValue, opacity: this.state.opacityBall
            }} />
          </View>
          <View style={{ flex: 2, padding: 5, flexDirection: 'row' }}>

            <TouchableHighlight onPress={() => this.peak(true)} style={{ flex: 1, marginLeft: 15 }}>
              <View style={{ flex: 1 }}>
                <View style={{
                  backgroundColor: colors.primmed, borderRadius: 5, alignItems: 'center', justifyContent: 'center',
                  flex: 1, marginTop: 20, marginRight: 10
                }}>
                  <Text style={{ fontSize: 24, color: colors.white }}>PEAK</Text>
                </View>
                <Text style={{
                  color: colors.white, fontSize: 18, fontWeight: 'bold', backgroundColor: colors.primbutton, paddingLeft: 8,
                  paddingRight: 8, position: 'absolute', borderRadius: 10, top: 10, right: 2
                }}>{this.state.peaks}</Text>
              </View>
            </TouchableHighlight>

            <TouchableHighlight onPress={() => this.checkIfInTarget()} style={{ flex: 1, marginRight: 15 }}>
              <View style={{
                backgroundColor: colors.buttonok, borderRadius: 5, alignItems: 'center', justifyContent: 'center',
                flex: 1, overflow: 'visible', marginTop: 20, marginLeft: 10
              }}>
                <Text style={{ fontSize: 24, color: colors.white }}>CHECK</Text>
              </View>
            </TouchableHighlight>
          </View>
        </Animated.View >
      </View >
    );
  }
}

