import React, { Component } from 'react'
import { Icon } from 'react-native-elements'
import { View, Image, TouchableHighlight, Dimensions, Animated, Alert } from 'react-native'

import BaseGame from './common/BaseGame';
import colors from './../../colors';
import {SearchDifferencesStore} from './SearchDifferencesStore';
import GameScoreStatusBar from './common/GameScoreStatusBar';
import TimeLeftBar from './common/TimeLeftBar';
import FinishedModal from './common/FinishedModal';
import State from './../services/State'; // temp for looping 3 diffs

import SoundService from './../services/SoundService'; 


export default class SearchDifferencesGame extends BaseGame {

    debug = false;
    correct = 0;
    gameFromStore;

    constructor(props) {
        super(props);

        this.state = {
            score: 0, lives: 3, bounceValue: new Animated.Value(1),
            vinkje1: false, vinkje2: false, vinkje3: false
        };

        gameFromStore =  SearchDifferencesStore().getSemiRandomGame(); 
        //gameFromStore = SearchDifferencesStore().games[1];
        
        
        /* 
        // loops trough all 3 diffs game in one session...

        gameFromStore = gamestore.games[State.verschvariant];

        State.verschvariant = State.verschvariant + 2;

        if (State.verschvariant == 19) {
            State.verschvariant = 1;
        }
        else if (State.verschvariant == 20) {
            State.verschvariant = 0;
        }
        */
    }

    checkCordinate(evt) {
        let clickHit = false;
        let xClick = evt ? evt.nativeEvent.locationX : 0;
        let yClick = evt ? evt.nativeEvent.locationY : 0;
        this.refs.image.measure((fx, fy, width, height, px, py) => {
            var relX = (xClick / width) * 100;
            var relY = (yClick / height) * 100;

            // loop trough targets, and set V's when target is considered hit
            for (var i = 0; i < gameFromStore.targets.length; i++) {
                let t = gameFromStore.targets[i];
                let res = relX > t.xs && relX < t.xe && relY > t.ys && relY < t.ye;

                if (relX > t.xs && relX < t.xe && relY > t.ys && relY < t.ye) {
                    var targetBoxMidX = (((t.xs + ((t.xe - t.xs) / 2)) * width) / 100) - 20;
                    var targetBoxMidY = (((t.ys + ((t.ye - t.ys) / 2)) * height) / 100) - 20;
                    if (i == 0) {
                        clickHit = true;
                        if (!this.state.vinkje1.display) {
                            this.correct = this.correct + 1;
                            SoundService.playCorrectSound();
                            this.refs.gameScoreStatusBar.setSuccesNotification('+20');
                            this.setState({ vinkje1: { display: true, x: targetBoxMidX, y: targetBoxMidY }, score: this.state.score + 20 });
                        }
                    }
                    else if (i == 1) {
                        clickHit = true;
                        if (!this.state.vinkje2.display) {
                            this.correct = this.correct + 1;
                             SoundService.playCorrectSound();
                            this.refs.gameScoreStatusBar.setSuccesNotification('+20');
                            this.setState({ vinkje2: { display: true, x: targetBoxMidX, y: targetBoxMidY }, score: this.state.score + 20 });
                        }
                    }
                    else if (i == 2) {
                        clickHit = true;
                        if (!this.state.vinkje3.display) {
                            this.correct = this.correct + 1;
                             SoundService.playCorrectSound();
                            this.refs.gameScoreStatusBar.setSuccesNotification('+20');
                            this.setState({ vinkje3: { display: true, x: targetBoxMidX, y: targetBoxMidY }, score: this.state.score + 20 });
                        }
                    }
                    if (this.correct == 3 && !this.debug) {
                        clickHit = true;
                        this.gameFinishedHandler(false);
                    }
                }

                /** draw rectangeles for debug */
                if (this.debug) {
                    let x = (t.xs * width) / 100;
                    let y = (t.ys * height) / 100;
                    let imwidth = ((t.xe - t.xs) * width) / 100;
                    let imheight = ((t.ye - t.ys) * height) / 100;

                    if (i == 0) {
                        this.setState({ debug1: { x: x, y: y, w: imwidth, h: imheight } });
                    }
                    else if (i == 1) {
                        this.setState({ debug2: { x: x, y: y, w: imwidth, h: imheight } });
                    }
                    else if (i == 2) {
                        this.setState({ debug3: { x: x, y: y, w: imwidth, h: imheight } });
                    }
                }
            }

            if (!clickHit  && !this.debug) { //misclicked!
                SoundService.playWrongSound();
                // broadcast warning 
                this.shakeScreen();
                if (this.state.lives <= 1) { //dead
                    this.gameFinishedHandler(true);
                }
                this.setState({ lives: this.state.lives - 1 });
            }
        })
    }

    timeUpCallBack() {
        this.gameFinishedHandler(true);
    }

    gameFinishedHandler(playerDied) {
        let resultInfo = {penalties: [], score: [], stars: 0, totalScore:0, playerDied: playerDied };
        this.refs.timeLeftBar.stopAnimation();

        let timeLeft = 0;
     
        if (!playerDied) { // only counts when survived the game
            timeLeft = this.refs.timeLeftBar.getTimeLeft() ? this.refs.timeLeftBar.getTimeLeft() : 0;
        }
        resultInfo.score.push({label: 'Time Left',value: timeLeft, icon: 'time'});
       
        let correctClicks = this.correct * 50;
        resultInfo.score.push({label: 'Correct x50',value: correctClicks, icon: 'correct'});

        let misClicks = playerDied? 60 : this.state.lives < 3 ? (3- this.state.lives) * 20 : 0;
        resultInfo.penalties.push({label: 'Lives Lost x20', value: misClicks, icon: 'lives'});

        //max score 270: 120 timeleft, 150 correct = 270
        let score = timeLeft + correctClicks - misClicks;
        resultInfo.totalScore = score;

        /** STARS CALCULATION */
        let stars = 0;
        if (score > 260) {
            stars = 5;
        }
        else if (score > 0){
            stars = 1 + (score * (4 / 260));
        }
        resultInfo.stars = stars;

        /** EINDE SCORE CALCULATION */
        this.postResult(resultInfo);
    }

    showEndGame() {
        this.debug = true;
        this.checkCordinate();
    }

    render() {
        var {height, width} = Dimensions.get('window');

        var vinkje1, vinkje2, vinkje3;
        var debug1, debug2, debug3;

        debug1 = this.state.debug1 && !this.state.vinkje1 ? <View style={{ borderColor: colors.primbutton, borderRadius: 3, borderWidth: 2, position: 'absolute', top: this.state.debug1.y, left: this.state.debug1.x, width: this.state.debug1.w, height: this.state.debug1.h }}></View> : null;
        debug2 = this.state.debug2 && !this.state.vinkje2 ? <View style={{ borderColor: colors.primbutton, borderRadius: 3, borderWidth: 2, position: 'absolute', top: this.state.debug2.y, left: this.state.debug2.x, width: this.state.debug2.w, height: this.state.debug2.h }}></View> : null;
        debug3 = this.state.debug3 && !this.state.vinkje3 ? <View style={{ borderColor: colors.primbutton, borderRadius: 3, borderWidth: 2, position: 'absolute', top: this.state.debug3.y, left: this.state.debug3.x, width: this.state.debug3.w, height: this.state.debug3.h }}></View> : null;

        vinkje1 = this.state.vinkje1 ? <Icon name='check' type="font-awesome" size={40} color={colors.buttonok}
            containerStyle={{ position: 'absolute', top: this.state.vinkje1.y, left: this.state.vinkje1.x }} /> : null;

        vinkje2 = this.state.vinkje2 ? <Icon name='check' type="font-awesome" size={40} color={colors.buttonok}
            containerStyle={{ position: 'absolute', top: this.state.vinkje2.y, left: this.state.vinkje2.x }} /> : null;

        vinkje3 = this.state.vinkje3 ? <Icon name='check' type="font-awesome" size={40} color={colors.buttonok}
            containerStyle={{ position: 'absolute', top: this.state.vinkje3.y, left: this.state.vinkje3.x }} /> : null;


        return (
            <View style={{ flex: 1, backgroundColor: 'black' }}>
                <FinishedModal show={this.state.finishedOverlay} />
                <GameScoreStatusBar navigation={this.props.navigation} ref="gameScoreStatusBar" lives={this.state.lives} score={this.state.score} />
                <TimeLeftBar ref="timeLeftBar" timeUpCallback={this.timeUpCallBack.bind(this)} style={{ height: 20 }} timeLimit={120000} />
                <Animated.View style={{ flex: 1, transform: [{ scale: this.state.bounceValue }] }} >
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <TouchableHighlight style={{ flex: 1 }} activeOpacity={1} onPress={(evt) => this.checkCordinate(evt)}  >
                            <Image style={{ flex: 1, width: undefined, height: undefined, resizeMode: 'contain' }} source={gameFromStore.controlImage}>
                                <View style={{ flex: 1, borderBottomWidth: 2, borderColor: 'black' }} >
                                </View>
                            </Image>
                        </TouchableHighlight>
                    </View>
                    <View style={{ flex: 1 }}>
                        <TouchableHighlight style={{ flex: 1 }} activeOpacity={1} ref="image" onPress={(evt) => this.checkCordinate(evt)} >
                            <Image style={{ flex: 1, width: undefined, height: undefined, resizeMode: 'contain' }} source={gameFromStore.image} />
                        </TouchableHighlight>
                        {vinkje1}
                        {vinkje2}
                        {vinkje3}
                        {debug1}
                        {debug2}
                        {debug3}
                    </View>
                </Animated.View>
            </View>
        )
    }
}
