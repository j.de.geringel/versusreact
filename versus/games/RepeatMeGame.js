
import React, { Component } from 'react'
import { View, Text, TouchableWithoutFeedback, Animated, Image, Modal } from 'react-native'

import colors from './../../colors';
import BaseGame from './common/BaseGame';
import GameScoreStatusBar from './common/GameScoreStatusBar';
import FinishedModal from './common/FinishedModal';
import SoundService from './../services/SoundService';

import Sound from 'react-native-sound';


export default class RepeatMeGame extends BaseGame {

  currentRound = 1;
  currentRoundCordinates = [];
  faders = [];
  roundProgress = 0;
  userTurn = false;
  totalClickPoints = 0;
  totalLevelUpPoints = 0;

  notes = [
    new Sound('note_do.wav', Sound.MAIN_BUNDLE, (error) => error ? console.warn(error) : null),
    new Sound('note_re.wav', Sound.MAIN_BUNDLE, (error) => error ? console.warn(error) : null),
    new Sound('note_mi.wav', Sound.MAIN_BUNDLE, (error) => error ? console.warn(error) : null),
    new Sound('note_fa.wav', Sound.MAIN_BUNDLE, (error) => error ? console.warn(error) : null),
    new Sound('note_sol.wav', Sound.MAIN_BUNDLE, (error) => error ? console.warn(error) : null),
    new Sound('note_la.wav', Sound.MAIN_BUNDLE, (error) => error ? console.warn(error) : null),
  ]


  constructor(props) {
    // init the app
    super(props);
    this.state = {
      fadeCircle1: new Animated.Value(0.0),
      fadeCircle2: new Animated.Value(0.0),
      fadeCircle3: new Animated.Value(0.0),
      fadeCircle4: new Animated.Value(0.0),
      fadeCircle5: new Animated.Value(0.0),
      fadeCircle6: new Animated.Value(0.0),
      lives: 3,
      score: 0,
      bounceValue: new Animated.Value(1),
      finishedOverlay: false
    }
    this.faders = [this.state.fadeCircle1, this.state.fadeCircle2, this.state.fadeCircle3, this.state.fadeCircle4, this.state.fadeCircle5, this.state.fadeCircle6];
  }

  componentDidMount() {
    setTimeout(() => this.glowAll(), 500);
    setTimeout(() => this.setState({ finishedOverlay: true, modalHeader: 'Remember pattern...', modalText: '' }), 1600);
    setTimeout(() => { this.setState({ finishedOverlay: false }); this.startRound() }, 3100);

  }

  componentWillUnmount() {
    try {
      this.notes.forEach((note) => note.release());
    }
    catch (e) {
      console.warn("ERROR UNLOADING SOUNDS:" + e);
    }
  }

  glowAll() {
    this.faders.forEach((fadeVar, i) => this.glowCircle(fadeVar));
  }

  startRound = async function () {

    await this.sleep(600);

    // determine levels 
    let levelLength = this.currentRound == 1 ? 1 :
      this.currentRound == 2 ? 3 :
        this.currentRound == 3 ? 4 :
          (this.currentRound == 4 || this.currentRound == 5) ? 5 :
            (this.currentRound == 6 || this.currentRound == 7) ? 6 :
              this.currentRound - 1;

    for (let i = 0; i <= levelLength; i++) {
      var randomIndex = Math.floor(Math.random() * this.faders.length);
      if (this.currentRoundCordinates.length > 0 && randomIndex == this.currentRoundCordinates[this.currentRoundCordinates.length - 1]) { // we dont want the same tiles to glow twice in a row
        i = i - 1;
      }
      else {
        var fader = this.faders[randomIndex];
        this.glowCircle(fader, randomIndex);
        this.currentRoundCordinates.push(randomIndex);
        await this.sleep(700);
      }
    }
    this.setState({ finishedOverlay: true, modalHeader: 'Your Turn', modalText: '' });
    setTimeout(() => { this.setState({ finishedOverlay: false }); this.userTurn = true; }, 500);
  }

  checkCorrect = async function (index) {
    if (this.userTurn) {
      if (index == this.currentRoundCordinates[this.roundProgress]) {

        this.glowCircle(this.faders[index], index);
        this.roundProgress = this.roundProgress + 1;
        if (this.roundProgress == this.currentRoundCordinates.length) { // round finished
          this.setState({ score: this.state.score + 120 });
          this.totalClickPoints = this.totalClickPoints + 20;
          this.totalLevelUpPoints = this.totalLevelUpPoints + 100;
          this.refs.gameScoreStatusBar.setSuccesNotification('+120');
          await this.sleep(600);
          this.glowAll();
          await this.sleep(600);
          this.currentRound = this.currentRound + 1;
          this.userTurn = false;

          // first few levels we present a remember label to help
          if (this.currentRound < 3) {
            this.currentRound < 3 ? this.setState({ finishedOverlay: true, modalHeader: 'Remember...', modalText: '' }) : null;
            setTimeout(() => { this.setState({ finishedOverlay: false }); this.startRound() }, 700);
          }
          else {
            this.startRound();
          }
        }
        else {
          this.setState({ score: this.state.score + 20 });
          this.totalClickPoints = this.totalClickPoints + 20;
          this.refs.gameScoreStatusBar.setSuccesNotification('+20');
        }
      }
      else { // wrong click
        SoundService.playWrongSound();
        if (this.state.lives == 1) { // last life and misclick, game over
          this.gameFinishedHandler();
        }
        this.setState({ lives: this.state.lives - 1 });
        this.shakeScreen();
      }
    }
  }

  gameFinishedHandler = function () {
    this.shakeScreen();
    let resultInfo = { penalties: [], score: [], stars: 0, totalScore: 0, playerDied: true };

    resultInfo.score.push({ label: 'Correct x20', value: this.totalClickPoints, icon: 'correct' });
    resultInfo.score.push({ label: 'Levels x100', value: this.totalLevelUpPoints, icon: 'levelCleared' });

    resultInfo.totalScore = this.state.score;

    /** STARS CALCULATION */
    let stars = 0;

    if (this.state.score >= 1400) {
      stars = 5;
    }
    else { //between 1400 and 0 there are 4 points
      stars = 1 + (this.state.score * (4 / 1400));
    }
    resultInfo.stars = stars;

    /** EINDE SCORE CALCULATION */
    this.postResult(resultInfo);
  }

  glowCircle = function (fadeVar, index) {
    if (index !== undefined) {
      this.notes[index].play((success) => {
        if (!success) {
          console.warn('playback failed due to audio decoding errors');
        }
      });
    }
    Animated.sequence([
      Animated.timing(fadeVar,
        { toValue: 1.0, duration: 100 }
      ),
      Animated.timing(fadeVar,
        { toValue: 0.0, duration: 900 }
      )
    ]).start();
  }

  render() {

    return (
      <View style={{ flex: 1 }}>
        <FinishedModal show={this.state.finishedOverlay} header={this.state.modalHeader} explain={this.state.modalText} />
        <GameScoreStatusBar navigation={this.props.navigation} ref="gameScoreStatusBar" lives={this.state.lives} score={this.state.score} />

        <Animated.View style={{ flex: 1, transform: [{ scale: this.state.bounceValue }] }} >
          <View style={{ flex: 1, height: undefined, width: undefined, padding: 0, backgroundColor: colors.primdark, borderColor: 'black', borderWidth: 4 }}>
            <View style={{ flex: 2, flexDirection: 'row' }} >
              <TouchableWithoutFeedback onPress={() => this.checkCorrect(0)} style={{ flex: 1, justifyContent: 'center' }} >
                <View style={{ borderColor: 'black', borderWidth: 4, flex: 1 }}>
                  <Animated.View style={{ flex: 1, backgroundColor: '#03A45E', opacity: this.state.fadeCircle1 }} />
                </View>
              </TouchableWithoutFeedback>

              <TouchableWithoutFeedback onPress={() => this.checkCorrect(1)} style={{ flex: 1, justifyContent: 'center' }} >
                <View style={{ borderColor: 'black', borderWidth: 4, flex: 1 }}>
                  <Animated.View style={{ flex: 1, backgroundColor: '#6EBF47', opacity: this.state.fadeCircle2 }} />
                </View>
              </TouchableWithoutFeedback>
            </View>

            <View style={{ flex: 2, flexDirection: 'row' }} >
              <TouchableWithoutFeedback onPress={() => this.checkCorrect(2)} style={{ flex: 1, justifyContent: 'center' }} >
                <View style={{ borderColor: 'black', borderWidth: 4, flex: 1 }}>
                  <Animated.View style={{ flex: 1, backgroundColor: '#FEF102', opacity: this.state.fadeCircle3 }} />
                </View>
              </TouchableWithoutFeedback>

              <TouchableWithoutFeedback onPress={() => this.checkCorrect(3)} style={{ flex: 1, justifyContent: 'center' }} >
                <View style={{ borderColor: 'black', borderWidth: 4, flex: 1 }}>
                  <Animated.View style={{ flex: 1, backgroundColor: '#F8A51B', opacity: this.state.fadeCircle4 }} />
                </View>
              </TouchableWithoutFeedback>
            </View>

            <View style={{ flex: 2, flexDirection: 'row' }} >
              <TouchableWithoutFeedback onPress={() => this.checkCorrect(4)} style={{ flex: 1, justifyContent: 'center' }} >
                <View style={{ borderColor: 'black', borderWidth: 4, flex: 1 }}>
                  <Animated.View style={{ flex: 1, backgroundColor: '#F58225', opacity: this.state.fadeCircle5 }} />
                </View>
              </TouchableWithoutFeedback>

              <TouchableWithoutFeedback onPress={() => this.checkCorrect(5)} style={{ flex: 1, justifyContent: 'center' }} >
                <View style={{ borderColor: 'black', borderWidth: 4, flex: 1 }}>
                  <Animated.View style={{ flex: 1, backgroundColor: '#EE1C25', opacity: this.state.fadeCircle6 }} />
                </View>
              </TouchableWithoutFeedback>
            </View>

          </View>
        </Animated.View>
      </View>
    );
  }
}

