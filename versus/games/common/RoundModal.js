

import React, { Component } from 'react'
import { View, Modal, Text } from 'react-native'
import { Button } from 'react-native-elements'
import colors from './../../../colors';

import SoundService from './../../services/SoundService'; 


export default class RoundModal extends Component {


    constructor(props) {
        super(props);
    }

    render() {
        let bonusSection;

        if (this.props.bonusses && this.props.bonusses.length > 0) {
            bonusSection =
                (<View style={{backgroundColor: 'black', paddingBottom: 20 }}>
                    {this.props.bonusses && this.props.bonusses.length > 0 ?
                        <View style={{ flexDirection: 'row', marginTop: 20 }} >
                            <Text style={{ flex: 3, color: 'white', opacity: 1, fontSize: 24, fontWeight: 'bold', paddingLeft: 30 }}>Bonus</Text>

                        </View> : null}
                    {this.props.bonusses ? this.props.bonusses.map((bonus, i) => {
                        return (
                            <View key={i} style={{ flexDirection: 'row', marginTop: 5 }} >
                                <Text style={{ flex: 3, color: 'white', opacity: 0.7, fontSize: 20, paddingLeft: 30 }}>{bonus.txt}</Text>
                                <Text style={{ flex: 1, color: 'gold', fontWeight: 'bold', opacity: 0.7, fontSize: 20, textAlign: 'left' }}>{'+' + bonus.score}</Text>
                            </View>
                        )
                    }) : null}
                </View>);
        }


        return (
            <Modal animationType={"slide"} transparent={true} visible={this.props.show} onRequestClose={() => this.props.show = false} >
                <View style={{ flex: 1 }}>
                    <View style={[{ marginTop: 20, opacity: 1.0, backgroundColor: colors.primdark, flex: 1 }]}>
                        {bonusSection}

                        <View style={{ flex: 4, justifyContent: 'center', alignItems: 'center', padding: 20 }}>

                            <Text style={{ color: 'white', fontSize: 45, fontWeight: 'bold', textAlign: 'center' }}>{this.props.header}</Text>
                            <Text style={{ color: 'white', fontSize: 20, textAlign: 'center', paddingTop: 30, marginBottom: 30 }}>
                                {this.props.explain}
                            </Text>
                            <Button onPress={() => {SoundService.playClickSound(); this.props.closeCallback();}}
                                raised large title="READY" backgroundColor={colors.buttonok} fontSize={22} borderRadius={3} buttonStyle={{ paddingLeft: 50, paddingRight: 50 }} />
                        </View>
                        <View style={{ flex: 1 }} />
                    </View>
                </View>
            </Modal>
        )

    }
}