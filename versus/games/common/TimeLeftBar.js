

import React, { Component } from 'react'
import { View, Animated, Easing, Dimensions } from 'react-native'
import colors from './../../../colors';


export default class TimeLeftBar extends Component {

    windowMaxWidth;
    activeAnimation;
    widthAnimateValue;

    constructor(props) {
        super(props);

        this.windowMaxWidth = Dimensions.get('window').width;
        this.state = { width: new Animated.Value(this.windowMaxWidth), color: new Animated.Value(0) };

        if (!this.props.holdOnStart) {
            this.startAnimation(this.props.timeLimit);
        }

    }

    componentDidMount() {
        this.state.width.addListener(({value}) => this.widthAnimateValue = value);
    }

    componentWillUnmount() {
        this.state.width.removeAllListeners();
    }

    getTimeLeft = function () {
        let percentageTimeBarLeft = this.widthAnimateValue / this.windowMaxWidth;
        return Math.floor((percentageTimeBarLeft * this.props.timeLimit) / 1000);
    }

    stopAnimation = function () {
        this.activeAnimation.stop();
    }

    startAnimation = function (duration) {
        this.state.width = new Animated.Value(this.windowMaxWidth);
        this.state.color = new Animated.Value(0);

        this.activeAnimation = Animated.parallel([
            Animated.timing(this.state.width, {
                toValue: 0,
                duration: duration ? duration : 5000,
                easing: Easing.linear
            }),
            Animated.timing(this.state.color, {
                toValue: 300,
                duration: duration ? duration : 5000,
                easing: Easing.linear
            })]);

        this.activeAnimation.start((finished) => {
            if (finished.finished) {
                this.props.timeUpCallback();
            }
        });
    }

    render() {

        let color = this.state.color.interpolate({
            inputRange: [0, 300],
            outputRange: ['rgba(0, 140, 70, 1)', 'rgba(210, 21, 21, 1)']
        });

        return (
            <View style={{ height: 15, flexDirection: 'row', backgroundColor: 'black' }}>
                <Animated.View style={{ backgroundColor: color, width: this.state.width }} />
            </View>
        )

    }
}