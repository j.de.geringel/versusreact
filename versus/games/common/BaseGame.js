
import React, { Component } from 'react'
import { StatusBar, BackAndroid, Animated } from 'react-native'

import { ResultService } from './../../services/results/ResultService'
import { Router } from './../../../Router'
import State from './../../services/State';


export default class BaseGame extends Component {

    interval;
    resultPosted = false;

    constructor(props) {
        super(props);
        StatusBar.setHidden(true);
        BackAndroid.addEventListener('hardwareBackPress', this._androidback);
        ResultService().lockResult(State.currentMatch.gameid, State.currentRound.roundNumber);

    }

    /* handle back button  for android */
    _androidback = function () {
        return true;
    }

    /* mount and unmount handle statusbar, backbutton and interval things */
    componentWillUnmount() {
        StatusBar.setHidden(false);
        BackAndroid.removeEventListener('hardwareBackPress', this._androidback);
        if (this.interval) {
            clearInterval(this.interval);
        }
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
    }

    /* house keeping */
    componentDidMount() {
       
    }

    /**
    * Randomize array element order in-place.
   * Using Durstenfeld shuffle algorithm.
    */
    shuffleArray = function (array) {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }

    /* shake animation */
    shakeScreen = function () {
        Animated.stagger(200, [
            Animated.spring(                          // Base: spring, decay, timing
                this.state.bounceValue,                 // Animate `bounceValue`
                {
                    toValue: 1.10,                         // Animate to smaller size
                    friction: 9,
                    tension: 400                     // Bouncier spring
                }
            ),
            Animated.spring(                          // Base: spring, decay, timing
                this.state.bounceValue,                 // Animate `bounceValue`
                {
                    toValue: 1.0,                         // Animate to smaller size
                    friction: 9,
                    tension: 400                     // Bouncier spring
                }
            )
        ]).start();
    }

    // ES7, async/await
    sleep(ms = 0) {
        return new Promise(r => setTimeout(r, ms));
    }

    async postResult(resultInfo) {
        resultInfo.stars < 0 ? resultInfo.stars = 0: null;
        resultInfo.stars = Math.round(resultInfo.stars * 100) / 100;

        if (State.currentRound.resultDelayOnTimeup && resultInfo.playerDied) {
            if (this.showEndGame) {
                this.showEndGame();
            }
            await this.sleep(State.currentRound.resultDelayOnTimeup * 1000);
            this.setState({ finishedOverlay: true, modalHeader: null, modalText: null})
            await this.sleep(2200);
            this.setState({ finishedOverlay: false });
            this.props.navigation.navigate('GameResultPage', { resultInfo: resultInfo});
        }
        else {
            this.setState({ finishedOverlay: true });
            await this.sleep(2200);
              this.setState({ finishedOverlay: false });
            this.props.navigation.navigate('GameResultPage', { resultInfo: resultInfo });
        }
    }
}