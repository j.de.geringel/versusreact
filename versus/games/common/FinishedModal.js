

import React, { Component } from 'react'
import { View, Modal, Text, Image } from 'react-native'
import { Button, Icon } from 'react-native-elements'
import colors from './../../../colors';


export default class FinishedModal extends Component {


    constructor(props) {
        super(props);
    }

    render() {
        let modalContent;
        if (this.props.show && this.props.show == "success") {
               modalContent = (
                <View style={{padding: 20, }} >
                    <Image source={require('./../../../images/icons/checked.png')} style={{width: 200, height: 200}} />
                </View>)
        }
        else {
            modalContent = (
                <View style={{ backgroundColor: colors.black, padding: 20, borderRadius: 5, borderWidth: 2, borderColor: 'white' }} >
                    <Text style={{ fontSize: 30, color: 'white', textAlign: 'center' }}>{this.props.header !== undefined ? this.props.header : 'Game Finished'}</Text>
                    <Text style={{ fontSize: 20, color: colors.divider, textAlign: 'center' }}>{this.props.explain !== undefined ? this.props.explain : 'Standby for your results...'}</Text>
                </View>)
        }
        return (

            <Modal animationType={"fade"} transparent={true} visible={this.props.show ? true : false} onRequestClose={() => this.props.show = false} >
                <View style={{ flex: 1, justifyContent: 'center', opacity: 1.0, alignItems: 'center' }}>
                    {modalContent}
                </View>
            </Modal>
        )

    }
}