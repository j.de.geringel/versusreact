
import React, { Component } from 'react'

import { Icon } from 'react-native-elements'
import { View, Text, Animated, Alert } from 'react-native'

import colors from './../../../colors';
import State from './../../services/State';
import { Router } from './../../../Router'

import { NavigationActions } from 'react-navigation'



export default class GameScoreStatusBar extends Component {

    mymisclicks = 0;
    interval;
    notification = null;

    constructor(props) {
        super(props);
        this.state = {
            fadeAnim: new Animated.Value(1)
        };
    }

    /* house keeping */
    componentDidMount() {

    }

    componentWillUnmount() {

    }

    setSuccesNotification(text) {


        this.state.fadeAnim = new Animated.Value(1);


        this.setState({ notification: <Text style={{ fontSize: 15, color: 'green' }}>{text}</Text> });
        Animated.timing(this.state.fadeAnim,
            { toValue: 0.5, duration: 700 }
        ).start(() => {
            this.setState({ notification: null });
            Animated.timing(this.state.fadeAnim,
                { toValue: 1.0, duration: 400 }
            ).start();
        });

    }

    render() {
        let scoreToDisplay = this.state.notification ? this.state.notification : this.props.score;

        return (

            <View style={{ height: 40, backgroundColor: 'black', flexDirection: 'row' }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                    <View style={{ flex: 4, height: 40, flexDirection: 'row', alignItems: 'center', paddingLeft: 10, }}>
                        <Text style={{ fontSize: 15, color: colors.divider, paddingLeft: 10 }} >Score</Text>

                        <Animated.Text style={{ fontSize: 15, color: colors.dividerDarker, opacity: this.state.fadeAnim, paddingLeft: 10 }}>
                            {scoreToDisplay}
                        </Animated.Text>
                    </View>
                    <Text>2 left</Text>

                    <View style={{ flex: 5, height: 40, flexDirection: 'row', alignItems: 'center', paddingLeft: 10 }}>
                        <Icon name={this.props.lives > 0 ? 'heart' : 'heart-o'} size={20} color='red' type="font-awesome" />
                        <Icon name={this.props.lives > 1 ? 'heart' : 'heart-o'} size={20} color='red' type="font-awesome" containerStyle={{ marginLeft: 5 }} />
                        <Icon name={this.props.lives > 2 ? 'heart' : 'heart-o'} size={20} color='red' type="font-awesome" containerStyle={{ marginLeft: 5 }} />
                    </View>


                    <View style={{ flex: 2, height: 40, backgroundColor: colors.secbutton, justifyContent: 'center', alignSelf: 'center' }}>
                        <Icon name='info' size={25} color='white' onPress={() => Alert.alert('Info', State.currentRound.explain)} />
                    </View>
                    <View style={{ flex: 2, height: 40, backgroundColor: colors.primbutton, justifyContent: 'center', alignSelf: 'center' }}>
                        <Icon name='cancel' size={25} color='white'
                            onPress={() => Alert.alert(
                                'Quit game',
                                'Quitting will provide the worst possible score. Are you sure?',
                                [
                                    {
                                        text: 'Quit', onPress: () => 
                                            this.props.navigation.dispatch(NavigationActions.reset({
                                                index: 0,
                                                actions: [
                                                    NavigationActions.navigate({ routeName: 'Initial' })
                                                ]
                                            }))
                                    },
                                    { text: 'Game on', onPress: () => true },
                                ]
                            )}
                        />
                    </View>
                </View>
            </View>
        )
    }
}