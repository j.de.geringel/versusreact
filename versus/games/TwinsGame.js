
import React, { Component } from 'react'
import { View, Text, TouchableWithoutFeedback, Animated, Dimensions, Image } from 'react-native'

import colors from './../../colors';
import GameScoreStatusBar from './common/GameScoreStatusBar';
import TimeLeftBar from './common/TimeLeftBar';
import BaseGame from './common/BaseGame';
import FinishedModal from './common/FinishedModal';
import SoundService from './../services/SoundService'; 

export default class TwinsGame extends BaseGame {

  icons = [];
  icons2 = [];
  twinIcon; //the target
  clickLock = false;

  iconsTypes = [
    { label: 'airplane', value: require('./../../images/icons/airplane.png') },
    { label: 'campfire', value: require('./../../images/icons/campfire.png') },
    { label: 'car', value: require('./../../images/icons/car.png') },
    { label: 'carrot', value: require('./../../images/icons/carrot.png') },
    { label: 'cone', value: require('./../../images/icons/cone.png') },
    { label: 'log', value: require('./../../images/icons/log.png') },
    { label: 'megaphone', value: require('./../../images/icons/megaphone.png') },
    { label: 'rocket', value: require('./../../images/icons/rocket.png') },
    { label: 'shapes', value: require('./../../images/icons/shapes.png') },
    { label: 'animal', value: require('./../../images/icons/animal.png') },
    { label: 'binoculars', value: require('./../../images/icons/binoculars.png') },
    { label: 'cheese', value: require('./../../images/icons/cheese.png') },
    { label: 'console', value: require('./../../images/icons/game-console.png') },
    { label: 'glass', value: require('./../../images/icons/glass.png') },
    { label: 'internet', value: require('./../../images/icons/internet.png') },
    { label: 'money-bag', value: require('./../../images/icons/money-bag.png') },
    { label: 'shoe', value: require('./../../images/icons/shoe.png') },
    { label: 'weather', value: require('./../../images/icons/weather.png') },
  ]

  constructor(props) {
    // init the app
    super(props);
    this.state = { lives: 3, score: 0, bounceValue: new Animated.Value(1), levelTime: 20000, level: 0, finishedOverlay: false };
  }

  componentDidMount() {
    this.nextRound();
  }

  placeIcons(arrayToFill, randomIconList) {
    let minSize = this.contWidth / 6;
    let maxSize = this.contWidth / 3;

    for (let i = 0; i < 8; i++) {
      //let offsetLeft = i < 4 ? i * (width / 4) : (i - 4) * (width / 4); //1st and 2nf row
      let size = minSize + (Math.floor(Math.random() * (maxSize - minSize)));
      let left = i == 0 || i == 4 ? 5 : 10 + arrayToFill[i - 1].xEnd;

      let top = i < 4 ? Math.floor(Math.random() * 80) : Math.floor(Math.random() * 80) + arrayToFill[i - 4].yEnd;
      if (i >= 5 && top < arrayToFill[i - 5].yEnd) { //top overlaps with this one or previous, shift down
        top = arrayToFill[i - 5].yEnd + 3;
      }

      if (i == 2 || i == 6) { // second to last from row, 
        if (left + size > this.contWidth - minSize) { // shrink to make place for 4th element in row
          size = this.contWidth - left - minSize; // 
        }
      }

      if (left + size > this.contWidth || top + size > this.contHeight) { // to large for container
        size = Math.min(this.contWidth - left, this.contHeight - top);
        if (i > 3) {
          if (size < minSize) {
            size = minSize;
            top = this.contHeight - minSize;
          }
        }
      }
      let rotate = Math.floor(Math.random() * 360);

      arrayToFill.push({ left: left, top: top, icon: randomIconList[0], size: size, xEnd: size + left, yEnd: size + top, rotate: rotate });
      randomIconList.splice(0, 1); //remove from list
    }
    this.forceUpdate();
  }

  
  async nextRound() {
    this.state.level = this.state.level + 1;
    await this.sleep(0);
    await this.measureWindow();

    this.state.levelTime = this.state.level < 8 ? this.state.levelTime - 2000 : this.state.levelTime - 500;
    this.refs.timeLeftBar.startAnimation(this.state.levelTime);
    this.icons = [];
    this.icons2 = [];

    let randomIcons = this.shuffleArray(this.iconsTypes).slice();
    this.twinIcon = randomIcons[Math.floor(Math.random() * 8)]; // between 0 and 7
    this.placeIcons(this.icons, randomIcons);
    randomIcons.splice(Math.floor(Math.random() * 8), 0, this.twinIcon)// put selected Double somewhere in 0, and 7
    this.placeIcons(this.icons2, randomIcons);
  }

  async measureWindow() {
    return new Promise(r => {
      this.refs.topContainer.measure((fx, fy, contWidth, contHeight, px, py) => {
        this.contWidth = contWidth;
        this.contHeight = contHeight;
        r("ok");
      });
    });
  };

  async checkClick(iconClicked) {
    if (!this.clickLock) {
      this.clickLock = true;
      if (this.twinIcon.label == iconClicked) { // correct
        SoundService.playCorrectSound();
        this.refs.timeLeftBar.stopAnimation();
        this.refs.gameScoreStatusBar.setSuccesNotification('+100');
        this.setState({ finishedOverlay: "success", score: this.state.score + 100 });
        await this.sleep(1500);

        this.setState({ finishedOverlay: false });
        this.nextRound();
        this.clickLock = false;
      }
      else {
        this.wrongClickHandler();
      }
    }
  }

  timeUpCallBack() {
    this.clickLock = true;
    this.wrongClickHandler();
  }

  async wrongClickHandler() {
    SoundService.playWrongSound();
    this.shakeScreen();
    let liveAtWriting = this.state.lives;
    this.refs.timeLeftBar.stopAnimation();
    this.setState({ lives: this.state.level == 1 ? this.state.lives : this.state.lives - 1 , showBorder: true });
    await this.sleep(2000);
    
    if (liveAtWriting == 1) {
      this.gameFinishedHandler(true);
    }
    else {
      this.setState({ showBorder: false });
      this.nextRound();
      this.clickLock = false;
    }
  }

  gameFinishedHandler(playerDied) {
    let resultInfo = { penalties: [], score: [], stars: 0, totalScore: this.state.score, playerDied: playerDied };
    resultInfo.score.push({ label: 'Levels x100', value: this.state.score, icon: 'correct' });

    /** Stars CALCULATION */
    let stars = 0;
    if (this.state.score > 1400) {
      stars = 5;
    }
    else { // gaan we van 5 naar 0 sterren in 50 seconden 0.1 ster per secodnden
      stars = 1 + (this.state.score * (4 / 1400));
    }
    resultInfo.stars =stars;

    this.postResult(resultInfo);
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <FinishedModal show={this.state.finishedOverlay} />
        <GameScoreStatusBar navigation={this.props.navigation} ref="gameScoreStatusBar" lives={this.state.lives} score={this.state.score} />
        <TimeLeftBar ref="timeLeftBar" timeUpCallback={this.timeUpCallBack.bind(this)} style={{ height: 20 }} timeLimit={this.state.timeLimit} />
        <Animated.View style={{ flex: 1, transform: [{ scale: this.state.bounceValue }], backgroundColor: 'black', padding: 15 }} >
          <View style={{ flex: 1, backgroundColor: '#E4EAED', marginBottom: 7, borderRadius: 8 }} ref="topContainer">
            {this.icons ? this.icons.map((icon, i) => {
              return (
                <TouchableWithoutFeedback key={i} onPress={() => this.checkClick(icon.icon.label)}>
                  <View style={{
                    position: 'absolute', left: icon.left, top: icon.top, width: icon.size, height: icon.size, borderColor: colors.primbutton,
                    borderWidth: this.state.showBorder && icon.icon.label == this.twinIcon.label ? 2 : 0, borderRadius: 10
                  }} >
                    <Image source={icon.icon.value} style={{ width: icon.size - 5, height: icon.size - 5, transform: [{ rotate: icon.rotate + ' deg' }, { scale: 0.8 }] }} />
                  </View>
                </TouchableWithoutFeedback>)
            }) : null}
          </View>
          <View style={{ flex: 1, backgroundColor: '#E4EAED', marginTop: 7, borderRadius: 8 }}>
            {this.icons2 ? this.icons2.map((icon, i) => {
              return (
                <TouchableWithoutFeedback key={i} onPress={() => this.checkClick(icon.icon.label)}>
                  <View style={{
                    position: 'absolute', left: icon.left, top: icon.top, width: icon.size, height: icon.size, borderColor: colors.primbutton,
                    borderWidth: this.state.showBorder && icon.icon.label == this.twinIcon.label ? 2 : 0, borderRadius: 10
                  }}>
                    <Image source={icon.icon.value} style={{ width: icon.size - 5, height: icon.size - 5, transform: [{ rotate: icon.rotate + ' deg' }, { scale: 0.8 }] }} />
                  </View>
                </TouchableWithoutFeedback>)
            }) : null}
          </View>
        </Animated.View>
      </View>
    );
  }
}

