
import React, { Component } from 'react'
import { View, Text, TouchableHighlight, Animated, Modal } from 'react-native'
import { Button, Icon } from 'react-native-elements'

import colors from './../../colors';
import GameScoreStatusBar from './common/GameScoreStatusBar';
import BaseGame from './common/BaseGame';
import TimeLeftBar from './common/TimeLeftBar';
import RoundModal from './common/RoundModal';
import FinishedModal from './common/FinishedModal';

import SoundService from './../services/SoundService'; 

export default class MemoryTrainGame extends BaseGame {

  lastIcon = '';
  lock = false;
  gameOver = false;
  totalClickPoints = 0;
  totalLevelUpPoints = 0;
  totalPerfectLevelBonus = 0;
  initialLevelTime = 28000;

  gameModes = [
    { name: 'or', instruction: 'MATCHES PREVIOUS SHAPE OR COLOR?' },
    { name: 'shape', instruction: 'MATCHES PREVIOUS SHAPE?' },
    { name: 'color', instruction: 'MATCHES PREVIOUS COLOR?' }
  ];


  icons = ['car', 'rocket', 'heart', 'star', 'fort-awesome'];
  colors = [colors.buttonok, '#FF9326', colors.primbutton, 'yellow', colors.secbutton];

  constructor(props) {

    super(props);

    this.state = {
      animatedLeft: new Animated.Value(0.0),
      bounceValue: new Animated.Value(1),
      iconName: this.icons[2],
      iconColor: this.colors[2],
      currentRound: 1,
      score: 0,
      level: 0,
      lives: 3,
      rounds: [0, 0, 0, 0],
      levelTime: this.initialLevelTime,
      modalVisible: true,
      modalHeading: 'Warmup',
      modalExplain: 'A small warmup game without consequences',
      gameMode: this.gameModes[0]
    }
  }

  startLevel() {

    if (this.state.level > 0) {
      this.state.rounds = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

      this.state.levelTime = this.initialLevelTime - (this.state.level <= 3 ? this.state.level * 4000 :
        ((this.state.level - 3) * 2000) + 12000);

    }
    this.lastIcon = '';
    this.setState({ modalVisible: false, currentRound: 1 });

  }

  answerSubmitted(matchAnswer) {
    if (!this.lock && this.state.currentRound <= this.state.rounds.length && !this.gameOver) {
      if (!this.lastIcon) { // there was no previous icon game started.
         SoundService.playSwooshSound();
        this.refs.timeLeftBar.startAnimation(this.state.levelTime);
      }
      else {
        if (this._answerCorrect(matchAnswer)) { // correct!
          SoundService.playSwooshSound();
          this.state.rounds[this.state.currentRound - 1] = 1;
          if (this.state.level > 0) {
            this.state.score = this.state.score + 20;
            this.totalClickPoints = this.totalClickPoints + 20;
            this.refs.gameScoreStatusBar.setSuccesNotification('+20');
          }
          this.forceUpdate();

        }
        else {  // not Correct!
          SoundService.playWrongSound();
          this.state.rounds[this.state.currentRound - 1] = 2;
          this.state.lives = this.state.level > 0 ? this.state.lives - 1 : this.state.lives;
          this.shakeScreen();
          if (this.state.lives == 0) {
            this.refs.timeLeftBar.stopAnimation();
            this.gameFinishedHandler();
          }
          this.forceUpdate();
        }
        this.state.currentRound = this.state.currentRound + 1;
        if (this.state.currentRound > this.state.rounds.length) { // round finished
          this._roundFinished();
        }
      }
      this.lock = true; // to prevent double clicks
      this._shiftSymbol();
    }
  }

  _answerCorrect(matchAnswer) {
    if (this.state.gameMode.name == 'or') {
      return (this.lastIcon.name == this.state.iconName || this.lastIcon.color == this.state.iconColor) === matchAnswer;
    }
    else if (this.state.gameMode.name == 'shape') {
      return (this.lastIcon.name == this.state.iconName) === matchAnswer;
    }
    else {
      return (this.lastIcon.color == this.state.iconColor) === matchAnswer;
    }
  }

  _roundFinished() {
    this.refs.timeLeftBar.stopAnimation();
    this.state.modalHeading = 'Level ' + (this.state.level + 1);
    this.state.modalExplain = "Clear the level before time runs out, 3 strikes and you're out.";
    if (this.state.level > 0) {

      this.state.score = this.state.score + 100;
      this.totalLevelUpPoints = this.totalLevelUpPoints + 100;
      this.state.modalBonusses = [{ txt: 'Level cleared', score: 100 }];

      if (this.state.lives == 3) {
        this.state.score = this.state.score + 100;
        this.totalPerfectLevelBonus = this.totalPerfectLevelBonus + 100;
        this.state.modalBonusses.push({ txt: 'Perfect Level', score: 100 });
        this.refs.gameScoreStatusBar.setSuccesNotification('+200');
      }
      else {
        this.refs.gameScoreStatusBar.setSuccesNotification('+100');
      }
    }
    this.setState({ level: this.state.level + 1, modalVisible: true });
  }

  _shiftSymbol() {
    Animated.timing(this.state.animatedLeft,
      { toValue: -350, duration: 200 }
    ).start(() => {
      this.setState({ animatedLeft: new Animated.Value(350.0) });
      this._setIconState();
      this.lock = false;
      Animated.timing(this.state.animatedLeft,
        { toValue: 0, duration: 200 }
      ).start();
    });
  }

  _setIconState() {
    // store icon before switching as lasticon
    this.lastIcon = { name: this.state.iconName, color: this.state.iconColor };


    // determine game mode
    let gameMode;
    if (this.state.level == 0) { // all flavors in memory train
      gameMode = this.state.currentRound == 1 ? this.gameModes[1] : this.state.currentRound == 2 ? this.gameModes[2] : this.gameModes[0];
    }
    else {
      let random = Math.random();
      gameMode = random > 0.66 ? this.gameModes[0] : random > 0.33 ? this.gameModes[1] : this.gameModes[2];
    }


    // get colors
    let randomIcon = this.icons[Math.floor(Math.random() * this.icons.length)];
    let randomColor = this.colors[Math.floor(Math.random() * this.colors.length)];


    if (Math.random() > 0.40) {
      this.setState({ iconName: randomIcon, iconColor: randomColor, gameMode: gameMode });
    }
    else { // retain property which is checked (to improve yes'es)

      let retainedName = (gameMode.name == 'shape') ? this.state.iconName : null; //retain shape
      let retainedColor = (gameMode.name == 'color') ? this.state.iconColor : null; //retain shape

      if (gameMode == 'or') {
        if (Math.random() > 0.6) {
          retainedName = this.state.iconName;
          retainedColor = this.state.retainedColor;
        }
      }

      this.setState({
        iconName: retainedName ? retainedName : randomIcon,
        iconColor: retainedColor ? retainedColor : randomColor,
        gameMode: gameMode
      });
    }
  }

  gameFinishedHandler() {
    let resultInfo = { penalties: [], score: [], stars: 0, totalScore: 0, playerDied: true };
    this.gameOver = true;

    resultInfo.score.push({ label: 'Correct x20', value: this.totalClickPoints, icon: 'correct' });
    resultInfo.score.push({ label: 'Levels x100', value: this.totalLevelUpPoints, icon: 'levelCleared' });
    resultInfo.score.push({ label: 'Perfect x100', value: this.totalPerfectLevelBonus, icon: 'levelPerfect' });

    resultInfo.totalScore = this.state.score;

    /** STARS CALCULATION */
    let stars = 0;
    if (this.state.score >= 2300) {
      stars = 5;
    }
    else if (this.state.score >= 1500) {  // 800 points for 1 star
      stars = 4 + ((this.state.score - 1500) * (1 / 800));
    }
    else { // 1500 points for 3 stars
      stars = 1 + (this.state.score * (3 / 1500));
    }
    resultInfo.stars = stars;

    /** EINDE SCORE CALCULATION */
    this.postResult(resultInfo);
  }

  timeUpCallBack() {
    this.shakeScreen();
    if (this.state.lives == 1) {
      this.gameFinishedHandler();
    }
    else {
      this.state.modalHeading = 'Level ' + (this.state.level + 1);
      this.state.modalExplain = "Time up, you lost a live. Try again!";

      this.setState({ modalVisible: true, lives: this.state.level > 0 ? this.state.lives - 1 : this.state.lives });

    }
  }

  createProgressIcon(iconState, index) {
    let iconName = iconState == 0 ? 'circle-o' : iconState == 1 ? 'check-circle' : 'times-circle';
    let icoColor = iconState == 0 ? colors.primdark : iconState == 1 ? colors.buttonok : colors.primbutton;

    return (<View key={index} style={{ flex: 1, backgroundColor: icoColor, borderColor: 'black', borderLeftWidth: 1 }}>
      <Icon name={iconName} type="font-awesome" size={22} color={icoColor} containerStyle={{ flex: 1 }} />
    </View>)
  }


  render() {

    return (
      <View style={{ flex: 1, backgroundColor: colors.primdark }}>
        <FinishedModal show={this.state.finishedOverlay} />
        <GameScoreStatusBar navigation={this.props.navigation} ref="gameScoreStatusBar" lives={this.state.lives} score={this.state.score} />
        <TimeLeftBar ref="timeLeftBar" timeUpCallback={this.timeUpCallBack.bind(this)} style={{ height: 20, marginTop: 0 }} holdOnStart={true} />
        <RoundModal show={this.state.modalVisible} header={this.state.modalHeading} explain={this.state.modalExplain}
          closeCallback={this.startLevel.bind(this)} bonusses={this.state.modalBonusses} />

        <View style={{ height: 30, opacity: 0.5, flexDirection: 'row' }}>
          {this.state.rounds.map((roundState, i) => {
            return (this.createProgressIcon(roundState, i))
          })}
        </View>

        <Animated.View style={{ flex: 1, transform: [{ scale: this.state.bounceValue }], backgroundColor: 'black', justifyContent: 'center' }} >

          <Text style={{ color: colors.divider, fontSize: 23, textAlign: 'center', textAlignVertical: 'center', marginTop: 30 }}>
            {this.lastIcon ? this.state.gameMode.instruction : 'REMEMBER...'}
          </Text>
          <View style={{ flexDirection: 'row', justifyContent: 'center', flex: 4, padding: 20 }}>

            <Animated.View style={{
              justifyContent: 'center', alignItems: 'center', flex: 3, position: 'relative',
              left: this.state.animatedLeft
            }}>
              <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Icon name={this.state.iconName} type="font-awesome" size={200} color={this.state.iconColor} />
              </View>
            </Animated.View>

          </View>
          <View style={{ flexDirection: 'row', flex: 1, }}>
            <TouchableHighlight style={{ flex: this.lastIcon ? 1 : 0 }} onPress={() => this.answerSubmitted(false)}>
              <View style={{ flex: 1, backgroundColor: colors.primbutton, justifyContent: 'center' }}><Text style={{ color: 'white', fontSize: 25, textAlign: 'center' }}>{this.lastIcon ? 'NO' : null}</Text></View>
            </TouchableHighlight>
            <TouchableHighlight style={{ flex: 1 }} onPress={() => this.answerSubmitted(true)}>
              <View style={{ flex: 1, backgroundColor: colors.buttonok, justifyContent: 'center' }}><Text style={{ color: 'white', fontSize: 25, textAlign: 'center' }}>{this.lastIcon ? 'YES' : 'START'}</Text></View>
            </TouchableHighlight>
          </View>

        </Animated.View>
      </View>
    );
  }
}

