import React, { Component } from 'react'
import { View, Text, TouchableHighlight, ScrollView, ActivityIndicator, Keyboard, TextInput } from 'react-native'
import { List, ListItem, Button, SearchBar, FormInput, Icon } from 'react-native-elements'
import { Router } from './../Router'

import colors from './../colors';
import constants from './../constants';

import { UserService } from './services/UserService'
import SoundService from './services/SoundService'
import { XhrGameService } from './services/games/XhrGameService'

import Avatar from './components/Avatar'
import RegularHeader from './components/RegularHeader'
import { NavigationActions } from 'react-navigation'


export default class NewGameSearchScreen extends Component {

  searchTerm;

  constructor(props) {
    super(props)
    this.state = { users: [] };

  }

  searchUser = function (name) {
    this.refs.textInput.blur();
    SoundService.playClickSound();
    // do http request to fetch users
    if (name && name.length > 0) {
      fetch(constants.apiAddress + '/user?search=' + name)
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.data) {
            this.setState({ users: responseJson.data });
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }

  inviteUser = async function (inviteeId, inviteeName) {
    SoundService.playClickSound();
    await XhrGameService().startGame(inviteeId, inviteeName);

    this.props.navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Initial' })
      ]
    }));

  }

  render() {
    let users = this.state.users;


    return (
      <View style={{ flex: 1, backgroundColor: colors.lightbg }}>

        <RegularHeader message='Invite User' navigation={this.props.navigation} />
        <View style={{ flex: 1 }}>
          <View style={{ flexDirection: 'row', backgroundColor: colors.lightbg }}>
            <Icon name="search" color={colors.dividerDarker} size={30} containerStyle={{ paddingLeft: 20, paddingRight: 20 }} />

            <TextInput onSubmitEditing={(event) => this.searchUser(event.nativeEvent.text)}
              placeholder='Search by Username' placeholderTextColor={colors.dividerDarker} returnKeyType='search'
              style={{ fontSize: 22, padding: 6, flex: 1, color: colors.primlight }} autoFocus={true} autoCorrect={false} ref="textInput" />
          </View>




          <ScrollView contentContainerStyle={{ justifyContent: 'flex-start', backgroundColor: colors.primalpha }}>
            {
              users.map((u, i) => (
                <TouchableHighlight key={i} onPress={() => {
                  if (!u.loading) {
                    u.loading = true;
                    this.forceUpdate();
                    this.inviteUser(u.id, u.username);
                  }
                }}>
                  <View style={{
                    height: 70, borderTopWidth: i == 0 ? 1 : 0, borderBottomWidth: 1, borderColor: colors.divider, backgroundColor: colors.white,
                    paddingLeft: 10, flexDirection: 'row'
                  }}>
                    <View style={{ width: 54, alignItems: 'center', justifyContent: 'center' }}>
                      <Avatar userid={u.id} image={u.image} size={50} />
                    </View>

                    <View style={{ flex: 1, justifyContent: 'center', padding: 10 }}>
                      <Text style={{ color: 'gray', fontSize: 25 }}>{u.username}</Text>
                      <Text style={{ fontSize: 12, color: colors.dividerDarker }}>{'Member since: ' + new Date(u.timestamp).toISOString().substring(0, 10)}</Text>
                    </View>
                    <View style={{ width: 40, justifyContent: 'center', alignItems: 'center', paddingRight: 10 }}>
                      {u.loading ? <ActivityIndicator animating={true}
                        style={{ alignItems: 'center', justifyContent: 'center', padding: 0, height: 50 }} size="large" />
                        : <Icon name='chevron-right' color={colors.primalpha} size={40} />}



                    </View>
                  </View>
                </TouchableHighlight>
              ))
            }

          </ScrollView>
        </View>
      </View>
    )
  }
}
   //<ListItem containerStyle={{ backgroundColor: 'white', padding: 20 }} key={i} title={u.username} onPress={() => this.inviteUser(u.id, u.username)} />
