
import React, { Component } from 'react'
import { View, Text, TextInput, ScrollView, TouchableHighlight, Modal, Alert } from 'react-native'
import { Icon, Button } from 'react-native-elements'
import { Router } from './../Router'

import colors from './../colors';
import { ResultService } from './services/results/ResultService';
import { UserService } from './services/UserService';
import { StorageService } from './services/StorageService';
import SoundService from './services/SoundService';

import State from './services/State';
import Avatar from './components/Avatar';
import RegularHeader from './components/RegularHeader'



export default class PartyGameLobby extends Component {

  name = '';

  constructor(props) {
    super(props);
    this.state = { modalVisible: false, users: [] };
  }

  componentDidMount() {
    // set default user as itself
    if (UserService().getUser().name) {
      this.addUser(UserService().getUser().name, UserService().getUser().id);
    }

  }

  openModal() {
    SoundService.playClickSound();
    this.setState({ modalVisible: true });
  }

  addUser(name, id) {
    SoundService.playClickSound();
    if (name) {
      var usersTempVar = this.state.users.slice(); //state is immutable
      usersTempVar.push({ username: name, userid: id, rounds: [], totalscore: 0 });
      this.setState({ modalVisible: false, users: usersTempVar });
      this.name = '';
    }
  }

  removeUser(index) {
    SoundService.playClickSound();
    var usersTempVar = this.state.users.slice(); //state is immutable
    usersTempVar.splice(index, 1);
    this.setState({ users: usersTempVar });
  }

  async newGame() {
    SoundService.playClickSound();
    if (this.state.users.length < 2) {
      Alert.alert('Info', "You need at least 2 players to start a local game.");
    }
    else {
      if (await StorageService().getLocalGame()) {
        Alert.alert(
          'Start Game',
          'Starting a new local game will overwrite your current active local game. Are you sure?',
          [
            { text: 'Cancel', onPress: () => true },
            { text: 'Start Game', onPress: () => this.startGame() },
          ]
        )
      }
      else {
        this.startGame();
      }
    }
  }

  async startGame() {
    await StorageService().startLocalGame(this.state.users);
    State.currentMatch = { gameMode: 'local', noRounds: 7, dateStarted: new Date() };
    this.props.navigation.navigate('LobbyScreen', { gameMode: 'local', straightToTotals: true });

  }

  render() {

    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>

        <RegularHeader message={'Start Party Game'} navigation={this.props.navigation} />
        <Modal animationType={"slide"} transparent={false} visible={this.state.modalVisible}
          onRequestClose={() => this.state.modalVisisble = false} ref="modal">
          <View style={{ flex: 1, justifyContent: 'center', padding: 20, backgroundColor: colors.primdark }}>
            <View style={{ marginTop: 0 }}>
              <TextInput autoCapitalize='words' autoCorrect={false} style={{ height: 90, fontSize: 40, margin: 15, color: 'gray' }} placeholder="User name"
                placeholderTextColor={colors.divider} autoFocus={true} onChangeText={(name) => this.name = name} maxLength={10} />
              <View style={{ flexDirection: 'row', height: 65 }}>
                <TouchableHighlight style={{ flex: 1, marginRight: 7, marginLeft: 15 }}
                  onPress={() => this.setState({ modalVisible: false })}>
                  <View style={{ flex: 1, backgroundColor: colors.primbutton, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 22, color: colors.white }}>BACK</Text>
                  </View>
                </TouchableHighlight>

                <TouchableHighlight style={{ flex: 1, marginRight: 7, marginLeft: 15 }} onPress={() => this.addUser(this.name)}>
                  <View style={{ flex: 1, backgroundColor: colors.buttonok, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 22,  color: colors.white }}>ADD</Text>
                  </View>
                </TouchableHighlight>


              </View>
            </View>
          </View>
        </Modal>


        <View style={{ flex: 6, paddingLeft: 20, paddingRight: 20, paddingTop: 20, paddingBottom: 0 }}>
          {this.state.users.map((user, i) => {
            return <View key={i} style={{ paddingBottom: 10, paddingTop: 10, borderTopWidth: i == 0 ? 0 : 1, borderTopColor: colors.divider }}>
              <View style={{ flexDirection: 'row', height: 60 }}>
                <View style={{ width: 60, alignItems: 'center', }}>
                  <Avatar userid={user.userid} username={user.username} image={null} size={55} />
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingLeft: 15 }}>
                  <Text style={{ color: 'gray', fontSize: 40, textAlignVertical: 'center' }}>{user.username}</Text>
                </View>
                <View style={{ width: 60, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name="remove-circle" color={colors.primbutton} size={40} onPress={() => this.removeUser(i)} />
                </View>
              </View>
            </View>
          })}
        </View>


        <View style={{ flex: 2, alignItems: 'center', flexDirection: 'row' }}>

          <TouchableHighlight style={{ height: 65, flex: 1, marginRight: 7, marginLeft: 15 }} onPress={() => this.openModal()}>
            <View style={{ flex: 1, backgroundColor: colors.primmed, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ fontSize: 22, color: colors.white }}>ADD</Text>
            </View>
          </TouchableHighlight>

          <TouchableHighlight style={{ height: 65, flex: 1, marginRight: 7, marginLeft: 15 }} onPress={() => this.newGame()}>
            <View style={{ flex: 1, backgroundColor: colors.buttonok, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ fontSize: 22, color: colors.white }}>START</Text>
            </View>
          </TouchableHighlight>

        </View>
      </View>
    )
  }
}
