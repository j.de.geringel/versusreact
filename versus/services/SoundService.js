import Sound from 'react-native-sound';

var click = new Sound('button_click.wav', Sound.MAIN_BUNDLE);
var ticker = new Sound('ticker.mp3', Sound.MAIN_BUNDLE);
var correct = new Sound('correct.mp3', Sound.MAIN_BUNDLE);
var pop = new Sound('pop.mp3', Sound.MAIN_BUNDLE);
var swoosh = new Sound('woosh.mp3', Sound.MAIN_BUNDLE);
var wrongZoomer = new Sound('wrong_zoomer.wav', Sound.MAIN_BUNDLE);
var wrongResonates = new Sound('wrong_resonates.mp3', Sound.MAIN_BUNDLE);
var ascending = new Sound('ascending.wav', Sound.MAIN_BUNDLE);

export default SoundService = {
    playClickSound: function () {
        click.setVolume(0.4);
        click.play((success) => {
            if (!success) {
                console.warn('playback failed due to audio decoding errors');
            }
        });
    },
    playTickerSound: function () {
        ticker.setVolume(0.7);
        ticker.play((success) => {
            if (!success) {
                console.warn('playback failed due to audio decoding errors');
            }
        });
    },
    playCorrectSound: function () {
        correct.setVolume(0.7);
        correct.play((success) => {
            if (!success) {
                console.warn('playback failed due to audio decoding errors');
            }
        });
    },
    playPopSound: function () {
        pop.play((success) => {
            if (!success) {
                console.warn('playback failed due to audio decoding errors');
            }
        });
    },
    playSwooshSound: function () {
        swoosh.setVolume(0.7);
        swoosh.play((success) => {
            if (!success) {
                console.warn('playback failed due to audio decoding errors');
            }
        });
    },
    playAscending: function () {
        ascending.setVolume(1.0);
        ascending.play((success) => {
            if (!success) {
                console.warn('playback failed due to audio decoding errors');
            }
        });
    },
     stopAscending: function () {
         ascending.setVolume(0.0);
        //ascending.setCurrentTime(0.0);
    },
    playWrongSound: function () {
        // alternate 
        if (Math.random() > 0.5) {
            wrongZoomer.setVolume(0.4);
            wrongZoomer.play((success) => {
                if (!success) {
                    console.warn('playback failed due to audio decoding errors');
                }
            });
        }
        else {
            wrongResonates.setVolume(0.4);
            wrongResonates.play((success) => {
                if (!success) {
                    console.warn('playback failed due to audio decoding errors');
                }
            });

        }

    }
};