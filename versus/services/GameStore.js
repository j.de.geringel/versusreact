import State from './State'
import { LocalResultService } from './results/LocalResultService'

export var gameType = [{
  id: 0,
  title: 'Count Up!',
  explain: 'Tap the numbers from low to high, as fast as possible. Be quick but precise, mistakes will cause a penalty! ',
  type: 'Reaction',
  routeName: 'CounterGame',
  image: require('./../../images/screenshots/count-up.png'),
}, {
  id: 1,
  title: '3 Differences',
  explain: 'Spot the three differences between the two images. Tap the bottom image to spot them. Mistakes will cause a penalty!',
  type: 'Visual',
  routeName: 'SearchDifferencesGame',
  image: require('./../../images/screenshots/3diff.png'),
  resultDelayOnTimeup: 5
},
{
  id: 2,
  title: 'Monkey See',
  explain: 'Repeat the sequence after the example. The sequence will get longer when you progress. You have three lives.',
  type: 'Memory',
  routeName: 'RepeatMeGame',
  image: require('./../../images/screenshots/repeatme.png'),
},
{
  id: 3,
  title: 'Described',
  explain: 'Click the tile described by the text. Sounds easy? Be ware of time pressure and potential brain fries.',
  type: 'Reaction',
  routeName: 'WhatColor',
  image: require('./../../images/screenshots/matchme.png'),
},
{
  id: 4,
  title: 'Memory Train',
  explain: 'Is the icon the same as the last shown? Not a tough job in itself, but it gets tricky when we add enough time pressure.',
  type: 'Memory',
  routeName: 'MemoryTrainGame',
  image: require('./../../images/screenshots/memmorytrain.png'),
},
{
  id: 5,
  title: 'Boundaries',
  explain: 'Click the Check button when you estimate the dissapeared ball is in the light blue area. You can use Peak power-ups, but these are limited!',
  type: 'Game',
  routeName: 'BoundariesGame',
  image: require('./../../images/screenshots/boundaries.png'),
},
{
  id: 6,
  title: 'Twins',
  explain: 'You see two areas with icons. Only one icon is present at both the top as the bottom area. Tap it! You have three lives.',
  type: 'Visual',
  routeName: 'TwinsGame',
  image: require('./../../images/screenshots/twins.png'),
}];

export function GameStore() {

  return {
    /* Reproducable Seeded random based on gameid * round */
    getRandomGame: async function (gameid, roundParam) {

      let seed;
      // determine seed
      if (State.currentMatch.gameMode == 'local') {
        seed = State.currentMatch.dateStarted.getTime();
      }
      else {
        seed = gameid;       
      }

      // Shuffle Array
      let arrayCopy = gameType.slice();
      for (var i = arrayCopy.length - 1; i > 0; i--) {
        var x = Math.sin(seed) * (1000 * i);
        let rand = x - Math.floor(x);

        var j = Math.floor(rand * (i + 1));
        var temp = arrayCopy[i];
        arrayCopy[i] = arrayCopy[j];
        arrayCopy[j] = temp;
      }

      
      // SET ROUND TO PLAY
      let roundToPlay;
      if (State.currentMatch.gameMode == 'local') { // in case of local game add meta info

        // ADD META INFO TO DETERMINE WHO'S ROUND WE'RE PLAYING LOCALLY
        let userTurnInfo = await LocalResultService().getUserToPlayNextRound();

        roundToPlay = arrayCopy[roundParam ? roundParam - 1 : userTurnInfo.roundToPlay - 1]; // as parameter to display title on lobby

        roundToPlay.localPlayerName = userTurnInfo.playerName;
        roundToPlay.localPlayerIndex = userTurnInfo.playerIndex;
        if (roundToPlay.localPlayerIndex != 0) {
          roundToPlay.sameRound = true;
        }
      }
      else { // online, just get round from param
        roundToPlay = arrayCopy[roundParam -1];
      }
      return roundToPlay;
    }

  }
}
