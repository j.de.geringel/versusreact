
import constants from './../../constants'

var user = { name: null, id: null };

export function UserService() {
    return {
        getUser: function () {
            return user;
        },
        setUser: function (userToSet) {
            user.name = userToSet.userName;
            user.id = userToSet.id;
        },
        signedIn : function(){
            return user && user.id ? true : false;

        }
    }
}