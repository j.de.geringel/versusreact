var icons = [
   /* { label: 'airplane', value: require('./../../images/icons/airplane.png'), game: true },
    { label: 'campfire', value: require('./../../images/icons/campfire.png'), game: true },
    { label: 'car', value: require('./../../images/icons/car.png'), game: true },
    { label: 'carrot', value: require('./../../images/icons/carrot.png'), game: true},
    { label: 'cone', value: require('./../../images/icons/cone.png'), game: true},
    { label: 'log', value: require('./../../images/icons/log.png'), game: true},
    { label: 'megaphone', value: require('./../../images/icons/megaphone.png'), game: true},
    { label: 'rocket', value: require('./../../images/icons/rocket.png'), game: true},
    { label: 'shapes', value: require('./../../images/icons/shapes.png'), game: true},
    { label: 'animal', value: require('./../../images/icons/animal.png'), game: true},
    { label: 'binoculars', value: require('./../../images/icons/binoculars.png'), game: true},
    { label: 'cheese', value: require('./../../images/icons/cheese.png'), game: true},
    { label: 'console', value: require('./../../images/icons/game-console.png'), game: true},
    { label: 'glass', value: require('./../../images/icons/glass.png'), game: true},
    { label: 'internet', value: require('./../../images/icons/internet.png'), game: true},
    { label: 'money-bag', value: require('./../../images/icons/money-bag.png'), game: true},
    { label: 'shoe', value: require('./../../images/icons/shoe.png'), game: true},
    { label: 'weather', value: require('./../../images/icons/weather.png'), game: true},*/
    { label: 'speed', value: require('./../../images/icons/cat_speed.png'), game: false},
    { label: 'memory', value: require('./../../images/icons/cat_memory.png'), game: false},
      { label: 'visual', value: require('./../../images/icons/cat_visual.png'), game: false},
    { label: 'game', value: require('./../../images/icons/cat_game.png'), game: false},
    { label: 'trophy', value: require('./../../images/icons/trophy.png'), game: false},
]

export function getIconForGameCatogery(category){
    if (category == 'Reaction'){
       return icons.find((icon)=> icon.label =='speed');
    }
    else if (category == 'Visual'){
       return icons.find((icon)=> icon.label =='visual');
    }
    else if (category == 'Memory'){
        return icons.find((icon)=> icon.label =='memory');
    }
    else { // a game difficult to determine
        return icons.find((icon)=> icon.label =='game');
    }
}

