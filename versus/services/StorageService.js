import { AsyncStorage } from 'react-native'

export function StorageService() {
    return {
        postResultsInTopTen: async function (gameType, score) {
            return new Promise(resolve => {
                AsyncStorage.getItem('@versusSuperStore:GameScore' + gameType, (err, value) => {
                    if (value) {
                        let scoreList = JSON.parse(value);
                        scoreList = scoreList ? scoreList : [];

                        // find the ranked entry of this positon
                        let position = scoreList.sort((a, b) => b.score - a.score).findIndex((e) => score >= e.score);
                      
                        position = position == -1 ? scoreList.length + 1 : position + 1;

                        if (position <= 5) { // only top 5 are saved
                            scoreList.length > 5 ? scoreList.splice(-1, 1) : null; // remove last entry of sorted array
                            scoreList.push({ score: score, date: new Date() });
                            AsyncStorage.setItem('@versusSuperStore:GameScore' + gameType, JSON.stringify(scoreList));
                        }
                        resolve(position);

                    }
                    else { // first score for this user/game
                        AsyncStorage.setItem('@versusSuperStore:GameScore' + gameType, JSON.stringify([{ score: score, date: new Date() }]));
                        resolve(1);
                    }
                });
            });
        },
        getTopTen: async function (gameType) {
            return new Promise(resolve => {
                AsyncStorage.getItem('@versusSuperStore:GameScore' + gameType, (err, value) => {
                    if (value) {
                        let scoreList = JSON.parse(value);
                        scoreList = scoreList ? scoreList : [];


                        scoreList.sort((a, b) => b.score - a.score);

                        resolve(scoreList);

                    }
                    else {
                        resolve([]);
                    }
                });
            });
        },
        startLocalGame: async function (userArray) {
            return new Promise(resolve => {
                AsyncStorage.setItem('@versusSuperStore:LocalGame', JSON.stringify({ users: userArray, date: new Date() }), (err, value) => {
                    resolve(true);
                });
            });
        },
        removeLocalGame: async function (userArray) {
             return new Promise(resolve => {
                AsyncStorage.removeItem('@versusSuperStore:LocalGame', (err, value) => {
                    resolve(true);
                });
            });

        },    
        getLocalGame: async function () {
            return new Promise(resolve => {
                AsyncStorage.getItem('@versusSuperStore:LocalGame', (err, value) => {
                    value ? resolve(JSON.parse(value)) : resolve(null);
                });
            });
        },
      
    }
}