
import { UserService } from './../UserService'
import constants from './../../../constants'

export function XhrGameService() {
    return {
        startGame: async function (inviteeId, inviteeName) {
            return await fetch(constants.apiAddress + '/game', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    invitee: inviteeId,
                    inviteeName: inviteeName,
                    organiserName: UserService().getUser().name,
                    organiser: UserService().getUser().id
                })
            });
        },
        startOrJoinRandomGame: async function () {
            return await fetch(constants.apiAddress + '/game/random', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    userid: UserService().getUser().id,
                    username: UserService().getUser().name
                })
            });
        },
        removeGameMe: async function (gameid, opponentid) {
            return await fetch(constants.apiAddress + '/game', {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    gameid: gameid,
                    userid: UserService().getUser().id,
                    opponentid: opponentid
                })
            });
        },
        declineChallengeMe: async function (gameid) {
            return await fetch(constants.apiAddress + '/game/decline', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    gameid: gameid,
                    userid: UserService().getUser().id
                })
            });
        },
        acceptChallengeMe: async function (gameid) {
            return await fetch(constants.apiAddress + '/game/accept', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    gameid: gameid,
                    userid: UserService().getUser().id
                })
            });
        }
    }
}