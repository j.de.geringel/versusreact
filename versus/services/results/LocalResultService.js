import { AsyncStorage } from 'react-native'
import State from './../State'


/** Helper functions */

/* basicly returns the max rounds.length of all users */
function getMaxRound(localGame) {
    let roundToShow = 0;
    localGame.users.forEach((user, i) => {
        let userMaxRound = user.rounds.length;
        if (userMaxRound > roundToShow) {
            roundToShow = userMaxRound;
        }
    });
    return roundToShow;
}

export function LocalResultService() {

    return {
        postResult: async function (stars, points) {
            let localGameRaw = await AsyncStorage.getItem('@versusSuperStore:LocalGame');
            let localGame = JSON.parse(localGameRaw);
            // who are we playing for?
            let player = localGame.users[State.currentRound.localPlayerIndex];
            player.rounds.push({ points: points, stars: stars });
            player.totalscore = Math.round((player.totalscore + stars) * 100) / 100;
            await AsyncStorage.setItem('@versusSuperStore:LocalGame', JSON.stringify(localGame));
            return true;
        },
        /** Base implementation at ResultService.getLatestResults() */
        getLatestResults: async function () {
            let localGameRaw = await AsyncStorage.getItem('@versusSuperStore:LocalGame');

            let localGame = JSON.parse(localGameRaw);

            // get the round to show... 
            let roundToShow = getMaxRound(localGame);

            // get stars and points for the round to show
            localGame.users.forEach((user, i) => {
                user.stars = roundToShow > 0 && user.rounds.length >= roundToShow ? user.rounds[roundToShow - 1].stars : null;
                user.points = roundToShow > 0 && user.rounds.length >= roundToShow ? user.rounds[roundToShow - 1].points : null;
                user.lastround = user.rounds.length;
            });

            // determine next round
            let nextRoundInLocalGame = 99;

            localGame.users.forEach((user, i) => {
                if (user.rounds.length < nextRoundInLocalGame) {
                    nextRoundInLocalGame = user.rounds.length + 1;
                }
            });
            localGame.users.nextRound = nextRoundInLocalGame;

            return localGame.users;
        },
        /** looks trough local storage and basicly returns the max rounds.length of all users*/
        getMaxRoundMe: async function () {
            let localGameRaw = await AsyncStorage.getItem('@versusSuperStore:LocalGame');
            let localGame = JSON.parse(localGameRaw);

            return getMaxRound(localGame);;
        },
        getResultForRound: async function (round) {
            let localGameRaw = await AsyncStorage.getItem('@versusSuperStore:LocalGame');
            let localGame = JSON.parse(localGameRaw);

            localGame.users.forEach((user, i) => {
                user.stars = user.rounds.length >= round ? user.rounds[round - 1].stars : null;
                user.points = user.rounds.length >= round ? user.rounds[round - 1].points : null;
                user.lastround = user.rounds.length;
            });
            return localGame.users;

        },
        /** Returns the user which turn it is to play a round  */
        getUserToPlayNextRound: async function () {
            let localGameRaw = await AsyncStorage.getItem('@versusSuperStore:LocalGame');
            let localGame = JSON.parse(localGameRaw);

            let minRound = 99;
            let userIndexWithLeastRounds;

            localGame.users.forEach((user, i) => {
                if (user.rounds.length < minRound) {
                    minRound = user.rounds.length;
                    userIndexWithLeastRounds = i;
                }
            });

            return {
                roundToPlay: minRound + 1,
                playerIndex: userIndexWithLeastRounds,
                playerName: localGame.users[userIndexWithLeastRounds].username
            };

        }

    }
}
