
import {UserService} from './../UserService'
import {GameStore} from './../GameStore'
import constants from './../../../constants'

export function XhrResultService() {
    return {
        
        postResult: async function (gameid, stars, points, round, maxNoRounds) {
            let timeInMilis = new Date().getMilliseconds();
            let timeInMilisCheck = timeInMilis + (stars * 200) + points;
            return await fetch(constants.apiAddress + '/result', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'nginxts' : timeInMilisCheck
                },
                body: JSON.stringify({
                    gameid: gameid,
                    userid: UserService().getUser().id,
                    round: round,
                    points: points,
                    stars: stars,
                    maxNoRounds: maxNoRounds,
                    curTimeInMilis: timeInMilis

                })
            });
        },
        lockResult: async function (gameid, round) {
            let currentRound = await GameStore().getRandomGame(gameid, round);

            return await fetch(constants.apiAddress + '/result/lock', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    gameid: gameid,
                    userid: UserService().getUser().id,
                    gametype: currentRound.id,
                    round: round
                })
            });
        },
        getLatestResults: async function (gameid, userid) {
            let response = await fetch(constants.apiAddress + '/results?userid=' + userid + '&gameid=' + gameid);
            responseJson = await response.json();
            return responseJson.data;
            
        },
        getMaxRoundMe: function (rounds) {
            let userRound = rounds.find((r) => r.userid == UserService().getUser().id);
            return userRound.round ? userRound.round : 0;
        },
        getResultForRound: async function (gameid, round) {
            let response = await fetch(constants.apiAddress +'/results?gameid=' + gameid + '&round=' + round);
            responseJson = await response.json();
            return responseJson.data;

        }
    }
}

