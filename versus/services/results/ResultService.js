
import { LocalResultService } from './LocalResultService'
import { XhrResultService } from './XhrResultService'
import State from './../State'


export function ResultService() {
    return {
        /** Post the results of a round to eiher local or server. */
        postResult: async function (gameid, stars, points, round, maxNoRounds) {
            try {
                State.currentMatch.gameMode == 'local' ? await LocalResultService().postResult(stars, points) : 
                    State.currentMatch.gameMode == 'online' ? await XhrResultService().postResult(gameid, stars, points, round, maxNoRounds) : null;
                return true;
            }
            catch (error) {
                console.error('error' + error);
                return false;
            }
        },
        /** Locks the round by pre-posting results so people cannot retry a game when it is started. */
        lockResult: async function (gameid, round) {
            try {
                State.currentMatch.gameMode == 'online' ? await XhrResultService().lockResult(gameid, round) : null;
                return true;
            }
            catch (error) {
                console.error('error' + error);
                return false;
            }
        },
        /** Get the latest results... but not later then user's own last round. 
         * 
         * Results looks like : {[stars, points, lastround, totalpoints], ...} 
        */
        getLatestResults: async function (gameid, userid) {
            try {
                return State.currentMatch.gameMode == 'local' ? await LocalResultService().getLatestResults() :
                    await XhrResultService().getLatestResults(gameid, userid);
            }
            catch (error) {
                console.error('error' + error);
                return false;
            }
        },
          /** Get results for specific round. 
         * 
         * Results looks like : {[stars, points, lastround, totalpoints], ...} 
        */
        getResultForRound: async function (gameid, round) {
            try {
                return State.currentMatch.gameMode == 'local' ? await LocalResultService().getResultForRound(round) :
                    await XhrResultService().getResultForRound(gameid, round);
            }
            catch (error) {
                console.error('error' + error);
                return false;
            }
        },
        /** Gets the latest result of the user.
         *  -  local looks trough local storage and basicly does a rounds.length
         *  -  xhr looks trough the given array and looks up the user round
        */
        getMaxRoundMe: async function (rounds) {
            try {
                return State.currentMatch.gameMode == 'local' ? await LocalResultService().getMaxRoundMe() :
                    await XhrResultService().getMaxRoundMe(rounds);
            }
            catch (error) {
                console.error('error' + error);
                return false;
            }
        },
      
    }
}