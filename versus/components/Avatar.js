

import React, { Component } from 'react'
import { View, Image } from 'react-native'

import colors from './../../colors';

export default class Avatar extends Component {

    source = null;

    avatars = [
        { name: 'aap', source: require('./../../images/avatars/aap_no_back.png') },
        { name: 'panda', source: require('./../../images/avatars/panda_no_back.png') },
        { name: 'uil', source: require('./../../images/avatars/uil_no_back.png') },
        { name: 'hond', source: require('./../../images/avatars/hond_no_back.png') },
        { name: 'vos', source: require('./../../images/avatars/vos_no_back.png') },
        { name: 'wolf', source: require('./../../images/avatars/wolf_no_back.png') },
        { name: 'kat', source: require('./../../images/avatars/kat_no_back.png') },
        { name: 'beer', source: require('./../../images/avatars/beer_no_back.png') },
        { name: 'muis', source: require('./../../images/avatars/muis_no_back.png') },
        { name: 'koe', source: require('./../../images/avatars/koe_no_back.png') },
        { name: 'haas', source: require('./../../images/avatars/haas_no_back.png') }
    ]

    constructor(props) {
        super(props);
        this.state = { source: null };
    }

    setImageProps = function (user, image) {
        if (image == null) { // no image? set animal tied to id
            return this.setImgageBasedOnId();
        }
        else if (image.startsWith("http")) { // so this is a url, get it from web
            return { uri: this.props.image };
        }
        else { // image is something but not an url, it's a avatar!
            // todo v, a, a, v, a, v, a
        }

    }


    // will always return same image for given userid
    setImgageBasedOnId(onError) {
        var seed = 0;
        if (this.props.userid) {
           seed = this.props.userid;
        }
        else {
            let charArray = this.props.username.split('');
            charArray.forEach(function (value) {
                seed = seed + value.charCodeAt();
            });
        }
        x = Math.sin(seed) * 10000;
        let rand = x - Math.floor(x);

        return this.avatars[Math.floor(rand * this.avatars.length)].source;
    }


    render() {
        let source;
        if (this.state.error) { //forcing avatar image
            source = this.setImgageBasedOnId();
        }
        else {
            source = this.setImageProps(this.props.userid, this.props.image);
        }

        return (
            <Image source={source} onError={(error) => this.setState({ error: true })}
                style={{ height: this.props.size ? this.props.size : 60, width: this.props.size ? this.props.size : 60, borderRadius: 8,  borderWidth: 0 , borderColor: colors.lightbg }} />
        )
    }
}

