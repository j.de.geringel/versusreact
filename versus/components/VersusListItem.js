import React, { Component } from 'react'
import { View, Image, Text } from 'react-native'
import { Icon} from 'react-native-elements'

import colors from './../../colors';

/**
 * API:
 *   - title, subtitle
 *   - image, catImage (defaults to chevron)
 *   - backgroundColor (defaults to white), textColor (defaults to white)
 */
export default class VersusListItem extends Component {

    setNativeProps(props) {
        this.refs['VERSUS_LIST_ITEM'].setNativeProps(props)
    }
    
    render() {
        return (
            <View ref="VERSUS_LIST_ITEM" style={{ backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : 'white', 
             padding: 10, height: 70, borderBottomWidth: 1, borderBottomColor: colors.divider, flexDirection: 'row' }}>
                <View style={{ width: 54, alignItems: 'center', paddingLeft: 10, justifyContent: 'center' }}>
                    <Image source={this.props.image} style={{ height: 50, width: 50, borderRadius: 8 }} />
                </View>
                <View style={{ flex: 3, padding: 5, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 18, paddingBottom: 2, fontWeight: 'bold', color: this.props.textColor ? this.props.textColor: colors.black}}>{this.props.title}</Text>
                    <Text style={{  color: this.props.textColor ? this.props.textColor: 'gray', marginTop: 2, fontSize: 12 }}>{this.props.subTitle}</Text>
                </View>
                <View style={{ width: 50, justifyContent: 'center', alignItems: 'center' }}>
                    {this.props.catImage ? <Image source={this.props.catImage} style={{ width: 35, height: 35 }} /> : 
                      <Icon name='chevron-right' color={this.props.textColor ? this.props.textColor: 'gray'} size={40} /> }
                </View>
            </View>
        )
    }

}