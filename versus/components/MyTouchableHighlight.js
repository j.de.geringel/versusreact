import { View, TouchableHighlight} from 'react-native'
import Sound from 'react-native-sound';

export default class Avatar extends Component {

     componentDidMount() {
         
     }

     render() {
        let source;
        if (this.state.error) { //forcing avatar image
            source = this.setImgageBasedOnId();
        }
        else {
            source = this.setImageProps(this.props.userid, this.props.image);
        }

        return (
            <Image source={source} onError={(error) => this.setState({ error: true })}
                style={{ height: this.props.size ? this.props.size : 60, width: this.props.size ? this.props.size : 60, borderRadius: 8,  borderWidth: 0 , borderColor: colors.lightbg }} />
        )
    }

}