

import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Icon } from 'react-native-elements'

import colors from './../../colors';
import State from './../services/State'
import SoundService from './../services/SoundService'
import {UserService} from './../services/UserService'

export default class RegularHeader extends Component {

  constructor(props) {
    super(props);
  }

  determineBackNav(){
    SoundService.playClickSound();
    if (!this.props.navigation.goBack()){
      this.props.navigation.navigate(UserService().signedIn() ? 'Home' : 'SignUpScreen');
    }
  }
  
  render() {
    return (
      <View style={{ height: 50, padding: 5, paddingRight: 20, paddingLeft: 20, backgroundColor: colors.primdark, flexDirection: 'row' }}>
        <View style={{ alignItems: 'flex-start', flexDirection: 'row' }}>
          <Icon name='arrow-back' color={colors.primlight} style={{ marginRight: 40 }} size={40} onPress={() => { this.determineBackNav() } } />
        </View>
        <Text style={{ flex: 1, color: colors.white, textAlign: 'center', paddingRight: 15, paddingTop: 5, fontSize: 22 }}>{this.props.message}</Text>

      </View>
    )
  }
}