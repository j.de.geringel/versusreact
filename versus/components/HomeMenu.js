

import React, { Component, } from 'react'
import { View, Image, Modal, TouchableHighlight, Text, Alert,Linking } from 'react-native'
import { Icon } from 'react-native-elements'

import { UserService } from './../services/UserService'
import SoundService from './../services/SoundService'

import colors from './../../colors';

import { NavigationActions } from 'react-navigation'

export default class HomeMenu extends Component {
    render() {


        return (
            <Modal animationType={"none"} transparent={true} visible={this.props.show ? true : false} onRequestClose={() => this.props.closeCallback()} >
                <TouchableHighlight style={{ flex: 1 }} onPress={() => this.props.closeCallback()} >
                    <View style={{ flex: 1, opacity: 1.0, flexDirection: 'row', marginTop: 50 }}>
                        <View style={{ flex: 1 }} />
                        <View style={{ flex: 2, flexDirection: 'column' }}>
                            <View style={{ borderBottomWidth: 1, borderColor: colors.black, borderRightWidth: 1, borderLeftWidth: 1 }}>
                                <TouchableHighlight>
                                    <View style={{ borderBottomColor: colors.divider, borderBottomWidth: 1, padding: 15, backgroundColor: colors.primdark, flexDirection: 'row' }}>
                                        <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                                            <Icon name={"account-circle"} size={28} color={colors.primlight} />
                                        </View>
                                        <View style={{ flex: 3 }}>
                                            <Text style={{ color: colors.white, fontSize: 20 }}>{UserService().signedIn() ? UserService().getUser().name : 'Not Signed In'}</Text>
                                        </View>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight onPress={() => {
                                    SoundService.playClickSound();
                                    this.props.closeCallback();
                                    this.props.navigation.navigate('GameOverview');
                                }}>
                                    <View style={{
                                        borderBottomColor: colors.divider, borderBottomWidth: 1, padding: 15, backgroundColor: colors.white, flexDirection: 'row'
                                    }}>
                                        <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                                            <Icon name={"games"} size={28} color={colors.primlight} />
                                        </View>
                                        <View style={{ flex: 3 }}>
                                            <Text style={{ color: colors.black, fontSize: 20 }}>Practice</Text>
                                        </View>
                                    </View>
                                </TouchableHighlight>

                                <TouchableHighlight onPress={() => Alert.alert('Info',
                                    'Versus is made by Polyglot Programmers. We love feedback, visit us at http://www.polyprog.io or mail to info@polyprog.io!',
                                    [{ text: 'Send Mail', onPress: () => {
                                        Linking.openURL("mailto:info@polyprog.io").catch(err => console.warn('An error occurred', err));}},
                                    {
                                        text: 'CLOSE', onPress: () => true
                                    }]
                                )}>
                                    <View style={{
                                        borderBottomColor: colors.divider, borderBottomWidth: 1, padding: 15, backgroundColor: colors.white, flexDirection: 'row'
                                    }}>
                                        <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                                            <Icon name={"info-outline"} size={28} color={colors.primlight} />
                                        </View>
                                        <View style={{ flex: 3 }}>
                                            <Text style={{ color: colors.black, fontSize: 20 }}>About</Text>
                                        </View>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight onPress={() => {
                                    UserService().setUser({ userName: null, id: null });
                                    this.props.navigation.dispatch(
                                        NavigationActions.reset({
                                            index: 0, actions: [NavigationActions.navigate({ routeName: 'SignUpScreen' })]
                                        }));
                                }}>
                                    <View style={{
                                        borderBottomColor: colors.divider, borderBottomWidth: 1, padding: 15, backgroundColor: colors.white, flexDirection: 'row'
                                    }}>
                                        <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                                            <Icon name={"highlight-off"} size={26} color={colors.primbutton} />
                                        </View>
                                        <View style={{ flex: 3 }}>
                                            <Text style={{ color: colors.black, fontSize: 20 }}>Sign out</Text>
                                        </View>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                </TouchableHighlight>
            </Modal>
        )

    }
}