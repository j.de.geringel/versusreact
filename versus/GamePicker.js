import React from 'react'
import { View, AsyncStorage, TouchableHighlight, Animated, Dimensions, Image, Easing, Text, BackAndroid } from 'react-native'
import { Button, Icon } from 'react-native-elements'
import { GameStore, gameType } from './services/GameStore'
import SoundService from './services/SoundService'
import Sound from 'react-native-sound';
import State from './services/State'
import colors from './../colors';

import { NavigationActions } from 'react-navigation'


export default class GamePicker extends React.Component {


  imageWidth = Dimensions.get('window').width / 3;
  imageHeight = (Dimensions.get('window').height / 3) - 16;
  imageCarouselWidth = (this.imageWidth + 40) * gameType.length;  //padding
  activeOpacity = [];
  baseOpacity = [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2];
  previousOpacityIndex;
  gameTitle = '';
  breakRecursive = false;

  pop = new Sound('chop.wav', Sound.MAIN_BUNDLE);



  constructor(props) {
    super(props);
    this.state = {
      scrollPos: new Animated.Value(-this.imageCarouselWidth * 2), //+ iw*0.75 since we're centered by default
      h1Text: 'Picking game...',
      activeOpacity: [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2],
      gameTitle: '',
      h1Color: colors.primlight
    }
    BackAndroid.addEventListener('hardwareBackPress', this._androidback.bind(this));

  }

  /* handle back button  for android */
  _androidback = function () {
    return true;
  }


  async componentDidMount() {
    this.breakRecursive = false;
    await this.sleep(100);
    this.pop.setVolume(0.2);
    this.playSound();
    this.blinkImage();
    await this.sleep(1500);
    this.breakRecursive = true;

  }

  async componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', this._androidback);
    this.breakRecursive = true;
  }

  playSound() {
    if (!this.breakRecursive) {
      this.pop.play((success) => {
        if (!success) {
          console.warn('playback failed due to audio decoding errors');
        }
        else {
          this.breakRecursive ? null : this.playSound();
        }

      });
    }

  }

  async blinkImage() {

    var activeOpacityCopy = this.baseOpacity.slice(); //reset

    if (!this.breakRecursive) {
      await this.sleep(70);


      var index = Math.floor(Math.random() * activeOpacityCopy.length);

      if (this.previousOpacityIndex == index) { //dont blink twice
        index = Math.floor(Math.random() * activeOpacityCopy.length);
      }

      this.previousOpacityIndex = index;
      activeOpacityCopy[index] = 1.0;


      this.setState({ activeOpacity: activeOpacityCopy, gameTitle: gameType[index].title });
      this.blinkImage();
    }
    else { //recursive broken
      activeOpacityCopy[State.currentRound.id] = 1.0;
      SoundService.playCorrectSound();
      this.setState({ activeOpacity: activeOpacityCopy, gameTitle: gameType[State.currentRound.id].title, h1Color: colors.white });
      await this.sleep(500);
      this.props.navigation.dispatch(
        NavigationActions.reset({
          index: 0, actions: [NavigationActions.navigate({ routeName: 'GameSplashScreen' })]
        }));
    }
  }

  userClicked() {
    if (!this.breakRecursive) {
      this.breakRecursive = true; // recursive still active, break 'naturally'
    }

  }


  // ES7, async/await
  sleep(ms = 0) {
    return new Promise(r => setTimeout(r, ms));
  }

  render() {

    // TODO MOVE PASSPHONE LOGIC TO LOBBY SCREEN
    return (

      <View style={{ flex: 1, backgroundColor: colors.black }}>
        <View style={{ height: 48, backgroundColor: colors.primdark, flexDirection: 'row' }}>
          <View style={{ flex: 3, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ color: this.state.h1Color, fontSize: 25, textAlign: 'center' }}>{this.state.gameTitle}</Text>
          </View>

        </View>
        <TouchableHighlight onPress={() => this.userClicked()} style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>

            <View style={{ flexDirection: 'row', flex: 1 }}>
              {[0, 1, 2].map((ind) => {
                return (
                  <View key={'image' + ind} style={{ flex: 1, opacity: this.state.activeOpacity[ind] }}><Image source={gameType[ind].image} style={{ flex: 1, width: undefined, height: undefined, resizeMode: 'contain' }} /></View>
                )
              })}
            </View>
            <View style={{ flexDirection: 'row', flex: 1 }}>
              {[3, 4, 5].map((ind) => {
                return (
                  <View key={'image' + ind} style={{ flex: 1, opacity: this.state.activeOpacity[ind] }}><Image source={gameType[ind].image} style={{ flex: 1, width: undefined, height: undefined, resizeMode: 'contain' }} /></View>
                )
              })}
            </View>
            <View style={{ flexDirection: 'row', flex: 1 }}>
              <View style={{ flex: 1 }}><View style={{ width: this.imageWidth, height: this.imageHeight, backgroundColor: colors.black }} /></View>
              <View style={{ flex: 1, opacity: this.state.activeOpacity[6] }}><Image source={gameType[6].image} style={{ flex: 1, width: undefined, height: undefined, resizeMode: 'contain' }} /></View>
              <View style={{ flex: 1 }}><View style={{ width: this.imageWidth, height: this.imageHeight, backgroundColor: colors.black }} /></View>

            </View>
          </View>
        </TouchableHighlight>


      </View >
    )



  }
}
/*  <View style={{position:'absolute', left:0, right:0, top: 0, bottom:0, justifyContent: 'center', alignItems: 'center', flexDirection:'row'}}>
          <View style={{height: 50, flex:1, backgroundColor:colors.black, opacity: 0.5}}><Text style={{color: colors.white, fontSize: 30, textAlign:'center'}}>{this.gameTitle}</Text></View>
        </View>
*/