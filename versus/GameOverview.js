import React, { Component } from 'react'
import { View, Text, Image, Dimensions, TouchableWithoutFeedback, ScrollView } from 'react-native'
import { Router } from './../Router'
import State from './services/State';
import RegularHeader from './components/RegularHeader'
import colors from './../colors'
import { getIconForGameCatogery } from './services/IconService';
import SoundService from './services/SoundService';
import { gameType } from './services/GameStore'


export default class GameOverview extends Component {

    imageWidth = Dimensions.get('window').width / 3.5;


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.black }}>
                <RegularHeader message={'Practice'} navigation={this.props.navigation} />
                <ScrollView>
                    <View style={{ backgroundColor: colors.black, flexDirection: 'row' }}>
                        {[0, 1, 2].map((ind) => {
                            return (
                                <TouchableWithoutFeedback key={ind} onPress={() => {
                                    SoundService.playClickSound();
                                    State.currentMatch = { gameMode: 'practice' };
                                    State.currentRound = gameType[ind];
                                    this.props.navigation.navigate('GameSplashScreen');
                                }} >
                                    <View style={{ flex: 1, borderColor: colors.primdark, borderWidth: 2, alignItems: 'center', paddingTop: 10 }}>
                                        <Image source={gameType[ind].image} style={{ height: this.imageWidth * 1.51, width: this.imageWidth - 4, opacity: 0.9 }}>
                                            <Image source={getIconForGameCatogery(gameType[ind].type).value} style={{ width: 40, height: 40, position: 'absolute', right: 5, bottom: 5 }} />
                                        </Image>

                                        <View style={{ alignItems: 'center', backgroundColor: colors.black }}>
                                            <Text style={{ color: colors.primlight, padding: 10, fontSize: 16 }}>{gameType[ind].title}</Text>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            )
                        })}
                    </View>
                    <View style={{ backgroundColor: colors.black, flexDirection: 'row' }}>
                        {[3, 4, 5].map((ind) => {
                            return (
                                <TouchableWithoutFeedback key={ind} onPress={() => {
                                    SoundService.playClickSound();
                                    State.currentMatch = { gameMode: 'practice' };
                                    State.currentRound = gameType[ind];
                                    this.props.navigation.navigate('GameSplashScreen');
                                }} >

                                    <View style={{ flex: 1, borderColor: colors.primdark, borderWidth: 2, alignItems: 'center', paddingTop: 10 }}>
                                        <Image source={gameType[ind].image} style={{ height: this.imageWidth * 1.51, width: this.imageWidth - 4, opacity: 0.9 }}>
                                            <Image source={getIconForGameCatogery(gameType[ind].type).value} style={{ width: 40, height: 40, position: 'absolute', right: 5, bottom: 5 }} />
                                        </Image>

                                        <View style={{ alignItems: 'center', backgroundColor: colors.black }}>
                                            <Text style={{ color: colors.primlight, padding: 10, fontSize: 16 }}>{gameType[ind].title}</Text>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            )
                        })}
                    </View>
                    <View style={{ backgroundColor: colors.black, flexDirection: 'row' }}>
                        <TouchableWithoutFeedback key={6} onPress={() => {
                            SoundService.playClickSound();
                            State.currentMatch = { gameMode: 'practice' };
                            State.currentRound = gameType[6];
                            this.props.navigation.navigate('GameSplashScreen');
                        }} >
                            <View style={{ flex: 1, borderColor: colors.primdark, borderWidth: 2, alignItems: 'center', paddingTop: 10 }}>
                                <Image source={gameType[6].image} style={{ height: this.imageWidth * 1.51, width: this.imageWidth - 4, opacity: 0.9 }}>
                                    <Image source={getIconForGameCatogery(gameType[6].type).value} style={{ width: 40, height: 40, position: 'absolute', right: 5, bottom: 5 }} />
                                </Image>
                                <View style={{ alignItems: 'center', backgroundColor: colors.black }}>
                                    <Text style={{ color: colors.primlight, padding: 10, fontSize: 16 }}>{gameType[6].title}</Text>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>

                        <View style={{ flex: 1 }} />
                        <View style={{ flex: 1 }} />
                    </View>
                </ScrollView>
            </View>
        )
    }
}
