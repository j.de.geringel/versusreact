export default {
  primlight: '#6E8A9F',
  primmed: '#203e52',
  primdark: '#092435',
  primalpha: '#D1DBDD',
  divider: '#cbd2d9',
  dividerDarker: '#aab1b7',
  primbutton: '#E5481B',
  secbutton: '#1e76ae',
  statusbar: 'black',
  buttonok: '#008C46',
  black: '#000000',
  white: '#FFFFFF',
  lightbg: '#E4EAED'

  /*primlight: '#FFA042',
  primdark: '#F57C00',
  prrimalpha: '#FFE6CC',
  divider: '#cbd2d9',
  primbutton: '#00D96D',
  secbutton: '#FFA042' */
  
}
