import React from 'react';
import { AppRegistry, Text, View, StatusBar, AppState } from 'react-native';




import { Router } from './Router';
import colors from './colors';

class App extends React.Component {


  componentDidMount() {
    StatusBar.setBackgroundColor('black'); // in some cases the status bar defaulted back to gray
  }



  render() {
 
    /**
      * NavigationProvider is only needed at the top level of the app,
      * similar to react-redux's Provider component. It passes down
      * navigation objects and functions through context to children.
      *
      * StackNavigation represents a single stack of screens, you can
      * think of a stack like a stack of playing cards, and each time
      * you add a screen it slides in on top. Stacks can contain
      * other stacks, for example if you have a tab bar, each of the
      * tabs has its own individual stack. This is where the playing
      * card analogy falls apart, but it's still useful when thinking
      * of individual stacks.
      */

    return (
      <View style={{flex:1}}>
        <StatusBar backgroundColor='black' barStyle="light-content" />
        <Router />
      </View>
    );
  }
}




AppRegistry.registerComponent('versusreact', () => App);