import { StackNavigator } from 'react-navigation';

import Home from './versus/Home'
import GameSplashScreen from './versus/GameSplashScreen'
import SignUpScreen from './versus/signup/SignUpScreen'
import SignUpModal from './versus/signup/SignUpModal'
import LobbyScreen from './versus/LobbyScreen'
import Initial from './versus/Initial'
import NewGameScreen from './versus/NewGameScreen'
import NewGameSearchScreen from './versus/NewGameSearchScreen'
import GameOverview from './versus/GameOverview'
import GameResultPage from './versus/GameResultPage'
import GamePicker from './versus/GamePicker'


import CounterGame from './versus/games/CounterGame'
import RepeatMeGame from './versus/games/RepeatMeGame'
import SearchDifferencesGame from './versus/games/SearchDifferencesGame'
import WhatColor from './versus/games/WhatColor'
import MemoryTrainGame from './versus/games/MemoryTrainGame'
import BoundariesGame from './versus/games/BoundariesGame'
import TwinsGame from './versus/games/TwinsGame'
import PartyGameLobby from './versus/PartyGameLobby'




/**
  * This is where we map route names to route components. Any React
  * component can be a route, it only needs to have a static `route`
  * property defined on it, as in HomeScreen below
  */
export const Router = StackNavigator(
  {
    Initial: { screen: Initial },
    Home: { screen: Home },
    SignUpScreen: { screen: SignUpScreen },
    SignUpModal: { screen: SignUpModal },
    GameOverview: { screen: GameOverview },
    GameSplashScreen: { screen: GameSplashScreen },
    CounterGame: { screen: CounterGame },
    RepeatMeGame: { screen: RepeatMeGame },
    LobbyScreen: { screen: LobbyScreen },
    NewGameScreen: { screen: NewGameScreen },
    NewGameSearchScreen: { screen: NewGameSearchScreen },
    SearchDifferencesGame: { screen: SearchDifferencesGame },
    WhatColor: { screen: WhatColor },
    MemoryTrainGame: { screen: MemoryTrainGame },
    BoundariesGame: { screen: BoundariesGame },
    GameResultPage: { screen: GameResultPage },
    TwinsGame: { screen: TwinsGame },
    PartyGameLobby: { screen: PartyGameLobby },
    GamePicker: { screen: GamePicker },
  },
  {
    initialRouteName: 'Initial',
    headerMode: 'none'
  });

